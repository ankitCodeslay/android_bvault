package com.mobile.bvault.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.mobile.bvault.ui.view.DrawerInterface;
import com.mobile.bvault.ui.fragment.NavigationFragment;
import com.mobile.bvault.ui.activity.WishlistActivity;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.NotificationReceiver;

/**
 * Created by diptif on 16/08/17.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView, DrawerInterface {

    private static final String TAG = "BaseActivity";

    Dialog progressDialog;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    NavigationFragment navigationFragment;
    NotificationReceiver notificationReceiver;

    @Override
    public void showLoading() {
        if (isFinishing()) return;
        progressDialog = CommonUtils.showLoadingBar(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationReceiver = new NotificationReceiver(this);
        notificationReceiver.registerReceiver();
    }

    @Override
    public void hideLoading() {
        CommonUtils.hideLoadingBar(progressDialog);
    }

    @Override
    public boolean isConnectedToInternet() {
        return false;
    }

    @Override
    public void showSnackBar(String message) {
        if (message != null) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
            View subView = snackbar.getView();
            subView.setBackground(ContextCompat.getDrawable(this, R.color.white));
            TextView textView = (TextView) subView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(this, R.color.black_effective));
            snackbar.show();
        }
    }

    @Override
    public void showToastMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void setFullScreen() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }


    @Override
    public void initDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        if (drawerLayout != null && navigationView != null) {
//            navigationView.setNavigationItemSelectedListener(this);
            navigationFragment = new NavigationFragment();
            navigationFragment.setDrawerInterface(this);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.nav_fragment_container, navigationFragment)
                    .commit();

            ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    updateDrawerInfo();
                    CommonUtils.printErrorLog(TAG, "Drawer opened");
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    CommonUtils.printErrorLog(TAG, "Drawer closed");
                }
            };

            drawerLayout.addDrawerListener(actionBarDrawerToggle);
            actionBarDrawerToggle.syncState();
        }
    }

    @Override
    public void openDrawer() {
        if (drawerLayout != null) {
            drawerLayout.openDrawer(Gravity.START);
        }
    }

    @Override
    public void closeDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(Gravity.START);
        }
    }

    @Override
    public void setSupportToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    @Override
    public void setToolbarColor(int color) {
        if (toolbar != null) toolbar.setBackgroundResource(color);
        else CommonUtils.printErrorLog(TAG, "toolbar is not Instantiated");
    }

    @Override
    public void setTitle(String title) {
        TextView textView = (TextView) findViewById(R.id.toolbar_heading);
        if (textView != null && title != null) {
            textView.setText(title);
        }
    }

    @Override
    public void setTitle(int resId) {
        TextView textView = (TextView) findViewById(R.id.toolbar_heading);
        textView.setText(resId);
    }

    @Override
    public void setToolbarLeftIconClickListener() {
        ImageView imageView = (ImageView) findViewById(R.id.toolbar_left_layer);
        if (imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setOnClickListener(onToolbarLayerClickListener);
        }
    }

    @Override
    public void setToolbarRightIconClickListener() {
        RelativeLayout rightLayerContainer = (RelativeLayout) findViewById(R.id.right_layer_container);
        ImageView imageView = (ImageView) findViewById(R.id.toolbar_right_layer);
        if (rightLayerContainer != null && imageView != null) {
            rightLayerContainer.setVisibility(View.VISIBLE);
            imageView.setOnClickListener(onToolbarLayerClickListener);
        }
    }

    View.OnClickListener onToolbarLayerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.toolbar_left_layer:
                    openDrawer();
                    break;
                case R.id.toolbar_right_layer:
                    showWishlist();
                    break;
                case R.id.toolbar_search:
                    showWishlist();
                    break;
            }
        }
    };

    @Override
    public void showWishlist() {
        startActivity(new Intent(this, WishlistActivity.class));
    }

    @Override
    public void updateWishlistCount() {
        new BasePresenter(this, this).updateCount();
    }

    @Override
    public void updateWishlistCountCallback(int count) {
        TextView textView = (TextView) findViewById(R.id.wishlist_count);
        if (textView != null) {
            if (count > 0) {
                textView.setText(String.format("%02d", count));
                textView.setVisibility(View.VISIBLE);
            } else {
                textView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateWishlistCount();
        if (notificationReceiver != null) {
            notificationReceiver.registerReceiver();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (notificationReceiver != null) {
            notificationReceiver.unregisterReceiver();
        }
    }

    @Override
    public void updateDrawerInfo() {
        if (navigationFragment != null)
            navigationFragment.updateDrawerInfo();
    }

    @Override
    protected void onDestroy() {
        new BasePresenter(this).closeDB();
        if (notificationReceiver != null) {
            notificationReceiver.unregisterReceiver();
        }
        super.onDestroy();
    }

    public void toHideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public void toOpenPlayStore() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
