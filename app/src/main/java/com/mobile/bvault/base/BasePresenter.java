package com.mobile.bvault.base;

import android.content.Context;

import com.mobile.bvault.data.sqlite.DBHelper;

/**
 * Created by diptif on 30/08/17.
 */

public class BasePresenter {

    private Context context;
    private BaseView baseView;

    public BasePresenter(Context context, BaseView baseView) {
        this.context = context;
        this.baseView = baseView;
    }

    public BasePresenter(Context context) {
        this.context = context;
    }

    public void updateCount(){
        int count = DBHelper.getInstance(context).getItemCount();
        baseView.updateWishlistCountCallback(count);
    }

    public void closeDB() {
        DBHelper.getInstance(context).closeDB();
    }


}
