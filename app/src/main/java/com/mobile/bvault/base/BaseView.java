package com.mobile.bvault.base;

/**
 * Created by diptif on 16/08/17.
 */

public interface BaseView {

    void showLoading();

    void hideLoading();

    boolean isConnectedToInternet();

    void showSnackBar(String message);

    void showToastMessage(String message);

    void setFullScreen();


    /**
     * Sets default toolbar
     */
    void setSupportToolbar();

    /**
     * Sets toolbar color
     */
    void setToolbarColor(int color);


    /**
     * Sets toolbar title
     * @param title title passed by activity
     */
    void setTitle(String title);

    /**
     * Sets toolbar title
     * @param resId string resource id
     */
    void setTitle(int resId);

    /**
     * Sets click listener on cart icon
     */
    void setToolbarRightIconClickListener();

    /**
     * Sets click listener on menu icon to display the drawer
     */
    void setToolbarLeftIconClickListener();

    /**
     * Display the wishlist screen
     */
    void showWishlist();

    /**
     * Updates the cart count
     */
    void updateWishlistCount();

    /**
     * Update wishlist count callback
     * @param count number of items added to cart
     */
    void updateWishlistCountCallback(int count);

    void updateDrawerInfo();

}
