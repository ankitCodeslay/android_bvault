package com.mobile.bvault.aws;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 10/01/18.
 */

public class AWS implements Parcelable {

    @SerializedName("accessKeyId")
    String awsAccessKey = "";

    @SerializedName("secretAccessKey")
    String awsSecretKey = "";


    public AWS(String awsAccessKey, String awsSecretKey) {
        this.awsAccessKey = awsAccessKey;
        this.awsSecretKey = awsSecretKey;
    }

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    public void setAwsAccessKey(String awsAccessKey) {
        this.awsAccessKey = awsAccessKey;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.awsAccessKey);
        dest.writeString(this.awsSecretKey);
    }

    protected AWS(Parcel in) {
        this.awsAccessKey = in.readString();
        this.awsSecretKey = in.readString();
    }

    public static final Parcelable.Creator<AWS> CREATOR = new Parcelable.Creator<AWS>() {
        @Override
        public AWS createFromParcel(Parcel source) {
            return new AWS(source);
        }

        @Override
        public AWS[] newArray(int size) {
            return new AWS[size];
        }
    };
}
