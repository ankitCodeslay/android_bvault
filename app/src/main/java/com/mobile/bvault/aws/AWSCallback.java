package com.mobile.bvault.aws;

/**
 * Created by diptif on 09/10/17.
 */

public interface AWSCallback {

    void onAWSSuccessCallback(String fileName,String filePath);

    void onAWSFailureCallback(String errorMessage);
}
