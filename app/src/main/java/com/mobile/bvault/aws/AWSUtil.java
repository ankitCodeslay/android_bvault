package com.mobile.bvault.aws;

import android.content.Context;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.mobile.bvault.BuildConfig;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 25/07/16.
 */
public class AWSUtil {

    private AmazonS3 s3;
    private TransferUtility transferUtility;
    private Context context;
    private AWSCallback callback;

    private static final String TAG = "AWSUtil";

    public static final String S3_BUCKET = "bvault";
    public static final String strRegion = Regions.AP_SOUTH_1.getName();

    public AWSUtil(Context context, AWSCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    public TransferObserver download(String bucket, String objectKey, File file) {
        TransferObserver transferObserver = transferUtility.download(bucket, objectKey, file);
        return transferObserver;
    }

    public void uploadByKey(final String bucketPath, final File file, final String fileName) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            callback.onAWSFailureCallback(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<AWS> awsCall = userApiInterface.getAWSAccessKey(userId);
        awsCall.enqueue(new Callback<AWS>() {
            @Override
            public void onResponse(Call<AWS> call, Response<AWS> response) {
                if (response.isSuccessful()) {
                    AWS aws = response.body();
                    BasicAWSCredentials credentials = new BasicAWSCredentials(aws.getAwsAccessKey(), aws.getAwsSecretKey());
                    s3 = new AmazonS3Client(credentials);
                    s3.setRegion(Region.getRegion(Regions.AP_SOUTH_1));
                    transferUtility = new TransferUtility(s3, context);
                    upload(bucketPath, file, fileName);
                }else {
                    callback.onAWSFailureCallback(response.message());
                }
            }

            @Override
            public void onFailure(Call<AWS> call, Throwable t) {
                callback.onAWSFailureCallback(t.getMessage());
            }
        });
    }


    private void upload(String bucketPath, final File file, final String fileName) {

        final TransferObserver transferObserver = transferUtility.upload(S3_BUCKET, bucketPath, file);

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED){
                    CommonUtils.printErrorLog(TAG,"Completed");
                    callback.onAWSSuccessCallback(fileName,file.getPath());
                }else if (state == TransferState.FAILED){
                    CommonUtils.printErrorLog(TAG,"Failed");
                    callback.onAWSFailureCallback("Failed to upload the image on server");
                }else if (state == TransferState.CANCELED){
                    CommonUtils.printErrorLog(TAG,"Canceled");
                    callback.onAWSFailureCallback("Failed to upload the image on server");
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                CommonUtils.printErrorLog(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                        id, bytesTotal, bytesCurrent));
            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
                callback.onAWSFailureCallback(ex.getMessage());
            }
        });
    }

    public static String getProfileBucketPath(String fileName){
        return "ProfilePictures/"+fileName;
    }

    public static String generateAmazonImageURL(String folder,String fileName){
        return "https://s3-"+strRegion+".amazonaws.com/"+S3_BUCKET+"/"+folder+"/"+ encodeURIComponent(fileName);
    }

    public static String encodeURIComponent(String s) {
        String result;

        try {
            result = s
                    .replaceAll(" ", "%20");
//                    .replaceAll("\\%21", "!")
//                    .replaceAll("\\%27", "'")
//                    .replaceAll("\\%28", "(")
//                    .replaceAll("\\%29", ")")
//                    .replaceAll("\\%7E", "~");
        } catch (Exception e) {
            result = s;
        }

        return result;
    }
}