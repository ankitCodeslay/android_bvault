package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ravi on 2/22/2018.
 */

public class AppVersion implements Parcelable{

    private int versionCode;

    private String message;

    protected AppVersion(Parcel in) {
        versionCode = in.readInt();
        message = in.readString();
    }

    public static final Creator<AppVersion> CREATOR = new Creator<AppVersion>() {
        @Override
        public AppVersion createFromParcel(Parcel in) {
            return new AppVersion(in);
        }

        @Override
        public AppVersion[] newArray(int size) {
            return new AppVersion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(versionCode);
        dest.writeString(message);
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
