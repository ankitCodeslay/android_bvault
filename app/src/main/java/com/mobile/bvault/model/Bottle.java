package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diptif on 21/08/17.
 */

public class Bottle implements Parcelable {

    @SerializedName("_id")
    @Expose
    String bottleId;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("volume")
    @Expose
    int volume;

    @SerializedName("pegs")
    @Expose
    float pegs;

    @SerializedName("imageUrl")
    @Expose
    String imageURL;

    @SerializedName("price")
    @Expose
    List<Price> prices;

    @SerializedName("brand")
    @Expose
    String brand;

    @SerializedName("description")
    @Expose
    String description;

    @SerializedName("unit")
    @Expose
    String unit;

    @Expose
    String saveUpto;

    public Bottle(String bottleId, String name, int volume, int pegs, String imageURL, List<Price> prices, String brand, String description,String unit,String saveUpto) {
        this.bottleId = bottleId;
        this.name = name;
        this.volume = volume;
        this.pegs = pegs;
        this.imageURL = imageURL;
        this.prices = prices;
        this.brand = brand;
        this.description = description;
        this.unit = unit;
        this.saveUpto = saveUpto;
    }

    public String getBottleId() {
        return bottleId;
    }

    public void setBottleId(String bottleId) {
        this.bottleId = bottleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public float getPegs() {
        return pegs;
    }

    public void setPegs(int pegs) {
        this.pegs = pegs;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSaveUpto() {
        return saveUpto;
    }

    public void setSaveUpto(String saveUpto) {
        this.saveUpto = saveUpto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bottleId);
        dest.writeString(this.name);
        dest.writeInt(this.volume);
        dest.writeFloat(this.pegs);
        dest.writeString(this.imageURL);
        dest.writeTypedList(this.prices);
        dest.writeString(this.brand);
        dest.writeString(this.description);
        dest.writeString(this.unit);
        dest.writeString(this.saveUpto);
    }

    protected Bottle(Parcel in) {
        this.bottleId = in.readString();
        this.name = in.readString();
        this.volume = in.readInt();
        this.pegs = in.readFloat();
        this.imageURL = in.readString();
        this.prices = in.createTypedArrayList(Price.CREATOR);
        this.brand = in.readString();
        this.description = in.readString();
        this.unit = in.readString();
        this.saveUpto = in.readString();
    }

    public static final Creator<Bottle> CREATOR = new Creator<Bottle>() {
        @Override
        public Bottle createFromParcel(Parcel source) {
            return new Bottle(source);
        }

        @Override
        public Bottle[] newArray(int size) {
            return new Bottle[size];
        }
    };
}
