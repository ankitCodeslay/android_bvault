package com.mobile.bvault.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 04/10/17.
 */

public class HistoryObject {

    @SerializedName("bottle")
    Bottle bottle;

    @SerializedName("hotel")
    Hotel hotel;

    @SerializedName("uniqueCode")
    String uniqueCode;

    @SerializedName("consumedVolume")
    int consumedVolume;

    @SerializedName("totalVolume")
    int totalVolume;

    @SerializedName("purchasedType")
    String purchaseType;

    public HistoryObject(Bottle bottle, Hotel hotel, String uniqueCode, int consumedVolume, int totalVolume, String purchaseType) {
        this.bottle = bottle;
        this.hotel = hotel;
        this.uniqueCode = uniqueCode;
        this.consumedVolume = consumedVolume;
        this.totalVolume = totalVolume;
        this.purchaseType = purchaseType;
    }

    public Bottle getBottle() {
        return bottle;
    }

    public void setBottle(Bottle bottle) {
        this.bottle = bottle;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public int getConsumedVolume() {
        return consumedVolume;
    }

    public void setConsumedVolume(int consumedVolume) {
        this.consumedVolume = consumedVolume;
    }

    public int getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(int totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
