package com.mobile.bvault.model;

/**
 * Created by diptif on 23/10/17.
 */

public class HomeOption {

    Class activityName;
    String title;
    int image;

    public HomeOption(Class activityName, String title, int image) {
        this.activityName = activityName;
        this.title = title;
        this.image = image;
    }

    public Class getActivityName() {
        return activityName;
    }

    public void setActivityName(Class activityName) {
        this.activityName = activityName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
