package com.mobile.bvault.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 25/10/17.
 */

public class PaymentResponse {
    @SerializedName("_id")
    String paymentId;

    @SerializedName("user")
    String user;

    @SerializedName("hotel")
    String hotel;

    @SerializedName("bottle")
    String bottle;

    @SerializedName("created")
    String createdDate;

    @SerializedName("lastUpdated")
    String lastUpdated;

    @SerializedName("status")
    String status;

    @SerializedName("quantity")
    int quantity;


    public PaymentResponse(String paymentId, String user, String hotel, String bottle, String createdDate, String lastUpdated, String status, int quantity) {
        this.paymentId = paymentId;
        this.user = user;
        this.hotel = hotel;
        this.bottle = bottle;
        this.createdDate = createdDate;
        this.lastUpdated = lastUpdated;
        this.status = status;
        this.quantity = quantity;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getBottle() {
        return bottle;
    }

    public void setBottle(String bottle) {
        this.bottle = bottle;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
