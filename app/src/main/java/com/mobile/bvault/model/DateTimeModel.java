package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DateTimeModel implements Parcelable{

    @SerializedName("date")
    private String currentDate;
    @SerializedName("time")
    private String currentTime;

    public DateTimeModel(){}
    protected DateTimeModel(Parcel in) {
        currentDate = in.readString();
        currentTime = in.readString();
    }

    public static final Creator<DateTimeModel> CREATOR = new Creator<DateTimeModel>() {
        @Override
        public DateTimeModel createFromParcel(Parcel in) {
            return new DateTimeModel(in);
        }

        @Override
        public DateTimeModel[] newArray(int size) {
            return new DateTimeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currentDate);
        dest.writeString(currentTime);
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }
}
