package com.mobile.bvault.model;

import java.util.List;

/**
 * Created by Ravi on 2/15/2018.
 */

public class BottleListModel {

    private static BottleListModel bottleListModel;

    private BottleListModel() {

    }

    public static BottleListModel getInstance() {
        if (bottleListModel == null) {
            bottleListModel = new BottleListModel();
        }
        return bottleListModel;
    }

    List<Bottle> bottleList;

    public List<Bottle> getBottleList() {
        return bottleList;
    }

    public void setBottleList(List<Bottle> bottleList) {
        this.bottleList = bottleList;
    }
}
