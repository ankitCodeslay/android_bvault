package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by codeslay3 on 3/17/2018.
 */

public class PromoCodeModel implements Parcelable {

    @SerializedName("promoCode")
    private String promoCode;
    @SerializedName("promoCodeAmount")
    private String promoCodeAmt;
    @SerializedName("creationTime")
    private String dateTime;
    private String description;
    @SerializedName("usedStatus")
    private int usedStatus;
    private String referType;
    private String userName;
    @SerializedName("referByUserId")
    private User referByUser;
    @SerializedName("referToUserId")
    private User referToUser;
    private String expiryDate;

    protected PromoCodeModel(Parcel in) {
        promoCode = in.readString();
        promoCodeAmt = in.readString();
        dateTime = in.readString();
        description = in.readString();
        usedStatus = in.readInt();
        referType = in.readString();
        userName = in.readString();
        expiryDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(promoCode);
        dest.writeString(promoCodeAmt);
        dest.writeString(dateTime);
        dest.writeString(description);
        dest.writeInt(usedStatus);
        dest.writeString(referType);
        dest.writeString(userName);
        dest.writeString(expiryDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PromoCodeModel> CREATOR = new Creator<PromoCodeModel>() {
        @Override
        public PromoCodeModel createFromParcel(Parcel in) {
            return new PromoCodeModel(in);
        }

        @Override
        public PromoCodeModel[] newArray(int size) {
            return new PromoCodeModel[size];
        }
    };

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCodeAmt() {
        return promoCodeAmt;
    }

    public void setPromoCodeAmt(String promoCodeAmt) {
        this.promoCodeAmt = promoCodeAmt;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUsedStatus() {
        return usedStatus;
    }

    public void setUsedStatus(int usedStatus) {
        this.usedStatus = usedStatus;
    }

    public String getReferType() {
        return referType;
    }

    public void setReferType(String referType) {
        this.referType = referType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User getReferByUser() {
        return referByUser;
    }

    public void setReferByUser(User referByUser) {
        this.referByUser = referByUser;
    }

    public User getReferToUser() {
        return referToUser;
    }

    public void setReferToUser(User referToUser) {
        this.referToUser = referToUser;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}
