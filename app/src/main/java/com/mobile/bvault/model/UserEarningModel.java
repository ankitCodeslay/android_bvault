package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserEarningModel implements Parcelable {

    int earnedValue;
    boolean isBottleEnabled;
    String userId;
    int redeemPoints;

    public UserEarningModel() {

    }

    protected UserEarningModel(Parcel in) {
        earnedValue = in.readInt();
        isBottleEnabled = in.readByte() != 0;
        userId = in.readString();
        redeemPoints = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(earnedValue);
        dest.writeByte((byte) (isBottleEnabled ? 1 : 0));
        dest.writeString(userId);
        dest.writeInt(redeemPoints);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserEarningModel> CREATOR = new Creator<UserEarningModel>() {
        @Override
        public UserEarningModel createFromParcel(Parcel in) {
            return new UserEarningModel(in);
        }

        @Override
        public UserEarningModel[] newArray(int size) {
            return new UserEarningModel[size];
        }
    };

    public int getEarnedValue() {
        return earnedValue;
    }

    public void setEarnedValue(int earnedValue) {
        this.earnedValue = earnedValue;
    }

    public boolean isBottleEnabled() {
        return isBottleEnabled;
    }

    public void setBottleEnabled(boolean bottleEnabled) {
        isBottleEnabled = bottleEnabled;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(int redeemPoints) {
        this.redeemPoints = redeemPoints;
    }
}