package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 31/08/17.
 */

public class Rack implements Parcelable {

    @SerializedName("_id")
    @Expose
    String rackId;

    @SerializedName("bottle")
    @Expose
    Bottle bottle;

    @SerializedName("uniqueCode")
    @Expose
    String uniqueCode;

    @SerializedName("remainingVolume")
    @Expose
    int remainingVolume;

    @SerializedName("totalVolume")
    @Expose
    int totalVolume;

    @SerializedName("uniqueCodeExpiry")
    @Expose
    String expiryDate;

    public Rack(String rackId, Bottle bottle, String uniqueCode, int remainingVolume, int totalVolume, String expiryDate) {
        this.rackId = rackId;
        this.bottle = bottle;
        this.uniqueCode = uniqueCode;
        this.remainingVolume = remainingVolume;
        this.totalVolume = totalVolume;
        this.expiryDate = expiryDate;
    }

    public String getRackId() {
        return rackId;
    }

    public void setRackId(String rackId) {
        this.rackId = rackId;
    }

    public Bottle getBottle() {
        return bottle;
    }

    public void setBottle(Bottle bottle) {
        this.bottle = bottle;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public int getRemainingVolume() {
        return remainingVolume;
    }

    public void setRemainingVolume(int remainingVolume) {
        this.remainingVolume = remainingVolume;
    }

    public int getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(int totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.rackId);
        dest.writeParcelable(this.bottle, flags);
        dest.writeString(this.uniqueCode);
        dest.writeInt(this.remainingVolume);
        dest.writeInt(this.totalVolume);
        dest.writeString(this.expiryDate);
    }

    protected Rack(Parcel in) {
        this.rackId = in.readString();
        this.bottle = in.readParcelable(Bottle.class.getClassLoader());
        this.uniqueCode = in.readString();
        this.remainingVolume = in.readInt();
        this.totalVolume = in.readInt();
        this.expiryDate = in.readString();
    }

    public static final Creator<Rack> CREATOR = new Creator<Rack>() {
        @Override
        public Rack createFromParcel(Parcel source) {
            return new Rack(source);
        }

        @Override
        public Rack[] newArray(int size) {
            return new Rack[size];
        }
    };
}
