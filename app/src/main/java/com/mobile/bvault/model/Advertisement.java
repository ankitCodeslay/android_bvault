package com.mobile.bvault.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 11/09/17.
 */

public class Advertisement {

    @SerializedName("_id")
    @Expose
    String adsId;

    @SerializedName("imageUrl")
    String imageUrl;

    @SerializedName("startDate")
    String startDate;

    @SerializedName("endDate")
    String endDate;

    public Advertisement(String adsId, String imageUrl, String startDate, String endDate) {
        this.adsId = adsId;
        this.imageUrl = imageUrl;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getAdsId() {
        return adsId;
    }

    public void setAdsId(String adsId) {
        this.adsId = adsId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
