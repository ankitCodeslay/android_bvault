package com.mobile.bvault.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 31/08/17.
 */

public class PurchasedBottle {

    @SerializedName("bottle_id")
    @Expose
    String bottleId;

    @SerializedName("qty")
    @Expose
    int qty;

    @SerializedName("volume")
    @Expose
    int volume;

    @SerializedName("purchasedType")
    String purchasedType;

    @SerializedName("price")
    int price;

    public PurchasedBottle(String bottleId, int qty, int volume, String purchasedType, int price) {
        this.bottleId = bottleId;
        this.qty = qty;
        this.volume = volume;
        this.purchasedType = purchasedType;
        this.price = price;
    }

    public String getBottleId() {
        return bottleId;
    }

    public void setBottleId(String bottleId) {
        this.bottleId = bottleId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getPurchasedType() {
        return purchasedType;
    }

    public void setPurchasedType(String purchasedType) {
        this.purchasedType = purchasedType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
