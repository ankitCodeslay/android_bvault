package com.mobile.bvault.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diptif on 31/08/17.
 */

public class PurchaseRequest {

    @SerializedName("userId")
    @Expose
    String userId;

    @SerializedName("bottles")
    @Expose
    List<PurchasedBottle> bottles;

    @SerializedName("promoCodeId")
    @Expose
    String promoCodeId;

    @SerializedName("isValid")
    boolean isValid;

    int bottleRedeemPoints;

    public PurchaseRequest(String userId, List<PurchasedBottle> bottles, String promoCodeId, boolean isValid, int bottleRedeemPoints) {
        this.userId = userId;
        this.bottles = bottles;
        this.promoCodeId = promoCodeId;
        this.isValid = isValid;
        this.bottleRedeemPoints = bottleRedeemPoints;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<PurchasedBottle> getBottles() {
        return bottles;
    }

    public void setBottles(List<PurchasedBottle> bottles) {
        this.bottles = bottles;
    }

    public String getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(String promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public int getBottleRedeemPoints() {
        return bottleRedeemPoints;
    }

    public void setBottleRedeemPoints(int bottleRedeemPoints) {
        this.bottleRedeemPoints = bottleRedeemPoints;
    }
}
