package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 10/10/17.
 */

public class PromoCode implements Parcelable{
    @SerializedName("_id")
    String promoCodeId;

    @SerializedName("promoCode")
    String promoCode;

    @SerializedName("startDate")
    String startDate;

    @SerializedName("endDate")
    String endDate;

    @SerializedName("discount")
    float discount;

    @SerializedName("minAmmountForDiscount")
    float minAmountForDiscount;

    public PromoCode() {

    }

    public PromoCode(String promoCodeId, String promoCode, String startDate, String endDate, float discount, float minAmountForDiscount) {
        this.promoCodeId = promoCodeId;
        this.promoCode = promoCode;
        this.startDate = startDate;
        this.endDate = endDate;
        this.discount = discount;
        this.minAmountForDiscount = minAmountForDiscount;
    }

    protected PromoCode(Parcel in) {
        promoCodeId = in.readString();
        promoCode = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        discount = in.readFloat();
        minAmountForDiscount = in.readFloat();
    }

    public static final Creator<PromoCode> CREATOR = new Creator<PromoCode>() {
        @Override
        public PromoCode createFromParcel(Parcel in) {
            return new PromoCode(in);
        }

        @Override
        public PromoCode[] newArray(int size) {
            return new PromoCode[size];
        }
    };

    public String getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(String promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getMinAmountForDiscount() {
        return minAmountForDiscount;
    }

    public void setMinAmountForDiscount(float minAmountForDiscount) {
        this.minAmountForDiscount = minAmountForDiscount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(promoCodeId);
        dest.writeString(promoCode);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeFloat(discount);
        dest.writeFloat(minAmountForDiscount);
    }
}
