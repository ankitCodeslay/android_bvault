package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 07/09/17.
 */

public class Hotel implements Parcelable {

    @SerializedName("_id")
    String hotelId;

    @SerializedName("phone")
    String phone;

    @SerializedName("lat")
    Double lat;

    @SerializedName("lng")
    Double lng;

    @SerializedName("admin")
    String adminId;

    @SerializedName("name")
    String name;

    @SerializedName("description")
    String desc;

    @SerializedName("imageUrl")
    String[] imageURL;

    @SerializedName("distance")
    float distance;

    @SerializedName("address")
    String address;

    @SerializedName("type")
    String type;

    @SerializedName("city")
    String city;

    @SerializedName("music")
    String music = "";

    @SerializedName("cuisine")
    String cuisine = "";

    @SerializedName("cost")
    double cost;


    @SerializedName("taxPercentage")
    int taxPercentage;

    String moreInfo;

    String menuDescription;


    public Hotel(String hotelId, String phone, Double lat, Double lng, String adminId, String name, String desc, String[] imageURL, float distance, String address, String type, String city, String music, String cuisine, double cost, int taxPercentage, String moreInfo, String menuDescription) {
        this.hotelId = hotelId;
        this.phone = phone;
        this.lat = lat;
        this.lng = lng;
        this.adminId = adminId;
        this.name = name;
        this.desc = desc;
        this.imageURL = imageURL;
        this.distance = distance;
        this.address = address;
        this.type = type;
        this.city = city;
        this.music = music;
        this.cuisine = cuisine;
        this.cost = cost;
        this.taxPercentage = taxPercentage;
        this.moreInfo = moreInfo;
        this.menuDescription = menuDescription;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String[] getImageURL() {
        return imageURL;
    }

    public void setImageURL(String[] imageURL) {
        this.imageURL = imageURL;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public int getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(int taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public String getMenuDescription() {
        return menuDescription;
    }

    public void setMenuDescription(String menuDescription) {
        this.menuDescription = menuDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.hotelId);
        dest.writeString(this.phone);
        dest.writeValue(this.lat);
        dest.writeValue(this.lng);
        dest.writeString(this.adminId);
        dest.writeString(this.name);
        dest.writeString(this.desc);
        dest.writeStringArray(this.imageURL);
        dest.writeFloat(this.distance);
        dest.writeString(this.address);
        dest.writeString(this.type);
        dest.writeString(this.city);
        dest.writeString(this.music);
        dest.writeString(this.cuisine);
        dest.writeDouble(this.cost);
        dest.writeInt(this.taxPercentage);
        dest.writeString(this.moreInfo);
        dest.writeString(this.menuDescription);
    }

    protected Hotel(Parcel in) {
        this.hotelId = in.readString();
        this.phone = in.readString();
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
        this.adminId = in.readString();
        this.name = in.readString();
        this.desc = in.readString();
        this.imageURL = in.createStringArray();
        this.distance = in.readFloat();
        this.address = in.readString();
        this.type = in.readString();
        this.city = in.readString();
        this.music = in.readString();
        this.cuisine = in.readString();
        this.cost = in.readDouble();
        this.taxPercentage = in.readInt();
        this.moreInfo = in.readString();
        this.menuDescription = in.readString();
    }

    public static final Creator<Hotel> CREATOR = new Creator<Hotel>() {
        @Override
        public Hotel createFromParcel(Parcel source) {
            return new Hotel(source);
        }

        @Override
        public Hotel[] newArray(int size) {
            return new Hotel[size];
        }
    };
}
