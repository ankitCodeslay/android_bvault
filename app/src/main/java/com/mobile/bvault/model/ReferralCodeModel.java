package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ReferralCodeModel implements Parcelable {

    private String referralCode;

    protected ReferralCodeModel(Parcel in) {
        referralCode = in.readString();
    }

    public ReferralCodeModel() {

    }

    public static final Creator<ReferralCodeModel> CREATOR = new Creator<ReferralCodeModel>() {
        @Override
        public ReferralCodeModel createFromParcel(Parcel in) {
            return new ReferralCodeModel(in);
        }

        @Override
        public ReferralCodeModel[] newArray(int size) {
            return new ReferralCodeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(referralCode);
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}
