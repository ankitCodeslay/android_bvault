package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class UploadContactModel implements Parcelable {

    private String userId;
    private List<ContactModel> contacts;

    public UploadContactModel(){

    }

    protected UploadContactModel(Parcel in) {
        userId = in.readString();
        contacts = in.createTypedArrayList(ContactModel.CREATOR);
    }

    public static final Creator<UploadContactModel> CREATOR = new Creator<UploadContactModel>() {
        @Override
        public UploadContactModel createFromParcel(Parcel in) {
            return new UploadContactModel(in);
        }

        @Override
        public UploadContactModel[] newArray(int size) {
            return new UploadContactModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeTypedList(contacts);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<ContactModel> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactModel> contacts) {
        this.contacts = contacts;
    }
}
