package com.mobile.bvault.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 21/08/17.
 */

public class DrinkType {

    @SerializedName("_id")
    @Expose
    String typeId;

    @SerializedName("type")
    @Expose
    String type;

    public DrinkType(String typeId, String type) {
        this.typeId = typeId;
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
