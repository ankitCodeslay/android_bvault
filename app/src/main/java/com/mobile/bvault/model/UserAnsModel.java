package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserAnsModel implements Parcelable {

    @SerializedName("matchId")
    private String matchId;
    @SerializedName("userAnsOne")
    private String ansOne;
    @SerializedName("userAnsTwo")
    private String ansTwo;
    @SerializedName("userAnsThree")
    private String ansThree;
    private String userId;

    public UserAnsModel() {

    }

    protected UserAnsModel(Parcel in) {
        matchId = in.readString();
        ansOne = in.readString();
        ansTwo = in.readString();
        ansThree = in.readString();
        userId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(matchId);
        dest.writeString(ansOne);
        dest.writeString(ansTwo);
        dest.writeString(ansThree);
        dest.writeString(userId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserAnsModel> CREATOR = new Creator<UserAnsModel>() {
        @Override
        public UserAnsModel createFromParcel(Parcel in) {
            return new UserAnsModel(in);
        }

        @Override
        public UserAnsModel[] newArray(int size) {
            return new UserAnsModel[size];
        }
    };

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getAnsOne() {
        return ansOne;
    }

    public void setAnsOne(String ansOne) {
        this.ansOne = ansOne;
    }

    public String getAnsTwo() {
        return ansTwo;
    }

    public void setAnsTwo(String ansTwo) {
        this.ansTwo = ansTwo;
    }

    public String getAnsThree() {
        return ansThree;
    }

    public void setAnsThree(String ansThree) {
        this.ansThree = ansThree;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
