package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IPLMatchModel implements Parcelable {

    @SerializedName("matchId")
    private String id;
    @SerializedName("startDate")
    private String matchStartDate;
    @SerializedName("startTime")
    private String matchStartTime;
    private String teamOne;
    private String teamTwo;
    private String teamOneId;
    private String teamTwoId;
    private String teamOneImageUrl;
    private String teamTwoImageUrl;

    protected IPLMatchModel(Parcel in) {
        id = in.readString();
        matchStartDate = in.readString();
        matchStartTime = in.readString();
        teamOne = in.readString();
        teamTwo = in.readString();
        teamOneId = in.readString();
        teamTwoId = in.readString();
        teamOneImageUrl = in.readString();
        teamTwoImageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(matchStartDate);
        dest.writeString(matchStartTime);
        dest.writeString(teamOne);
        dest.writeString(teamTwo);
        dest.writeString(teamOneId);
        dest.writeString(teamTwoId);
        dest.writeString(teamOneImageUrl);
        dest.writeString(teamTwoImageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IPLMatchModel> CREATOR = new Creator<IPLMatchModel>() {
        @Override
        public IPLMatchModel createFromParcel(Parcel in) {
            return new IPLMatchModel(in);
        }

        @Override
        public IPLMatchModel[] newArray(int size) {
            return new IPLMatchModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMatchStartDate() {
        return matchStartDate;
    }

    public void setMatchStartDate(String matchStartDate) {
        this.matchStartDate = matchStartDate;
    }

    public String getMatchStartTime() {
        return matchStartTime;
    }

    public void setMatchStartTime(String matchStartTime) {
        this.matchStartTime = matchStartTime;
    }

    public String getTeamOne() {
        return teamOne;
    }

    public void setTeamOne(String teamOne) {
        this.teamOne = teamOne;
    }

    public String getTeamTwo() {
        return teamTwo;
    }

    public void setTeamTwo(String teamTwo) {
        this.teamTwo = teamTwo;
    }

    public String getTeamOneId() {
        return teamOneId;
    }

    public void setTeamOneId(String teamOneId) {
        this.teamOneId = teamOneId;
    }

    public String getTeamTwoId() {
        return teamTwoId;
    }

    public void setTeamTwoId(String teamTwoId) {
        this.teamTwoId = teamTwoId;
    }

    public String getTeamOneImageUrl() {
        return teamOneImageUrl;
    }

    public void setTeamOneImageUrl(String teamOneImageUrl) {
        this.teamOneImageUrl = teamOneImageUrl;
    }

    public String getTeamTwoImageUrl() {
        return teamTwoImageUrl;
    }

    public void setTeamTwoImageUrl(String teamTwoImageUrl) {
        this.teamTwoImageUrl = teamTwoImageUrl;
    }
}
