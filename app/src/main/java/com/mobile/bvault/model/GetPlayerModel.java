package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GetPlayerModel implements Parcelable {

    private String teamOneId;
    private String teamTwoId;

    public GetPlayerModel() {

    }

    protected GetPlayerModel(Parcel in) {
        teamOneId = in.readString();
        teamTwoId = in.readString();
    }

    public static final Creator<GetPlayerModel> CREATOR = new Creator<GetPlayerModel>() {
        @Override
        public GetPlayerModel createFromParcel(Parcel in) {
            return new GetPlayerModel(in);
        }

        @Override
        public GetPlayerModel[] newArray(int size) {
            return new GetPlayerModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(teamOneId);
        dest.writeString(teamTwoId);
    }

    public String getTeamOneId() {
        return teamOneId;
    }

    public void setTeamOneId(String teamOneId) {
        this.teamOneId = teamOneId;
    }

    public String getTeamTwoId() {
        return teamTwoId;
    }

    public void setTeamTwoId(String teamTwoId) {
        this.teamTwoId = teamTwoId;
    }
}
