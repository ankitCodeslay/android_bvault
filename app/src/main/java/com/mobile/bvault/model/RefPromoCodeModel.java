package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RefPromoCodeModel implements Parcelable {

    private String userId;
    private String promoCode;
    private boolean isValid;
    @SerializedName("amount")
    float discount;

    public RefPromoCodeModel() {

    }

    protected RefPromoCodeModel(Parcel in) {
        userId = in.readString();
        promoCode = in.readString();
        isValid = in.readByte() != 0;
        discount = in.readFloat();
    }

    public static final Creator<RefPromoCodeModel> CREATOR = new Creator<RefPromoCodeModel>() {
        @Override
        public RefPromoCodeModel createFromParcel(Parcel in) {
            return new RefPromoCodeModel(in);
        }

        @Override
        public RefPromoCodeModel[] newArray(int size) {
            return new RefPromoCodeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(promoCode);
        dest.writeByte((byte) (isValid ? 1 : 0));
        dest.writeFloat(discount);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }
}
