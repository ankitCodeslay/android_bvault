package com.mobile.bvault.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 27/09/17.
 */

public class History {

    @SerializedName("_id")
    String historyId;

    @SerializedName("date")
    String date;

    @SerializedName("action")
    String action;

    @SerializedName("object")
    HistoryObject  historyObject;

    @SerializedName("created")
    String createdDate;

    public History(String historyId, String date, String action, HistoryObject historyObject, String createdDate) {
        this.historyId = historyId;
        this.date = date;
        this.action = action;
        this.historyObject = historyObject;
        this.createdDate = createdDate;
    }

    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public HistoryObject getHistoryObject() {
        return historyObject;
    }

    public void setHistoryObject(HistoryObject historyObject) {
        this.historyObject = historyObject;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
