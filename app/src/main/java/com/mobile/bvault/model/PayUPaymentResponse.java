package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 12/01/18.
 */

public class PayUPaymentResponse{

    @SerializedName("payUResId")
    String payUResId;

    @SerializedName("email")
    String email;

    @SerializedName("name")
    String name;

    @SerializedName("phone")
    String phone;

    @SerializedName("txnId")
    String txnId;

    @SerializedName("status")
    String status;

    @SerializedName("amount")
    double amount;

    @SerializedName("purchaseItems")
    PurchaseRequest purchaseRequest;


    public PayUPaymentResponse(String payUResId, String email, String name, String phone, String txnId, String status, double amount, PurchaseRequest purchaseRequest) {
        this.payUResId = payUResId;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.txnId = txnId;
        this.status = status;
        this.amount = amount;
        this.purchaseRequest = purchaseRequest;
    }

    public String getPayUResId() {
        return payUResId;
    }

    public void setPayUResId(String payUResId) {
        this.payUResId = payUResId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public PurchaseRequest getPurchaseRequest() {
        return purchaseRequest;
    }

    public void setPurchaseRequest(PurchaseRequest purchaseRequest) {
        this.purchaseRequest = purchaseRequest;
    }
}
