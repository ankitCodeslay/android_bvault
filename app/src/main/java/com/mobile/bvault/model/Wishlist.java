package com.mobile.bvault.model;

/**
 * Created by diptif on 28/08/17.
 */

public class Wishlist {

    private int wishlistId;

    private String bottleId;

    private String name;

    private int price;

    private String description;

    private String imageURL;

    private int qty;

    private int volume;

    private String type;

    private String unit;

    public Wishlist(int wishlistId, String bottleId, String name, int price, String description, String imageURL, int qty, int volume, String type, String unit) {
        this.wishlistId = wishlistId;
        this.bottleId = bottleId;
        this.name = name;
        this.price = price;
        this.description = description;
        this.imageURL = imageURL;
        this.qty = qty;
        this.volume = volume;
        this.type = type;
        this.unit = unit;
    }

    public int getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(int wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getBottleId() {
        return bottleId;
    }

    public void setBottleId(String bottleId) {
        this.bottleId = bottleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
