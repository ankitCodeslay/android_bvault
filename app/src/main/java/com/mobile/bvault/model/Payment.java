package com.mobile.bvault.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 25/10/17.
 */

public class Payment {

    @SerializedName("_id")
    String paymentId;

    @SerializedName("user")
    User user;

    @SerializedName("hotel")
    Hotel hotel;

    @SerializedName("bottle")
    Bottle bottle;

    @SerializedName("created")
    String createdDate;

    @SerializedName("lastUpdated")
    String lastUpdated;

    @SerializedName("status")
    String status;

    @SerializedName("quantity")
    int quantity;

    @SerializedName("uniqueCode")
    String uniqueCode;

    @SerializedName("cost")
    double cost;

    public Payment(String paymentId, User user, Hotel hotel, Bottle bottle, String createdDate, String lastUpdated, String status, int quantity, String uniqueCode, double cost) {
        this.paymentId = paymentId;
        this.user = user;
        this.hotel = hotel;
        this.bottle = bottle;
        this.createdDate = createdDate;
        this.lastUpdated = lastUpdated;
        this.status = status;
        this.quantity = quantity;
        this.uniqueCode = uniqueCode;
        this.cost = cost;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Bottle getBottle() {
        return bottle;
    }

    public void setBottle(Bottle bottle) {
        this.bottle = bottle;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
