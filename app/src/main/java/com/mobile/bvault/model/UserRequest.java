package com.mobile.bvault.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 24/11/17.
 */

public class UserRequest {

    @SerializedName("firstName")
    String firstName;

    @SerializedName("lastName")
    String lastName;

    @SerializedName("email")
    String email;

    @SerializedName("username")
    String userName;

    @SerializedName("password")
    String password;

    @SerializedName("phone")
    String phone;

    @SerializedName("dateOfBirth")
    String dateOfBirth;

    @SerializedName("isOtherLogin")
    boolean isOtherLogin = false;

    private String referralCode;

    public UserRequest(){

    }

    public UserRequest(String firstName, String lastName, String email, String userName, String password, String phone, String dateOfBirth, boolean isOtherLogin,String referralCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.phone = phone;
        this.dateOfBirth = dateOfBirth;
        this.isOtherLogin = isOtherLogin;
        this.referralCode = referralCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isOtherLogin() {
        return isOtherLogin;
    }

    public void setOtherLogin(boolean otherLogin) {
        isOtherLogin = otherLogin;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}

