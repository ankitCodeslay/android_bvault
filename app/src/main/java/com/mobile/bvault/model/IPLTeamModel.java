package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IPLTeamModel implements Parcelable {

    @SerializedName("teamOneIdPlayers")
    private List<PlayerModel> teamOnePlayers;
    @SerializedName("teamtwoIdPlayers")
    private List<PlayerModel> teamtwoPlayers;

    protected IPLTeamModel(Parcel in) {
        teamOnePlayers = in.createTypedArrayList(PlayerModel.CREATOR);
        teamtwoPlayers = in.createTypedArrayList(PlayerModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(teamOnePlayers);
        dest.writeTypedList(teamtwoPlayers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IPLTeamModel> CREATOR = new Creator<IPLTeamModel>() {
        @Override
        public IPLTeamModel createFromParcel(Parcel in) {
            return new IPLTeamModel(in);
        }

        @Override
        public IPLTeamModel[] newArray(int size) {
            return new IPLTeamModel[size];
        }
    };

    public List<PlayerModel> getTeamOnePlayers() {
        return teamOnePlayers;
    }

    public void setTeamOnePlayers(List<PlayerModel> teamOnePlayers) {
        this.teamOnePlayers = teamOnePlayers;
    }

    public List<PlayerModel> getTeamtwoPlayers() {
        return teamtwoPlayers;
    }

    public void setTeamtwoPlayers(List<PlayerModel> teamtwoPlayers) {
        this.teamtwoPlayers = teamtwoPlayers;
    }
}
