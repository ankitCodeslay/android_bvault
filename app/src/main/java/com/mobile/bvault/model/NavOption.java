package com.mobile.bvault.model;

/**
 * Created by diptif on 11/09/17.
 */

public class NavOption {

    Class activityName;
    String title;

    public NavOption(Class activityName, String title) {
        this.activityName = activityName;
        this.title = title;
    }

    public Class getActivityName() {
        return activityName;
    }

    public void setActivityName(Class activityName) {
        this.activityName = activityName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
