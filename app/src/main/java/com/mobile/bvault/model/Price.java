package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diptif on 19/09/17.
 */

public class Price implements Parcelable {

    @SerializedName("label")
    private String label;

    @SerializedName("price")
    private int price;

    @SerializedName("volume")
    private int volume;

    public Price(String label, int price, int volume) {
        this.label = label;
        this.price = price;
        this.volume = volume;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.label);
        dest.writeInt(this.price);
        dest.writeInt(this.volume);
    }

    protected Price(Parcel in) {
        this.label = in.readString();
        this.price = in.readInt();
        this.volume = in.readInt();
    }

    public static final Parcelable.Creator<Price> CREATOR = new Parcelable.Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel source) {
            return new Price(source);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };
}
