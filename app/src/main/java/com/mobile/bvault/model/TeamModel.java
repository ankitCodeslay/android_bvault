package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TeamModel implements Parcelable {

    private String teamId;
    private String teamName;

    public TeamModel(String teamId, String teamName) {
        this.teamId = teamId;
        this.teamName = teamName;
    }

    protected TeamModel(Parcel in) {
        teamId = in.readString();
        teamName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(teamId);
        dest.writeString(teamName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TeamModel> CREATOR = new Creator<TeamModel>() {
        @Override
        public TeamModel createFromParcel(Parcel in) {
            return new TeamModel(in);
        }

        @Override
        public TeamModel[] newArray(int size) {
            return new TeamModel[size];
        }
    };

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
