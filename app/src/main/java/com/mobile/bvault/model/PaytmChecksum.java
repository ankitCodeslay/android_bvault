package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaytmChecksum implements Parcelable{

    String MID;
    String ORDER_ID;
    String CUST_ID;
    String INDUSTRY_TYPE_ID;
    String CHANNEL_ID;
    String TXN_AMOUNT;
    String WEBSITE;
    String CALLBACK_URL;
    String EMAIL;
    String MOBILE_NO;
    String CHECKSUMHASH;
    String MERC_UNQ_REF;

    public PaytmChecksum(){

    }

    protected PaytmChecksum(Parcel in) {
        MID = in.readString();
        ORDER_ID = in.readString();
        CUST_ID = in.readString();
        INDUSTRY_TYPE_ID = in.readString();
        CHANNEL_ID = in.readString();
        TXN_AMOUNT = in.readString();
        WEBSITE = in.readString();
        CALLBACK_URL = in.readString();
        EMAIL = in.readString();
        MOBILE_NO = in.readString();
        CHECKSUMHASH = in.readString();
        MERC_UNQ_REF = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MID);
        dest.writeString(ORDER_ID);
        dest.writeString(CUST_ID);
        dest.writeString(INDUSTRY_TYPE_ID);
        dest.writeString(CHANNEL_ID);
        dest.writeString(TXN_AMOUNT);
        dest.writeString(WEBSITE);
        dest.writeString(CALLBACK_URL);
        dest.writeString(EMAIL);
        dest.writeString(MOBILE_NO);
        dest.writeString(CHECKSUMHASH);
        dest.writeString(MERC_UNQ_REF);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaytmChecksum> CREATOR = new Creator<PaytmChecksum>() {
        @Override
        public PaytmChecksum createFromParcel(Parcel in) {
            return new PaytmChecksum(in);
        }

        @Override
        public PaytmChecksum[] newArray(int size) {
            return new PaytmChecksum[size];
        }
    };

    public String getMID() {
        return MID;
    }

    public void setMID(String MID) {
        this.MID = MID;
    }

    public String getORDER_ID() {
        return ORDER_ID;
    }

    public void setORDER_ID(String ORDER_ID) {
        this.ORDER_ID = ORDER_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getINDUSTRY_TYPE_ID() {
        return INDUSTRY_TYPE_ID;
    }

    public void setINDUSTRY_TYPE_ID(String INDUSTRY_TYPE_ID) {
        this.INDUSTRY_TYPE_ID = INDUSTRY_TYPE_ID;
    }

    public String getCHANNEL_ID() {
        return CHANNEL_ID;
    }

    public void setCHANNEL_ID(String CHANNEL_ID) {
        this.CHANNEL_ID = CHANNEL_ID;
    }

    public String getTXN_AMOUNT() {
        return TXN_AMOUNT;
    }

    public void setTXN_AMOUNT(String TXN_AMOUNT) {
        this.TXN_AMOUNT = TXN_AMOUNT;
    }

    public String getWEBSITE() {
        return WEBSITE;
    }

    public void setWEBSITE(String WEBSITE) {
        this.WEBSITE = WEBSITE;
    }

    public String getCALLBACK_URL() {
        return CALLBACK_URL;
    }

    public void setCALLBACK_URL(String CALLBACK_URL) {
        this.CALLBACK_URL = CALLBACK_URL;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getMOBILE_NO() {
        return MOBILE_NO;
    }

    public void setMOBILE_NO(String MOBILE_NO) {
        this.MOBILE_NO = MOBILE_NO;
    }

    public String getCHECKSUMHASH() {
        return CHECKSUMHASH;
    }

    public void setCHECKSUMHASH(String CHECKSUMHASH) {
        this.CHECKSUMHASH = CHECKSUMHASH;
    }

    public String getMERC_UNQ_REF() {
        return MERC_UNQ_REF;
    }

    public void setMERC_UNQ_REF(String MERC_UNQ_REF) {
        this.MERC_UNQ_REF = MERC_UNQ_REF;
    }
}
