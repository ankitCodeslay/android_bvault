package com.mobile.bvault.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SubmitIPLAnsModel implements Parcelable {

    @SerializedName("matchId")
    private String matchId;
    @SerializedName("userAnsOne")
    private String ansOne;
    @SerializedName("userAnsTwo")
    private String ansTwo;
    @SerializedName("userAnsThree")
    private String ansThree;
    private String userId;

    @SerializedName("userAns")
    private UserAnsModel userAnsModel;

    public SubmitIPLAnsModel() {

    }


    protected SubmitIPLAnsModel(Parcel in) {
        matchId = in.readString();
        ansOne = in.readString();
        ansTwo = in.readString();
        ansThree = in.readString();
        userId = in.readString();
        userAnsModel = in.readParcelable(UserAnsModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(matchId);
        dest.writeString(ansOne);
        dest.writeString(ansTwo);
        dest.writeString(ansThree);
        dest.writeString(userId);
        dest.writeParcelable(userAnsModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubmitIPLAnsModel> CREATOR = new Creator<SubmitIPLAnsModel>() {
        @Override
        public SubmitIPLAnsModel createFromParcel(Parcel in) {
            return new SubmitIPLAnsModel(in);
        }

        @Override
        public SubmitIPLAnsModel[] newArray(int size) {
            return new SubmitIPLAnsModel[size];
        }
    };

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getAnsOne() {
        return ansOne;
    }

    public void setAnsOne(String ansOne) {
        this.ansOne = ansOne;
    }

    public String getAnsTwo() {
        return ansTwo;
    }

    public void setAnsTwo(String ansTwo) {
        this.ansTwo = ansTwo;
    }

    public String getAnsThree() {
        return ansThree;
    }

    public void setAnsThree(String ansThree) {
        this.ansThree = ansThree;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserAnsModel getUserAnsModel() {
        return userAnsModel;
    }

    public void setUserAnsModel(UserAnsModel userAnsModel) {
        this.userAnsModel = userAnsModel;
    }
}
