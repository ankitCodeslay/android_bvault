package com.mobile.bvault.presenter;

import android.content.Context;
import android.nfc.Tag;
import android.text.TextUtils;

import com.mobile.bvault.R;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.ReferralCodeModel;
import com.mobile.bvault.model.User;
import com.mobile.bvault.model.UserRequest;
import com.mobile.bvault.ui.view.SignupView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by diptif on 04/09/17.
 */

public class SignupPresenter {

    private Context context;
    private SignupView signupView;


    public SignupPresenter(Context context, SignupView signupView) {
        this.context = context;
        this.signupView = signupView;
    }

    public void validateUser(String userName, String email, String password, String phoneNumber, String dob) {
        if (userName.isEmpty()) {
            signupView.onValidationFail(context.getString(R.string.error_username_field_required));
        } else if (TextUtils.isEmpty(email)) {
            signupView.onValidationFail(context.getString(R.string.error_email_field_required));
        } else if (TextUtils.isEmpty(password)) {
            signupView.onValidationFail(context.getString(R.string.error_password_field_required));
        } else if (phoneNumber.isEmpty()) {
            signupView.onValidationFail(context.getString(R.string.error_phone_field_required));
        } else if (dob.isEmpty()) {
            signupView.onValidationFail(context.getString(R.string.error_dob_field_required));
        } else if (getDiffYears(dob) < 18) {
            signupView.onValidationFail(context.getString(R.string.error_dob_less));
        } else if (!phoneNumber.isEmpty() && phoneNumber.length() != 10) {
            signupView.onValidationFail(context.getString(R.string.error_phone_length));
        } else if (!TextUtils.isEmpty(password) && !CommonUtils.isPasswordValid(password)) {
            signupView.onValidationFail(context.getString(R.string.error_invalid_password));
        } else if (!CommonUtils.isEmailValid(email)) {
            signupView.onValidationFail(context.getString(R.string.error_invalid_email));
        } else {
            signupView.onValidationSuccess();
        }
    }

    private static int getDiffYears(String inputDateString) {
        Date inputDate = null;
        Date todayDate = null;
        int diff = 0;
        try {
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            inputDate = format.parse(inputDateString);

            todayDate = new Date();
            String todayDateString = format.format(todayDate);
            todayDate = format.parse(todayDateString);

            Calendar a = getCalendar(inputDate);
            Calendar b = getCalendar(todayDate);
            diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
            if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
                diff--;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diff;
    }

    private static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public void signup(String firstName, String lastName, String email, String username, String password, String phoneNumber, String dob, final boolean isOtherLogin, String referralCode) {
        CommonUtils.printErrorLog("test", username);
        if (!NetworkUtils.isNetworkConnected(context)) {
            signupView.onSignupFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        UserRequest userRequest = new UserRequest(firstName, lastName, email, username, password, phoneNumber, dob, isOtherLogin, referralCode);
        Call<User> userCall = userApiInterface.signupUser(userRequest);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    AppPreferencesHelper.getInstance(context).setUser(user);
                    AppPreferencesHelper.getInstance(context).setFacebookOrGoogleLogin(isOtherLogin);
                    signupView.onSignupSuccess(user);
                } else {
                    try {
                        String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                        signupView.onSignupFailure(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                        signupView.onSignupFailure(response.message());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        signupView.onSignupFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                signupView.onSignupFailure(t.getMessage());
            }
        });
    }


    public void sendOtp(String mobileNo, String otp) {
        if (!NetworkUtils.isNetworkConnected(context)) {
            signupView.onSignupFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        String message = otp + "%20is%20the%20OTP%20for%20your%20BVault%20account,%20kindly%20verify.";

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://sms.digimiles.in/")
                .build();
        String url = "http://sms.digimiles.in/bulksms/bulksms?username=di78-dreamtech&password=digimile&type=0&dlr=1&destination=" + mobileNo + "&source=BVault&message=" + message;
        UserApiInterface userApiInterface = retrofit.create(UserApiInterface.class);
        Call<String> stringCall = userApiInterface.sendOtpRequest(url);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void validateReferralCode(String referralCode) {
        if (!NetworkUtils.isNetworkConnected(context)) {
            signupView.onSignupFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        ReferralCodeModel referralCodeModel = new ReferralCodeModel();
        referralCodeModel.setReferralCode(referralCode);
        Call<ReferralCodeModel> userCall = userApiInterface.validateReferralCode(referralCodeModel);
        userCall.enqueue(new Callback<ReferralCodeModel>() {
            @Override
            public void onResponse(Call<ReferralCodeModel> call, Response<ReferralCodeModel> response) {
                if (response.isSuccessful()) {
                    signupView.onRefCodeValidationSuccess();
                } else {
                    signupView.onRefCodeValidationFail(context.getString(R.string.messageRefCodeFail));
                }
            }

            @Override
            public void onFailure(Call<ReferralCodeModel> call, Throwable t) {
                signupView.onRefCodeValidationFail(t.getMessage());
            }
        });
    }
}
