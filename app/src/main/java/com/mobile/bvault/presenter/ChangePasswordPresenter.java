package com.mobile.bvault.presenter;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.mobile.bvault.R;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.ui.view.ChangePasswordView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 06/10/17.
 */

public class ChangePasswordPresenter implements View.OnClickListener{

    private static final String TAG = "ChangePasswordPresenter";

    private Context context;
    private ChangePasswordView changePasswordView;
    private Dialog dialog;
    EditText etCurrentPassword;
    EditText etNewPassword;
    EditText etConfirmPassword;
    Button btnCancel;
    Button btnChangePassword;



    public ChangePasswordPresenter(Context context, ChangePasswordView changePasswordView) {
        this.context = context;
        this.changePasswordView = changePasswordView;
        initView();
    }

    private void initView() {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_change_password);

        etCurrentPassword = (EditText) dialog.findViewById(R.id.et_current_password);
        etNewPassword = (EditText) dialog.findViewById(R.id.et_new_password);
        etConfirmPassword = (EditText) dialog.findViewById(R.id.et_confirm_password);

        btnChangePassword = (Button) dialog.findViewById(R.id.btn_change_password);
        btnChangePassword.setOnClickListener(this);
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
    }

    public void showDialog() {
        if (dialog != null) {
            dialog.show();
            etCurrentPassword.setText("");
            etNewPassword.setText("");
            etConfirmPassword.setText("");
        }
    }

    void hideDialog() {
        CommonUtils.printErrorLog(TAG,"Hide the change password dialog");
        if (dialog != null)
            dialog.dismiss();
    }

    void changePassword() {
        CommonUtils.printErrorLog(TAG,"clicked change password button");
        validate();
    }

    private void validate() {
        String currentPassword = etCurrentPassword.getText().toString();
        String newPassword = etNewPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if (TextUtils.isEmpty(currentPassword)) {
            changePasswordView.onPasswordChangeFailure(context.getString(R.string.error_password_field_required));

        } else if (TextUtils.isEmpty(newPassword)) {
            changePasswordView.onPasswordChangeFailure(context.getString(R.string.error_new_password_field_required));

        } else if (!confirmPassword.equals(newPassword)) {
            changePasswordView.onPasswordChangeFailure(context.getString(R.string.error_password_same_field_required));

        }else if (!TextUtils.isEmpty(newPassword) && !CommonUtils.isPasswordValid(newPassword)) {
            changePasswordView.onPasswordChangeFailure(context.getString(R.string.error_invalid_password));

        }else if (!TextUtils.isEmpty(currentPassword) && !TextUtils.isEmpty(newPassword) && currentPassword.equals(newPassword)) {
            changePasswordView.onPasswordChangeFailure(context.getString(R.string.error_same_password));

        } else {
            updatePassword(currentPassword,newPassword);
        }
    }

    private void updatePassword(String currentPwd, String newPwd) {
        changePasswordView.showLoading();
        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> changePasswordCall = userApiInterface.changePassword(userId,currentPwd,newPwd);
        changePasswordCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                changePasswordView.hideLoading();
                if (response.isSuccessful()) {
                    hideDialog();
                    changePasswordView.onPasswordChangedSuccessful();
                }else {
                    try {
                        String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                        changePasswordView.onPasswordChangeFailure(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                        changePasswordView.onPasswordChangeFailure(response.message());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        changePasswordView.onPasswordChangeFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hideDialog();
                changePasswordView.hideLoading();
                changePasswordView.onPasswordChangeFailure(t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                hideDialog();
                break;
            case R.id.btn_change_password:
                changePassword();
                break;
        }
    }
}
