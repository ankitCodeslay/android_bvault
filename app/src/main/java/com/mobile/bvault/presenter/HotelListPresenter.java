package com.mobile.bvault.presenter;

import android.content.Context;
import android.location.Location;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.HotelApiInterface;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.ui.view.HotelListView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 21/08/17.
 */

public class HotelListPresenter {

    private static final String TAG = "HotelListPresenter";

    private HotelListView hotelListView;
    private Context context;

    public HotelListPresenter(HotelListView hotelListView, Context context) {
        this.hotelListView = hotelListView;
        this.context = context;
    }

    public void getHotelList(Location location) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            hotelListView.onHotelListFailureListener(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<List<Hotel>> apiCall = null;
        if (location != null) {
            CommonUtils.printErrorLog(TAG,"location not null");
            apiCall = hotelApiInterface.getHotels(location.getLatitude(),location.getLongitude());
        }else {
            CommonUtils.printErrorLog(TAG,"location is null");
            apiCall = hotelApiInterface.getHotels();
        }

        apiCall.enqueue(new Callback<List<Hotel>>() {
            @Override
            public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                if (response.isSuccessful()) {
                    CommonUtils.printErrorLog(TAG,"success");
                    hotelListView.onHotelListSuccessListener(response.body());
                }else {
                    hotelListView.onHotelListFailureListener(response.message());
                    CommonUtils.printErrorLog(TAG,"response failure");
                }
            }

            @Override
            public void onFailure(Call<List<Hotel>> call, Throwable t) {
                hotelListView.onHotelListFailureListener(t.getMessage());
                CommonUtils.printErrorLog(TAG,t.getMessage());
            }
        });
    }

    public void getHotelsByBottleId(Location location,String bottleId) {
        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<List<Hotel>> apiCall = null;
        if (location != null) {
            CommonUtils.printErrorLog(TAG,"location not null");
            apiCall = hotelApiInterface.getHotels(bottleId,location.getLatitude(),location.getLongitude());
        }else {
            CommonUtils.printErrorLog(TAG,"location is null");
            apiCall = hotelApiInterface.getHotels(bottleId);
        }

        apiCall.enqueue(new Callback<List<Hotel>>() {
            @Override
            public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                if (response.isSuccessful()) {
                    CommonUtils.printErrorLog(TAG,"success");
                    hotelListView.onHotelListSuccessListener(response.body());
                }else {
                    hotelListView.onHotelListFailureListener(response.message());
                    CommonUtils.printErrorLog(TAG,"response failure");
                }
            }

            @Override
            public void onFailure(Call<List<Hotel>> call, Throwable t) {
                hotelListView.onHotelListFailureListener(t.getMessage());
                CommonUtils.printErrorLog(TAG,t.getMessage());
            }
        });
    }
}
