package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.sqlite.DBHelper;
import com.mobile.bvault.ui.view.WishlistView;

/**
 * Created by diptif on 28/08/17.
 */

public class WishlistPresenter {

    private static final String TAG = "WishlistPresenter";

    private Context context;
    private WishlistView wishlistView;

    public WishlistPresenter(Context context, WishlistView wishlistView) {
        this.context = context;
        this.wishlistView = wishlistView;
    }

    public void getWishlist() {
        this.wishlistView.loadWishlist(DBHelper.getInstance(context).getWishlists());
    }

    public void clearWishlist() {
        DBHelper.getInstance(context).clearWishlist();
    }

    public void getTotalAmount() {
        int result = DBHelper.getInstance(context).getTotalAmount();
        wishlistView.setTotalAmount(result);
    }
}
