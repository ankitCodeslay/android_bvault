package com.mobile.bvault.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.data.sqlite.DBHelper;
import com.mobile.bvault.model.PayUPaymentResponse;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.PurchaseRequest;
import com.mobile.bvault.model.PurchasedBottle;
import com.mobile.bvault.model.Wishlist;
import com.mobile.bvault.ui.view.PurchaseView;
import com.mobile.bvault.utils.CommonUtils;
import com.payu.india.Payu.PayuConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 31/08/17.
 */

public class PurchasePresenter {

    private static final String TAG = "PurchasePresenter";

    private PurchaseView purchaseView;
    private Context context;

    public PurchasePresenter(Context context, PurchaseView purchaseView) {
        this.context = context;
        this.purchaseView = purchaseView;
    }

    public void savePurchasedBottle(PromoCode code) {

        PurchaseRequest purchaseRequest = getPurchaseRequestData(code);
        if (purchaseRequest != null) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<Void> call = bottleApiInterface.purchaseBottle(purchaseRequest);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        purchaseView.onPurchaseSuccess();
                    } else {
                        purchaseView.onPurchaseFailure(response.message());
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    purchaseView.onPurchaseFailure(t.getMessage());
                }
            });
        }
    }

    public void savePayUPurchaseResponse(PromoCode code, JSONObject payURes, Double amount) {
        PayUPaymentResponse payUPaymentResponse = getPayUPaymentRes(code, payURes, amount);
        if (payUPaymentResponse != null) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<Void> call = bottleApiInterface.payUResponse(payUPaymentResponse);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }
    }

    private PurchaseRequest getPurchaseRequestData(PromoCode code) {
        PurchaseRequest purchaseRequest = null;
        List<PurchasedBottle> purchasedBottles = new ArrayList<PurchasedBottle>();

        List<Wishlist> wishlists = DBHelper.getInstance(context).getWishlists();

        if (wishlists != null && wishlists.size() > 0) {
            String userId = AppPreferencesHelper.getInstance(context).getUserId();
            CommonUtils.printErrorLog(TAG, "userId: " + userId);

            for (Wishlist wishlist : wishlists) {
                purchasedBottles.add(new PurchasedBottle(wishlist.getBottleId(), wishlist.getQty(), wishlist.getVolume(), wishlist.getType(), wishlist.getPrice()));
            }

            if (code != null) {
                purchaseRequest = new PurchaseRequest(userId, purchasedBottles, code.getPromoCodeId(), true,0);
            } else {
                purchaseRequest = new PurchaseRequest(userId, purchasedBottles, "", true,0);
            }

        }
        return purchaseRequest;
    }

    private PayUPaymentResponse getPayUPaymentRes(PromoCode code, JSONObject payuRes, Double amount) {
        PayUPaymentResponse payUPaymentResponse = null;
        String payUId = "";
        String txnId = "";
        String userName = "";
        String email = "";
        String phone = "";
        String status = "";
        PurchaseRequest purchaseRequest = getPurchaseRequestData(code);
        try {
            if (payuRes.has(PayuConstants.ID))
                payUId = payuRes.getString(PayuConstants.ID);

            if (payuRes.has(PayuConstants.STATUS))
                status = payuRes.getString(PayuConstants.STATUS);

            if (payuRes.has(PayuConstants.TXNID))
                txnId = payuRes.getString(PayuConstants.TXNID);

            if (payuRes.has(PayuConstants.EMAIL))
                email = payuRes.getString(PayuConstants.EMAIL);

            if (payuRes.has(PayuConstants.PHONE))
                phone = payuRes.getString(PayuConstants.PHONE);

            if (payuRes.has(PayuConstants.FIRST_NAME))
                userName = payuRes.getString(PayuConstants.FIRST_NAME);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        payUPaymentResponse = new PayUPaymentResponse(payUId, email, userName, phone, txnId, status, amount, purchaseRequest);
        return payUPaymentResponse;
    }

    public String getPurchasedBottlesJsonString() {
        List<PurchasedBottle> purchasedBottles = new ArrayList<>();
        List<Wishlist> wishlists = DBHelper.getInstance(context).getWishlists();

        if (wishlists != null && wishlists.size() > 0) {
            for (Wishlist wishlist : wishlists) {
                purchasedBottles.add(new PurchasedBottle(wishlist.getBottleId(), wishlist.getQty(), wishlist.getVolume(), wishlist.getType(), wishlist.getPrice()));
            }
        }

        Gson gson = new GsonBuilder().create();
        JsonArray jsonArray = gson.toJsonTree(purchasedBottles).getAsJsonArray();
        return jsonArray.toString();
    }
}
