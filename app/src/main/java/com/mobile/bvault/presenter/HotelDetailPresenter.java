package com.mobile.bvault.presenter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Advertisement;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.ui.activity.HotelDetailActivity;
import com.mobile.bvault.ui.view.HotelDetailView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.PermissionUtil;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import static com.facebook.GraphRequest.TAG;

/**
 * Created by diptif on 24/08/17.
 */

public class HotelDetailPresenter {

    private HotelDetailView hotelView;
    private Context context;
    public Hotel hotel;

    public HotelDetailPresenter(HotelDetailView hotelView, Context context) {
        this.hotelView = hotelView;
        this.context = context;
    }

    public void setHotelDetail() {
        Bundle bundle = ((HotelDetailActivity) context).getIntent().getExtras();
        if (bundle != null) {
            this.hotel = bundle.getParcelable(Constants.HOTEL);
            if (this.hotel != null) {
                hotelView.setHotelName(this.hotel.getName());
                hotelView.setHotelAddress(this.hotel.getAddress());
                hotelView.setHotelImage(hotel.getImageURL());
                hotelView.setDistance(hotel.getDistance());
                hotelView.setPhoneNumber(hotel.getPhone());
                hotelView.setMusic(hotel.getMusic());
                hotelView.setCuisine(hotel.getCuisine());
                hotelView.setCost(hotel.getCost());
                hotelView.setMoreInfo(hotel.getMoreInfo());
            }
        }
    }

    public void showMap() {
        if (hotel != null && context != null) {
            double latitude = hotel.getLat();
            double longitude = hotel.getLng();
            String label = hotel.getName();
            String uriBegin = "geo:" + latitude + "," + longitude;
            String query = latitude + "," + longitude + "(" + label + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
            Uri uri = Uri.parse(uriString);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
            context.startActivity(intent);
        }
    }

    public void makePhoneCall() {
        if (hotel != null) {
            String uri = "tel:" + hotel.getPhone();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(uri));
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.requestPermissions(((AppCompatActivity) context), Constants.REQUEST_CALL_CODE, PermissionUtil.CALL_PERMISSION);
            } else {
                CommonUtils.printErrorLog(TAG, "inside start activity");
                context.startActivity(intent);
            }
        }
    }

    public void setHotelImages(CarouselView carouselView, final String[] sampleImages) {

//        final int[] sampleImages = {R.drawable.bottles, R.drawable.bottles, R.drawable.bottles, R.drawable.bottles, R.drawable.bottles};
        if (carouselView != null && sampleImages != null) {
            carouselView.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
//                    imageView.setImageResource(sampleImages[position]);
//                    CommonUtils.printErrorLog(TAG,"Image name "+hotelImages.get(position));
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    String imageUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_HOTELS, sampleImages[position]);
                    ImageUtil.setImage(context, imageView, imageUrl, R.drawable.banner);
                }
            });
            carouselView.setPageCount(sampleImages.length);

        }
    }
}
