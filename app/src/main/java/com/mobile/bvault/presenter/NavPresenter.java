package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.R;
import com.mobile.bvault.model.NavOption;
import com.mobile.bvault.ui.activity.ContactUsActivity;
import com.mobile.bvault.ui.activity.DrinkTypeActivity;
import com.mobile.bvault.ui.activity.HistoryActivity;
import com.mobile.bvault.ui.activity.HomeActivity;
import com.mobile.bvault.ui.activity.HotelListActivity;
import com.mobile.bvault.ui.activity.IplMatchActivity;
import com.mobile.bvault.ui.activity.NotificationActivity;
import com.mobile.bvault.ui.activity.RackListActivity;
import com.mobile.bvault.ui.activity.ReferAndEarnActivity;
import com.mobile.bvault.ui.activity.WishlistActivity;
import com.mobile.bvault.ui.view.NavView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diptif on 18/09/17.
 */

public class NavPresenter {

    private NavView navView;
    private Context context;

    public NavPresenter(Context context,NavView navView) {
        this.context = context;
        this.navView = navView;
    }

    public void setOptions() {
        List<NavOption> navOptions = new ArrayList<NavOption>();
        navOptions.add(new NavOption(DrinkTypeActivity.class, context.getString(R.string.browse_drink)));
        navOptions.add(new NavOption(HotelListActivity.class,context.getString(R.string.restaurant)));
        navOptions.add(new NavOption(RackListActivity.class,context.getString(R.string.my_rack)));
        navOptions.add(new NavOption(WishlistActivity.class,context.getString(R.string.my_order)));
        navOptions.add(new NavOption(HistoryActivity.class,context.getString(R.string.history)));
//        navOptions.add(new NavOption(NotificationActivity.class,context.getString(R.string.notification)));
        navOptions.add(0,new NavOption(HomeActivity.class,context.getString(R.string.home)));
        navOptions.add(new NavOption(ReferAndEarnActivity.class,context.getString(R.string.refer)));
        //navOptions.add(new NavOption(IplMatchActivity.class,context.getString(R.string.ipl)));
        navOptions.add(new NavOption(ContactUsActivity.class, context.getString(R.string.contact_us)));
        navOptions.add(new NavOption(null, context.getString(R.string.terms_condition)));

        navView.setOptions(navOptions);
    }
}
