package com.mobile.bvault.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSCallback;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.ReferralCodeModel;
import com.mobile.bvault.model.User;
import com.mobile.bvault.ui.view.ProfileView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.NetworkUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by diptif on 05/10/17.
 */

public class ProfilePresenter implements AWSCallback {

    private static final String TAG = "ProfilePresenter";

    private Context context;
    private ProfileView profileView;

    public ProfilePresenter(Context context, ProfileView profileView) {
        this.context = context;
        this.profileView = profileView;
    }

    public void validate(String fullName, String email, String phoneNumber, String dob) {
        if (fullName.isEmpty()) {
            profileView.onValidationFailure(context.getString(R.string.error_username_field_required));
        } else if (TextUtils.isEmpty(email)) {
            profileView.onValidationFailure(context.getString(R.string.error_email_field_required));
        } else if (TextUtils.isEmpty(dob)) {
            profileView.onValidationFailure(context.getString(R.string.error_dob_field_required));
        } else if (getDiffYears(dob) < 18) {
            profileView.onValidationFailure(context.getString(R.string.error_dob_less));
        } else if (TextUtils.isEmpty(phoneNumber)) {
            profileView.onValidationFailure(context.getString(R.string.error_phone_field_required));
        } else if (!CommonUtils.isEmailValid(email)) {
            profileView.onValidationFailure(context.getString(R.string.error_invalid_email));
        } else if (!phoneNumber.isEmpty() && phoneNumber.length() != 10) {
            profileView.onValidationFailure(context.getString(R.string.error_phone_length));
        } else {
            profileView.onValidationSuccess();
        }
    }

    private static int getDiffYears(String inputDateString) {
        Date inputDate = null;
        Date todayDate = null;
        int diff = 0;
        try {
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            inputDate = format.parse(inputDateString);

            todayDate = new Date();
            String todayDateString = format.format(todayDate);
            todayDate = format.parse(todayDateString);

            Calendar a = getCalendar(inputDate);
            Calendar b = getCalendar(todayDate);
            diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
            if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
                diff--;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diff;
    }

    private static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public void updateProfile(String name, String email, String phoneNumber, String dob, String favourite, String referralCode,boolean isOtherLogin) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            profileView.onProfileUpdateFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String[] nameArray = CommonUtils.getFirstNameAndLastName(name);
        String username = email.split("@")[0];
        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        String currentCity = AppPreferencesHelper.getInstance(context).getCurrentCity();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> updateCall = userApiInterface.updateUser(userId, nameArray[0], nameArray[1], email, username, phoneNumber, dob, favourite, currentCity, referralCode,isOtherLogin);
        updateCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    AppPreferencesHelper.getInstance(context).setUser(user);
                    profileView.onProfileUpdatedSuccessfully();
                } else {
                    profileView.onProfileUpdateFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                profileView.onProfileUpdateFailure(t.getMessage());
            }
        });
    }

    public void setUserInfo() {
        profileView.setProfile(AppPreferencesHelper.getInstance(context).getUser());
    }

    public void uploadImageOnAWS(String imagePath) {
        try {
            String fileName = ImageUtil.generateFileName();
            String bucketPath = AWSUtil.getProfileBucketPath(fileName);
            AWSUtil awsUtil = new AWSUtil(context, this);
            awsUtil.uploadByKey(bucketPath, new File(imagePath), fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProfileImage(String fileName, final String filePath) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            profileView.onProfileUpdateFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        final String userId = AppPreferencesHelper.getInstance(context).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> userCall = userApiInterface.updateUserProfilePic(userId, fileName);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    AppPreferencesHelper.getInstance(context).setImageUrl(user.getImageUrl());
                    profileView.onProfileImageUploadedSuccessfully(filePath);
                } else {
                    profileView.onProfileUpdateFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                profileView.onProfileUpdateFailure(t.getMessage());
            }
        });
    }

    @Override
    public void onAWSSuccessCallback(String fileName, String filePath) {
        updateProfileImage(fileName, filePath);
    }

    @Override
    public void onAWSFailureCallback(String errorMessage) {
        profileView.onProfileUpdateFailure(errorMessage);
    }

    public void sendOtp(String mobileNo, String otp) {
        if (!NetworkUtils.isNetworkConnected(context)) {
            profileView.onProfileUpdateFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        String message = otp + "%20is%20the%20OTP%20for%20your%20BVault%20account,%20kindly%20verify.";
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://sms.digimiles.in/")
                .build();
        String url = "http://sms.digimiles.in/bulksms/bulksms?username=di78-dreamtech&password=digimile&type=0&dlr=1&destination=" + mobileNo + "&source=BVault&message=" + message;
        UserApiInterface userApiInterface = retrofit.create(UserApiInterface.class);
        Call<String> stringCall = userApiInterface.sendOtpRequest(url);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void validateReferralCode(String referralCode) {
        if (!NetworkUtils.isNetworkConnected(context)) {
            profileView.onProfileUpdateFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        ReferralCodeModel referralCodeModel = new ReferralCodeModel();
        referralCodeModel.setReferralCode(referralCode);
        Call<ReferralCodeModel> userCall = userApiInterface.validateReferralCode(referralCodeModel);
        userCall.enqueue(new Callback<ReferralCodeModel>() {
            @Override
            public void onResponse(Call<ReferralCodeModel> call, Response<ReferralCodeModel> response) {
                if (response.isSuccessful()) {
                    profileView.onRefCodeValidationSuccess();
                } else {
                    profileView.onRefCodeValidationFail(context.getString(R.string.messageRefCodeFail));
                }
            }

            @Override
            public void onFailure(Call<ReferralCodeModel> call, Throwable t) {
                profileView.onRefCodeValidationFail(t.getMessage());
            }
        });
    }
}
