package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.model.DrinkType;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.ui.view.DrinkTypeView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 18/08/17.
 */

public class DrinkTypePresenter {

    private DrinkTypeView drinkTypeView;
    private Context context;

    public DrinkTypePresenter(DrinkTypeView drinkTypeView, Context context) {
        this.drinkTypeView = drinkTypeView;
        this.context = context;
    }

    public void getDrinkTypes() {

        if (!NetworkUtils.isNetworkConnected(context)) {
            drinkTypeView.onFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<List<DrinkType>> apiCall = bottleApiInterface.getDrinkTypes();
        apiCall.enqueue(new Callback<List<DrinkType>>() {
            @Override
            public void onResponse(Call<List<DrinkType>> call, Response<List<DrinkType>> response) {
                if (response.isSuccessful()){
                    drinkTypeView.onSuccess(response.body());
                }else {
                    drinkTypeView.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<DrinkType>> call, Throwable t) {
                String message;
                if (t != null){
                    message = t.getMessage();
                }else {
                    message = "Error occurred while loading the drink type";
                }
                drinkTypeView.onFailure(message);
            }
        });
    }
}
