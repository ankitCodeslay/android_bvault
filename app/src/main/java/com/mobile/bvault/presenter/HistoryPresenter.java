package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.History;
import com.mobile.bvault.ui.view.HistoryView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 21/08/17.
 */

public class HistoryPresenter {

    private static final String TAG = "HistoryPresenter";

    private HistoryView historyView;
    private Context context;

    public HistoryPresenter(Context context, HistoryView historyView) {
        this.context = context;
        this.historyView = historyView;
    }

    public void getHistory(int page) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            historyView.onFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        CommonUtils.printErrorLog("userId",userId);
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<List<History>> apiCall = userApiInterface.getHistory(userId,page);
        apiCall.enqueue(new Callback<List<History>>() {
            @Override
            public void onResponse(Call<List<History>> call, Response<List<History>> response) {
                if (response.isSuccessful()) {
                    historyView.onHistoryLoadedSuccessfully(response.body());
                }else {
                    historyView.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<History>> call, Throwable t) {
                historyView.onFailure(t.getMessage());
            }
        });
    }
}
