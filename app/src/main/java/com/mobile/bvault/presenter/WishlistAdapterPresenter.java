package com.mobile.bvault.presenter;

import android.content.Context;
import android.widget.TextView;

import com.mobile.bvault.data.sqlite.DBHelper;
import com.mobile.bvault.ui.view.WishlistAdapterView;

/**
 * Created by diptif on 30/08/17.
 */

public class WishlistAdapterPresenter {

    private Context context;
    private WishlistAdapterView adapterView;

    public WishlistAdapterPresenter(Context context, WishlistAdapterView adapterView) {
        this.context = context;
        this.adapterView = adapterView;
    }

    public void updateQtyPriceAndVolume(int qty,int price, int orderId, TextView view,TextView priceTextView,int position, int volume) {
        int result = DBHelper.getInstance(context).updateQuantityPriceAndVolume(orderId,qty,price,volume);
        adapterView.updateQuantityCallback(result,view,priceTextView,qty,price,position);
    }

    public void removeItemFromWishlist(int wishlistId, int position) {
        int result = DBHelper.getInstance(context).deleteItem(wishlistId);
        adapterView.removeItemCallback(result,position);
    }
}
