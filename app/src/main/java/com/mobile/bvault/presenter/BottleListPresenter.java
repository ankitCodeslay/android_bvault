package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.ui.view.BottleListView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 21/08/17.
 */

public class BottleListPresenter {

    private BottleListView bottleListView;
    private Context context;

    public BottleListPresenter(BottleListView bottleListView, Context context) {
        this.bottleListView = bottleListView;
        this.context = context;
    }

    public void getBottleList(String typeId) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            bottleListView.onFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<List<Bottle>> apiCall = bottleApiInterface.getBottleList(typeId);
        apiCall.enqueue(new Callback<List<Bottle>>() {
            @Override
            public void onResponse(Call<List<Bottle>> call, Response<List<Bottle>> response) {
                if (response.isSuccessful()) {
                    bottleListView.onSuccess(response.body());
                }else {
                    bottleListView.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Bottle>> call, Throwable t) {
                bottleListView.onFailure(t.getMessage());
            }
        });
    }
}
