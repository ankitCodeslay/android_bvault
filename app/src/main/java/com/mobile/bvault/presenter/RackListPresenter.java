package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.Rack;
import com.mobile.bvault.ui.view.RackListView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 21/08/17.
 */

public class RackListPresenter {

    private RackListView rackListView;
    private Context context;

    public RackListPresenter(Context context,RackListView rackListView) {
        this.rackListView = rackListView;
        this.context = context;
    }

    public void getBottleList() {

        if (!NetworkUtils.isNetworkConnected(context)) {
            rackListView.onFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        Call<List<Rack>> apiCall = bottleApiInterface.getRackList(userId);
        apiCall.enqueue(new Callback<List<Rack>>() {
            @Override
            public void onResponse(Call<List<Rack>> call, Response<List<Rack>> response) {
                if (response.isSuccessful()) {
                    rackListView.onSuccess(response.body());
                }else {
                    rackListView.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Rack>> call, Throwable t) {
                rackListView.onFailure(t.getMessage());
            }
        });
    }
}
