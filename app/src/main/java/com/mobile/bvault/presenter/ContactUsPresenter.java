package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.ApiInterface;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.ui.view.ContactUsView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 01/11/17.
 */

public class ContactUsPresenter {

    private Context context;
    private ContactUsView contactUsView;

    public ContactUsPresenter(Context context, ContactUsView contactUsView) {
        this.context = context;
        this.contactUsView = contactUsView;
    }

    public void contactUs(String message) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            contactUsView.onContactUsFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        if (userId != null) {
            UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
            Call<User> userCall = userApiInterface.contactUs(userId,message);
            userCall.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful())
                        contactUsView.onContactUsSuccess("Your message sent successfully");
                    else
                        contactUsView.onContactUsFailure(response.message());
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    contactUsView.onContactUsFailure(t.getMessage());
                }
            });
        }
    }
}
