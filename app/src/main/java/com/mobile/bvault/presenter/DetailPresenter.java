package com.mobile.bvault.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.Price;
import com.mobile.bvault.model.Wishlist;
import com.mobile.bvault.data.sqlite.DBHelper;
import com.mobile.bvault.ui.activity.BottleDetailActivity;
import com.mobile.bvault.ui.view.DetailView;
import com.mobile.bvault.utils.Constants;

/**
 * Created by diptif on 24/08/17.
 */

public class DetailPresenter {

    private DetailView detailView;
    private Context context;
    private Bottle bottle;

    public DetailPresenter(DetailView detailView, Context context) {
        this.detailView = detailView;
        this.context = context;
    }

    public void setBottleDetail() {
        Bundle bundle = ((BottleDetailActivity)context).getIntent().getExtras();
        if (bundle != null) {
             this.bottle = bundle.getParcelable(Constants.BOTTLE);
            if (this.bottle != null) {
                detailView.setBottleTitle(this.bottle.getName());
                detailView.setBottleDescription(this.bottle.getDescription());
                detailView.setBottleSaveText(this.bottle.getSaveUpto());
                detailView.setBottleImage(this.bottle.getImageURL());
                detailView.setBottleId(this.bottle.getBottleId());
                detailView.setPrice(bottle.getPrices(), bottle.getUnit());
            }
        }
    }

    public void addToCart(final Price selectedPrice) {
        if (this.bottle != null) {
            final Wishlist wishlist = new Wishlist(0,bottle.getBottleId(),
                    bottle.getName(),
                    selectedPrice.getPrice(),
                    bottle.getDescription(),
                    bottle.getImageURL(),
                    1,
                    selectedPrice.getVolume(),selectedPrice.getLabel(),bottle.getUnit());

            new Thread(new Runnable() {
                @Override
                public void run() {
                    DBHelper.getInstance(context).insertWishlist(wishlist);
                    if (context != null) {
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                isAddedToCart(selectedPrice);
                                detailView.updateWishlistCount();
                            }
                        });
                    }
                }
            }).start();
        }
    }

    public void isAddedToCart(Price selectedPrice){
        if (bottle != null && selectedPrice != null) {
            detailView.isAddedToCart(DBHelper.getInstance(context).isAlreadyAdded(bottle.getBottleId(),selectedPrice.getVolume()));
        }
    }
}
