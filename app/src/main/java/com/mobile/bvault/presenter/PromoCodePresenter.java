package com.mobile.bvault.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.RefPromoCodeModel;
import com.mobile.bvault.ui.view.PromoCodeView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 10/10/17.
 */

public class PromoCodePresenter {

    private Context context;
    private PromoCodeView promoCodeView;

    public PromoCodePresenter(Context context, PromoCodeView promoCodeView) {
        this.context = context;
        this.promoCodeView = promoCodeView;
    }

    public void applyCode(String code) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            promoCodeView.onPromoCodeFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        if (userId != null && !userId.isEmpty()) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<PromoCode> promoCodeCall = bottleApiInterface.applyPromoCode(code, userId);
            promoCodeCall.enqueue(new Callback<PromoCode>() {
                @Override
                public void onResponse(Call<PromoCode> call, Response<PromoCode> response) {
                    if (response.isSuccessful()) {
                        promoCodeView.onPromoCodeAppliedSuccessfully(response.body());
                    } else {
                        try {
                            String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                            promoCodeView.onPromoCodeFailure(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                            promoCodeView.onPromoCodeFailure(response.message());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            promoCodeView.onPromoCodeFailure(response.message());
                        }

                    }
                }

                @Override
                public void onFailure(Call<PromoCode> call, Throwable t) {
                    promoCodeView.onPromoCodeFailure(t.getMessage());
                }
            });

        } else {
            promoCodeView.onPromoCodeFailure("User is Logged Out");
        }
    }

    public void applyRefPromoCode(String code) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            promoCodeView.onPromoCodeFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        if (userId != null && !userId.isEmpty()) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            RefPromoCodeModel promoCodeModel = new RefPromoCodeModel();
            promoCodeModel.setUserId(userId);
            promoCodeModel.setPromoCode(code);
            Call<RefPromoCodeModel> promoCodeCall = bottleApiInterface.applyRefPromoCode(promoCodeModel);
            promoCodeCall.enqueue(new Callback<RefPromoCodeModel>() {
                @Override
                public void onResponse(Call<RefPromoCodeModel> call, Response<RefPromoCodeModel> response) {
                    if (response.isSuccessful()) {
                        RefPromoCodeModel promoCodeModel = response.body();

                        if (promoCodeModel.isValid()) {
                            PromoCode promoCode = new PromoCode();
                            promoCode.setPromoCodeId("");
                            promoCode.setDiscount(promoCodeModel.getDiscount());
                            promoCodeView.onPromoCodeAppliedSuccessfully(promoCode);
                        } else {
                            promoCodeView.onPromoCodeFailure("Invalid Promo Code");
                        }
                    } else {
                        promoCodeView.onPromoCodeFailure(response.message());
                    }
                }

                @Override
                public void onFailure(Call<RefPromoCodeModel> call, Throwable t) {
                    promoCodeView.onPromoCodeFailure(t.getMessage());
                }
            });

        } else {
            promoCodeView.onPromoCodeFailure("User is Logged Out");
        }
    }

    public void useRefPromoCode(String code) {
        if (TextUtils.isEmpty(code)) {
            return;
        }
        if (!NetworkUtils.isNetworkConnected(context)) {
            promoCodeView.onPromoCodeFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        if (userId != null && !userId.isEmpty()) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            RefPromoCodeModel promoCodeModel = new RefPromoCodeModel();
            promoCodeModel.setUserId(userId);
            promoCodeModel.setPromoCode(code);
            Call<RefPromoCodeModel> promoCodeCall = bottleApiInterface.usedReferralPromoCode(promoCodeModel);
            promoCodeCall.enqueue(new Callback<RefPromoCodeModel>() {
                @Override
                public void onResponse(Call<RefPromoCodeModel> call, Response<RefPromoCodeModel> response) {
                    if (response.isSuccessful()) {

                    }
                }

                @Override
                public void onFailure(Call<RefPromoCodeModel> call, Throwable t) {

                }
            });

        }
    }
}
