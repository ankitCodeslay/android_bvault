package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.model.DrinkType;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 05/09/17.
 */

public class SplashPresenter {

    private static final String TAG = "SplashPresenter";

    private Context context;

    public SplashPresenter(Context context) {
        this.context = context;
    }

    public void wakeupServer() {
        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<List<DrinkType>> listCall = bottleApiInterface.getDrinkTypes();
        listCall.enqueue(new Callback<List<DrinkType>>() {
            @Override
            public void onResponse(Call<List<DrinkType>> call, Response<List<DrinkType>> response) {
                if (response.isSuccessful()) {
                    CommonUtils.printErrorLog(TAG,"drink type api called successfully");
                }else {
                    CommonUtils.printErrorLog(TAG,"Fail drink type api call");
                }
            }

            @Override
            public void onFailure(Call<List<DrinkType>> call, Throwable t) {
//                CommonUtils.printErrorLog(TAG,t.getMessage());
            }
        });
    }
}
