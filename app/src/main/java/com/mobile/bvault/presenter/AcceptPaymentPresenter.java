package com.mobile.bvault.presenter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mobile.bvault.MyApp;
import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.HotelApiInterface;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.model.PaymentResponse;
import com.mobile.bvault.ui.view.AcceptPaymentView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 06/10/17.
 */

public class AcceptPaymentPresenter implements View.OnClickListener{

    private static final String TAG = "AcceptPaymentPresenter";

    private Context context;
    private AcceptPaymentView acceptPaymentView;
    private Dialog dialog;
    Button btnCancel;
    Button btnConfirm;

    TextView hotelNameTextView;
    TextView bottleNameTextView;
    TextView bottleCodeTextView;
    TextView quantityTextView;
    TextView costTextView;
    TextView taxTextView;


    private Payment payment;
    private Bundle bundle;

    String paymentId;

    public void setPayment(Payment payment) {
        this.payment = payment;
        CommonUtils.printErrorLog(TAG,""+payment.getCost());
        setPaymentData(payment);
    }

    public AcceptPaymentPresenter(Context context, AcceptPaymentView acceptPaymentView) {
        this.context = context;
        this.acceptPaymentView = acceptPaymentView;
        initView();
    }

    public AcceptPaymentPresenter(Context context, AcceptPaymentView acceptPaymentView,Payment payment) {
        this.context = context;
        this.acceptPaymentView = acceptPaymentView;
        this.payment = payment;
        initView();
        setPaymentData(payment);
    }

    private void initView() {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_payment_confirmation);

        hotelNameTextView = (TextView) dialog.findViewById(R.id.text_view_hotel_name);
        bottleNameTextView = (TextView) dialog.findViewById(R.id.text_view_bottle_name);
        bottleCodeTextView = (TextView) dialog.findViewById(R.id.text_view_bottle_code);
        quantityTextView = (TextView) dialog.findViewById(R.id.text_view_qty);
        costTextView = (TextView) dialog.findViewById(R.id.text_view_cost);
        taxTextView = (TextView) dialog.findViewById(R.id.text_view_tax);


        btnConfirm = (Button) dialog.findViewById(R.id.btn_change_password);
        btnConfirm.setOnClickListener(this);
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
    }

    public void showDialog() {
        CommonUtils.printErrorLog(TAG,"Show dialog");
        if (dialog != null && !((BaseActivity) context).isFinishing()) {
            dialog.show();
        }
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    void cancelPayment() {
        CommonUtils.printErrorLog(TAG,"Hide the change password dialog");
        acceptCancelPayment("Cancelled",context.getString(R.string.payment_cancelled_msg));
    }

    void confirmPayment() {
        CommonUtils.printErrorLog(TAG,"clicked change password button");
        acceptCancelPayment("Accepted",context.getString(R.string.payment_accepted_msg));
    }

    void acceptCancelPayment(final String status, final String message) {
        if (paymentId == null) {
            acceptPaymentView.onPaymentStatusChangeFailure("Something went wrong please try again");
            return;
        }

        if (!NetworkUtils.isNetworkConnected(context)) {
            acceptPaymentView.onPaymentStatusChangeFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<Payment> paymentCall = hotelApiInterface.updatePaymentStatus(paymentId,status);
        paymentCall.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                CommonUtils.printErrorLog(TAG,response.raw().body().toString());
                if (response.isSuccessful()) {
                    acceptPaymentView.onPaymentStatusChangeSuccessful(message);
                }else {
                    try {
                        String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                        acceptPaymentView.onPaymentStatusChangeFailure(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                        acceptPaymentView.onPaymentStatusChangeFailure(response.message());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        acceptPaymentView.onPaymentStatusChangeFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                acceptPaymentView.onPaymentStatusChangeFailure(t.getMessage());
            }
        });
    }

    public void getPaymentRequestByID(String paymentRequestId) {
        if (paymentRequestId == null) {
            acceptPaymentView.onPaymentStatusChangeFailure("Something went wrong please try again");
            return;
        }

        if (!NetworkUtils.isNetworkConnected(context)) {
            acceptPaymentView.onPaymentStatusChangeFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<Payment> paymentCall = hotelApiInterface.getPaymentStatus(paymentRequestId);
        paymentCall.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                if (response.isSuccessful()) {
                    CommonUtils.printErrorLog(TAG,"response: "+response.raw().toString());
                    CommonUtils.printErrorLog(TAG,"cost: "+response.body().getCost());
                    acceptPaymentView.onPaymentByIdSuccess(response.body());
                }else {
                    acceptPaymentView.onPaymentByIdFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                acceptPaymentView.onPaymentByIdFailure(t.getMessage());
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (payment != null && payment.getStatus().equalsIgnoreCase("Accepted")) {
                    hideDialog();
                } else  {
                    cancelPayment();
                }
                break;
            case R.id.btn_change_password:
                confirmPayment();
                break;
        }
    }

    private void setDetail(){
        if (bundle != null) {

        }
    }

    private void setPaymentData(Payment payment) {
        if (payment != null) {
            try {
                paymentId = payment.getPaymentId();
                hotelNameTextView.setText(""+payment.getHotel().getName());
                bottleNameTextView.setText(""+payment.getBottle().getName());
                bottleCodeTextView.setText(""+payment.getUniqueCode());
                quantityTextView.setText(""+payment.getQuantity()+" "+payment.getBottle().getUnit());
                taxTextView.setText(""+payment.getHotel().getTaxPercentage() + "%");
                if (payment.getStatus().equalsIgnoreCase("Accepted")) {
                    btnConfirm.setVisibility(View.GONE);
                    btnCancel.setText(context.getString(R.string.close));
                }else {
                    btnConfirm.setVisibility(View.VISIBLE);
                    btnCancel.setText(context.getString(R.string.cancel));
                }

                CommonUtils.printErrorLog(TAG,"cost "+payment.getCost());
                CommonUtils.printErrorLog(TAG,"tax "+payment.getHotel().getTaxPercentage());
                CommonUtils.printErrorLog(TAG,"cost "+ payment.getCost() * payment.getHotel().getTaxPercentage() / 100);
                long hotelCost = Math.round(payment.getCost() * payment.getHotel().getTaxPercentage() / 100);
                costTextView.setText("" + hotelCost + " (To be paid at hotel)");

            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
