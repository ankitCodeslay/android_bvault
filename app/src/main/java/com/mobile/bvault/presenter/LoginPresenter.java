package com.mobile.bvault.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.mobile.bvault.R;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.model.User;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.ui.view.LoginView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 17/08/17.
 */

public class LoginPresenter {

    private static final String TAG = "LoginPresenter";

    private Context context;
    private LoginView loginView;

    public LoginPresenter(Context context,LoginView loginView) {
        this.context = context;
        this.loginView = loginView;
    }

    public void validate(String email, String password) {

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            loginView.onValidationFail(context.getString(R.string.error_email_field_required));
        } else if (TextUtils.isEmpty(password)) {
            loginView.onValidationFail(context.getString(R.string.error_password_field_required));
        } else if (!CommonUtils.isEmailValid(email)) {
            loginView.onValidationFail(context.getString(R.string.error_invalid_email));
        }else {
            loginView.onValidationSuccess();
        }
    }

    public void loginAPICall(String email, String password){

        if (!NetworkUtils.isNetworkConnected(context)) {
            loginView.onLoginFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        UserApiInterface apiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> userCall = apiInterface.login(email,password);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response != null) {
                    if (response.isSuccessful()){
                        User user = response.body();
                        AppPreferencesHelper.getInstance(context).setUser(user);
                        loginView.onLoginSuccess(user);
                    }else {
                        try {
                            String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                            loginView.onLoginFailure(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                            loginView.onLoginFailure(response.message());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loginView.onLoginFailure(response.message());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (t != null)
                    loginView.onLoginFailure(t.getMessage());
            }
        });
    }

    public void updateDeviceToken(String deviceToken) {
        final String userId = AppPreferencesHelper.getInstance(context).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> userCall = userApiInterface.updateUserDeviceToken(userId,deviceToken);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    User user = response.body();
                    AppPreferencesHelper.getInstance(context).setDeviceToken(user.getDeviceToken());
                    loginView.onDeviceTokenUpdatedFailure(user.getDeviceToken());
                }else {
                    loginView.onDeviceTokenUpdatedFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                loginView.onDeviceTokenUpdatedFailure(t.getMessage());
            }
        });
    }

}
