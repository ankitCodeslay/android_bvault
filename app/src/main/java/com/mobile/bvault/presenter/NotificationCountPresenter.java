package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.HotelApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.NotificationCount;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.ui.view.NotificationCountView;
import com.mobile.bvault.ui.view.NotificationView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 21/08/17.
 */

public class NotificationCountPresenter {

    private NotificationCountView notificationCountView;
    private Context context;

    public NotificationCountPresenter(Context context, NotificationCountView notificationCountView) {
        this.context = context;
        this.notificationCountView = notificationCountView;
    }

    public void getRequestedPaymentCount() {

        if (!NetworkUtils.isNetworkConnected(context)) {
            notificationCountView.onNotificationCountFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(this.context).getUserId();
        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<NotificationCount> apiCall = hotelApiInterface.getRequestedPaymentCount(userId);
        apiCall.enqueue(new Callback<NotificationCount>() {
            @Override
            public void onResponse(Call<NotificationCount> call, Response<NotificationCount> response) {
                if (response.isSuccessful()) {
                    notificationCountView.onNotificationCountLoadedSuccessfully(response.body());
                }else {
                    notificationCountView.onNotificationCountFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<NotificationCount> call, Throwable t) {
                notificationCountView.onNotificationCountFailure(t.getMessage());
            }
        });
    }
}
