package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.R;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.HomeOption;
import com.mobile.bvault.ui.activity.DrinkTypeActivity;
import com.mobile.bvault.ui.activity.HotelListActivity;
import com.mobile.bvault.ui.activity.HowToPlayActivity;
import com.mobile.bvault.ui.activity.IplMatchActivity;
import com.mobile.bvault.ui.activity.RackListActivity;
import com.mobile.bvault.ui.activity.SplashActivity;
import com.mobile.bvault.ui.view.HomeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diptif on 11/09/17.
 */

public class HomePresenter {

    private HomeView homeView;
    private Context context;

    public HomePresenter(Context context, HomeView homeView) {
        this.context = context;
        this.homeView = homeView;
    }

    public void setHomeOptions() {

        List<HomeOption> homeOptions = new ArrayList<>();

        homeOptions.add(new HomeOption(DrinkTypeActivity.class, context.getString(R.string.browse_drink), R.drawable.bottle_wine_black));
        homeOptions.add(new HomeOption(HotelListActivity.class, context.getString(R.string.restaurant), R.drawable.restaurant_black));
        homeOptions.add(new HomeOption(RackListActivity.class, context.getString(R.string.my_rack), R.drawable.vault_black));
        //homeOptions.add(new HomeOption(HowToPlayActivity.class, context.getString(R.string.play_ipl), R.drawable.icon_cricket));
        homeOptions.add(new HomeOption(HowToPlayActivity.class, context.getString(R.string.play_fifa), R.drawable.icon_play_fifa));
        homeView.setHomeOptions(homeOptions);
    }
}
