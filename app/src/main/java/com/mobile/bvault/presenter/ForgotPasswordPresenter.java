package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.model.ForgotPassword;
import com.mobile.bvault.ui.view.ForgotPasswordView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 28/09/17.
 */

public class ForgotPasswordPresenter {

    private Context context;
    private ForgotPasswordView forgotPasswordView;

    public ForgotPasswordPresenter(Context context, ForgotPasswordView forgotPasswordView) {
        this.context = context;
        this.forgotPasswordView = forgotPasswordView;
    }

    public void forgotPassword(String username) {

        if (!NetworkUtils.isNetworkConnected(context)) {
            forgotPasswordView.onFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<ForgotPassword> passwordCall = userApiInterface.forgotPassword(username);
        passwordCall.enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                if (response.isSuccessful()) {
                    forgotPasswordView.onSuccess(response.body());
                } else {
                    try {
                        String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                        forgotPasswordView.onFailure(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                        forgotPasswordView.onFailure(response.message());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        forgotPasswordView.onFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                forgotPasswordView.onFailure(t.getMessage());
            }
        });
    }
}
