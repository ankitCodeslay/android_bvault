package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.data.sqlite.DBHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.ui.view.SignOutView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 28/09/17.
 */

public class SignOutPresenter {

    private Context context;
    private SignOutView signOutView;

    public SignOutPresenter(Context context, SignOutView signOutView) {
        this.context = context;
        this.signOutView = signOutView;
    }

    public void signOut() {

        if (!NetworkUtils.isNetworkConnected(context)) {
            signOutView.onSignOutFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(context).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> userCall =  userApiInterface.updateUserDeviceToken(userId,"");
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    signOutView.onSuccessfulSignOut();
                    AppPreferencesHelper.getInstance(context).removeUser();
                    DBHelper.getInstance(context).clearWishlist();
                } else {
                    signOutView.onSignOutFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                signOutView.onSignOutFailure(t.getMessage());
            }
        });
    }


}
