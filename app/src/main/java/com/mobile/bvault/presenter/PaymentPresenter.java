package com.mobile.bvault.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.mobile.bvault.AppEnvironment;
import com.mobile.bvault.BuildConfig;
import com.mobile.bvault.MyApp;
import com.mobile.bvault.R;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.ui.activity.WishlistActivity;
import com.mobile.bvault.ui.view.PaymentPresenterView;
import com.mobile.bvault.utils.CommonUtils;
import com.payu.magicretry.MainActivity;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by diptif on 13/11/17.
 */

public class PaymentPresenter {

    private static final String TAG = "PaymentPresenter";

    private Context context;
//    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;

    public PaymentPresenter(Context context) {
        this.context = context;
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hexString.toString();
    }

    /**
     * This function prepares the data for payment and launches payumoney plug n play sdk
     */
//    public void launchPayUMoneyFlow(double amount) {
//
//        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
//
//        //Use this to set your custom text on result screen button
////        payUmoneyConfig.setDoneButtonText("Done");
//
//        //Use this to set your custom title for the activity
//        payUmoneyConfig.setPayUmoneyActivityTitle("BVault");
//
//        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
//
//        String txnId = System.currentTimeMillis() + "";
//        String phone = AppPreferencesHelper.getInstance(context).getPhoneNumber();
//        String productName = "bvault";
//        String firstName = AppPreferencesHelper.getInstance(context).getFirstName();
//        String email = AppPreferencesHelper.getInstance(context).getEmail();
//        String udf1 = "";
//        String udf2 = "";
//        String udf3 = "";
//        String udf4 = "";
//        String udf5 = "";
//        String udf6 = "";
//        String udf7 = "";
//        String udf8 = "";
//        String udf9 = "";
//        String udf10 = "";
//
//
//        AppEnvironment appEnvironment = AppEnvironment.PRODUCTION;
//        builder.setAmount(amount)
//                .setTxnId(txnId)
//                .setPhone(phone)
//                .setProductName(productName)
//                .setFirstName(firstName)
//                .setEmail(email)
//                .setsUrl(appEnvironment.surl())
//                .setfUrl(appEnvironment.furl())
//                .setUdf1(udf1)
//                .setUdf2(udf2)
//                .setUdf3(udf3)
//                .setUdf4(udf4)
//                .setUdf5(udf5)
//                .setUdf6(udf6)
//                .setUdf7(udf7)
//                .setUdf8(udf8)
//                .setUdf9(udf9)
//                .setUdf10(udf10)
//                .setIsDebug(false)
//                .setKey(appEnvironment.merchant_Key())
//                .setMerchantId(appEnvironment.merchant_ID());
//
//        try {
//            mPaymentParams = builder.build();
//
//            /*
//            * Hash should always be generated from your server side.
//            * */
//            generateHashFromServer(mPaymentParams);
//
///*            *//**
//             * Do not use below code when going live
//             * Below code is provided to generate hash from sdk.
//             * It is recommended to generate hash from server side only.
//             * */
//            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
////           if (AppPreference.selectedTheme != -1) {
////                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MainActivity.this, AppPreference.selectedTheme,mAppPreference.isOverrideResultScreen());
////            } else {
//                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,((WishlistActivity)context), R.style.AppTheme_default, false);
////            }
//
//        } catch (Exception e) {
//            // some exception occurred
//            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
////            payNowButton.setEnabled(true);
//        }
//    }
//
//    /**
//     * This method generates hash from server.
//     *
//     * @param paymentParam payments params used for hash generation
//     */
//    public void generateHashFromServer(PayUmoneySdkInitializer.PaymentParam paymentParam) {
//        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.
//
//        HashMap<String, String> params = paymentParam.getParams();
//
//        // lets create the post params
//        StringBuffer postParamsBuffer = new StringBuffer();
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.KEY, params.get(PayUmoneyConstants.KEY)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.AMOUNT, params.get(PayUmoneyConstants.AMOUNT)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.TXNID, params.get(PayUmoneyConstants.TXNID)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.EMAIL, params.get(PayUmoneyConstants.EMAIL)));
//        postParamsBuffer.append(concatParams("productinfo", params.get(PayUmoneyConstants.PRODUCT_INFO)));
//        postParamsBuffer.append(concatParams("firstname", params.get(PayUmoneyConstants.FIRSTNAME)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF1, params.get(PayUmoneyConstants.UDF1)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF2, params.get(PayUmoneyConstants.UDF2)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF3, params.get(PayUmoneyConstants.UDF3)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF4, params.get(PayUmoneyConstants.UDF4)));
//        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF5, params.get(PayUmoneyConstants.UDF5)));
//
//        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();
//        CommonUtils.printErrorLog(TAG,"post parms: "+postParams);
//
//        // lets make an api call
//        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
//        getHashesFromServerTask.execute(postParams);
//    }
//
//    protected String concatParams(String key, String value) {
//        return key + "=" + value + "&";
//    }
//
//    /**
//     * This AsyncTask generates hash from server.
//     */
//    private class GetHashesFromServerTask extends AsyncTask<String, String, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected String doInBackground(String... postParams) {
//
//            String merchantHash = "";
//            try {
//                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
//                URL url = new URL(BuildConfig.BASE_URL + "api/get_hash");
//
//                String postParam = postParams[0];
//
//                byte[] postParamsByte = postParam.getBytes("UTF-8");
//
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setRequestMethod("POST");
//                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
//                conn.setDoOutput(true);
//                conn.getOutputStream().write(postParamsByte);
//
//                InputStream responseInputStream = conn.getInputStream();
//                StringBuffer responseStringBuffer = new StringBuffer();
//                byte[] byteContainer = new byte[1024];
//                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
//                    responseStringBuffer.append(new String(byteContainer, 0, i));
//                }
//
//                JSONObject response = new JSONObject(responseStringBuffer.toString());
//                CommonUtils.printErrorLog(TAG, response.toString());
//                Iterator<String> payuHashIterator = response.keys();
//                while (payuHashIterator.hasNext()) {
//                    String key = payuHashIterator.next();
//                    switch (key) {
//                        /**
//                         * This hash is mandatory and needs to be generated from merchant's server side
//                         *
//                         */
//                        case "payment_hash":
//                            merchantHash = response.getString(key);
//                            break;
//                        default:
//                            break;
//                    }
//                }
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (ProtocolException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return merchantHash;
//        }
//
//        @Override
//        protected void onPostExecute(String merchantHash) {
//            super.onPostExecute(merchantHash);
//
//            if (merchantHash.isEmpty() || merchantHash.equals("")) {
//                Toast.makeText(context, "Could not generate hash", Toast.LENGTH_SHORT).show();
//                ((WishlistActivity) context).hideLoading();
//            } else {
//                CommonUtils.printErrorLog(TAG,merchantHash);
////                mPaymentParams.setMerchantHash(merchantHash);
//
//
////                if (AppPreference.selectedTheme != -1) {
////                    PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, context, AppPreference.selectedTheme, mAppPreference.isOverrideResultScreen());
////                } else {
////                    PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,((WishlistActivity)context), R.style.AppTheme_default, false);
////                }
//            }
//        }
//    }
//
//    /**
//     * Thus function calculates the hash for transaction
//     *
//     * @param paymentParam payment params of transaction
//     * @return payment params along with calculated merchant hash
//     */
//    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {
//
//        StringBuilder stringBuilder = new StringBuilder();
//        HashMap<String, String> params = paymentParam.getParams();
//        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");
//
//        AppEnvironment appEnvironment = AppEnvironment.PRODUCTION;
//        stringBuilder.append(appEnvironment.salt());
//
//        String hash = hashCal(stringBuilder.toString());
//        CommonUtils.printErrorLog(TAG,"mobile side hash: "+hash);
//        paymentParam.setMerchantHash(hash);
//
//        return paymentParam;
//    }


}
