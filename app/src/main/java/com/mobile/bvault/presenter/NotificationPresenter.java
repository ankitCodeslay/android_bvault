package com.mobile.bvault.presenter;

import android.content.Context;

import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.network.HotelApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.ui.view.BottleListView;
import com.mobile.bvault.ui.view.NotificationView;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 21/08/17.
 */

public class NotificationPresenter {

    private NotificationView notificationView;
    private Context context;

    public NotificationPresenter(Context context,NotificationView notificationView) {
        this.context = context;
        this.notificationView = notificationView;
    }

    public void getRequestedPaymentList() {

        if (!NetworkUtils.isNetworkConnected(context)) {
            notificationView.onPaymentRequestFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(this.context).getUserId();
        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<List<Payment>> apiCall = hotelApiInterface.getRequestedPayment(userId);
        apiCall.enqueue(new Callback<List<Payment>>() {
            @Override
            public void onResponse(Call<List<Payment>> call, Response<List<Payment>> response) {
                if (response.isSuccessful()) {
                    notificationView.onPaymentRequestLoadedSuccessfully(response.body());
                }else {
                    notificationView.onPaymentRequestFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Payment>> call, Throwable t) {
                notificationView.onPaymentRequestFailure(t.getMessage());
            }
        });
    }
}
