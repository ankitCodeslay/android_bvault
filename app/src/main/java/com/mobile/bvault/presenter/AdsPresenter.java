package com.mobile.bvault.presenter;

import android.content.Context;
import android.widget.ImageView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.HotelApiInterface;
import com.mobile.bvault.model.Advertisement;
import com.mobile.bvault.ui.view.AdsView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.NetworkUtils;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 18/09/17.
 */

public class AdsPresenter {

    private static final String TAG = "AdsPresenter";

    private Context context;
    private AdsView adsView;

    public AdsPresenter(Context context, AdsView adsView) {
        this.context = context;
        this.adsView = adsView;
    }

    public void getAds(){
//        adsView.onAdsLoadedSuccessfully(null);

        if (!NetworkUtils.isNetworkConnected(context)) {
            adsView.onAdsFailure(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        HotelApiInterface hotelApiInterface = ApiClient.getClient().create(HotelApiInterface.class);
        Call<List<Advertisement>> adsCall = hotelApiInterface.getAdvertisement();
        adsCall.enqueue(new Callback<List<Advertisement>>() {
            @Override
            public void onResponse(Call<List<Advertisement>> call, Response<List<Advertisement>> response) {
                if (response.isSuccessful()){
                    adsView.onAdsLoadedSuccessfully(response.body());
                }else {
                    adsView.onAdsFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Advertisement>> call, Throwable t) {
                adsView.onAdsFailure(t.getMessage());
            }
        });
    }

    public void setAds(CarouselView carouselView, final List<Advertisement> advertisementList) {

//        final int[] sampleImages = {R.drawable.ads, R.drawable.ads, R.drawable.ads, R.drawable.ads, R.drawable.ads};
        if (carouselView != null && advertisementList != null) {
            carouselView.reSetSlideInterval(15000);
            CommonUtils.printErrorLog(TAG,"advertisement size "+advertisementList.size());
            CommonUtils.printErrorLog(TAG,"advertisement size "+advertisementList.size());
            carouselView.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
//                    imageView.setImageResource();
                    CommonUtils.printErrorLog(TAG,"Image name "+advertisementList.get(position).getImageUrl());
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    String imageUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_ADS,advertisementList.get(position).getImageUrl());
                    ImageUtil.setImage(context,imageView,imageUrl, R.drawable.banner);
                }
            });
            carouselView.setPageCount(advertisementList.size());
//            carouselView.setPageCount(sampleImages.length);
        }
    }
}
