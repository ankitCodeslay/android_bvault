package com.mobile.bvault.presenter;

import android.content.Context;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by diptif on 21/09/17.
 */

public class MapPresenter implements OnMapReadyCallback{

    private Context context;
    private SupportMapFragment mapFragment;
    MarkerOptions markerOptions;
    LatLng latLng;

    public MapPresenter(Context context) {
        this.context = context;
//        mapFragment = (SupportMapFragment) ((AppCompatActivity) this.context).getSupportFragmentManager().findFragmentById(R.id.map);
    }

    public void setLocation(double lat, double lng) {
        latLng = new LatLng(lat,lng);
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (markerOptions != null && latLng != null) {
            googleMap.addMarker(markerOptions);
            googleMap.getUiSettings().setMapToolbarEnabled(true);
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(latLng,14f,0,0)));
        }
    }
}
