package com.mobile.bvault;

import android.app.ActivityManager;
import android.app.Application;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OSNotificationPayload;
import com.onesignal.OneSignal;
import com.mobile.bvault.ui.activity.HomeActivity;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;
import static android.media.RingtoneManager.getDefaultUri;

/**
 * Created by diptif on 17/08/17.
 */

public class MyApp extends MultiDexApplication {

    private static final String TAG = "MyApp";
    AppEnvironment appEnvironment;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .setNotificationReceivedHandler(new ExampleNotificationReceivedHandler())
                .init();

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );

        appEnvironment = AppEnvironment.PRODUCTION;
    }

    // This fires when a notification is opened by tapping on it or one is received while the app is running.
    class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            try {
                OSNotificationPayload payload = result.notification.payload;
                JSONObject data = payload.additionalData;
                openNotification(data);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //   app in foreground
    class ExampleNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            OSNotificationPayload payload = notification.payload;
            JSONObject data = payload.additionalData;
            if (data == null) return;
            if (data.has("message")) {
                if (isAppInForeground()) {
                    try {
                        handleDataMessage(data.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                broadCastMessage(data);
            }
        }
    }

    public void broadCastMessage(JSONObject data) {
        try {
            Intent intent = new Intent();
            intent.putExtras(getBundle(data));
            intent.setAction(Constants.INTENT_FILTER_BROADCAST_NOTIFICATION);
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openNotification(final JSONObject data) {
        try {

            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.putExtras(getBundle(data));
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bundle getBundle(JSONObject jsonObject) {
        Bundle bundle = null;
        try {
            String paymentId = jsonObject.getString(Constants.PAYMENT_RECEIPT_ID);
            bundle = new Bundle();
            bundle.putString(Constants.PAYMENT_RECEIPT_ID, paymentId);

        } catch (Exception e) {
            e.printStackTrace();
            bundle = null;
        }
        return bundle;
    }

    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }

    public void setAppEnvironment(AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }


    private void handleDataMessage(String message) {
        Context context = getApplicationContext();
        if (context == null) {
            return;
        }
        try {
            String title = "Bvault";
            long timestamp = System.currentTimeMillis();

            final int icon = R.drawable.notification_icon;
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            final PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

            Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.bigText(message);
            bigTextStyle.setBigContentTitle(title);

            Notification notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                    .setStyle(bigTextStyle)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(resultPendingIntent)
                    .setSound(alarmSound)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setWhen(timestamp)
                    .setSmallIcon(getNotificationIcon())
                    .setPriority(Notification.PRIORITY_HIGH)
                    //.setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setContentText(message)
                    .build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify((int) System.currentTimeMillis(), notification);
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon : R.drawable.notification_icon;
    }

    private boolean isAppInForeground() {
        Context context = getApplicationContext();
        if (context == null) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
            String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();

            return foregroundTaskPackageName.toLowerCase().equals(context.getPackageName().toLowerCase());
        } else {
            ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(appProcessInfo);
            if (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE) {
                return true;
            }

            KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            // App is foreground, but screen is locked, so show notification
            return km.inKeyguardRestrictedInputMode();
        }
    }

}
