package com.mobile.bvault.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diptif on 16/06/16.
 */
public class PermissionUtil {

    public static final String[] CAMERA_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static final String[] CALL_PERMISSION = {
            Manifest.permission.CALL_PHONE
    };

    public static final String[] LOCATION_PERMISSION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    public static boolean hasPermissionsGranted(Context context, String[] permission) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            return hasRequestedPermission(permission, context);
        }else {
            return true;
        }
    }

    public static boolean hasRequestedPermission(String[] permissions, Context context){
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    public static boolean shouldShowRequestPermissionRationale(Activity activity, String[] permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true;
            }
        }
        return false;
    }


    public static void requestToAcceptDeniedPermissions(String[] permissions, Activity activity, int REQUEST_VIDEO_PERMISSIONS){
        List<String> deniedPermissionArrayList = new ArrayList<String>();
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
               deniedPermissionArrayList.add(permission);
            }
        }
        String[] convertedArray = deniedPermissionArrayList.toArray(new String[deniedPermissionArrayList.size()]);

        if (deniedPermissionArrayList.size() > 0)
        ActivityCompat.requestPermissions(activity, convertedArray, REQUEST_VIDEO_PERMISSIONS);

    }

    public static void requestPermissions(Activity activity, int REQUEST_PERMISSION_CODE, String[] permission) {
        if (!shouldShowRequestPermissionRationale(activity,permission)) {
            ActivityCompat.requestPermissions(activity, permission, REQUEST_PERMISSION_CODE);
        } else {
            requestToAcceptDeniedPermissions(permission, activity, REQUEST_PERMISSION_CODE);
        }
    }
}
