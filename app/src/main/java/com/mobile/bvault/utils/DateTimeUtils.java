package com.mobile.bvault.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Mayank on 14/07/2016.
 */
public class DateTimeUtils {


    public static String getDateByTimestamp(String sTime) {
        String date = "";
        try {
            long timestamp = Long.valueOf(sTime) * 1000;
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timestamp);
            date = android.text.format.DateFormat.format("yyyy-MM-dd", cal).toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getTimeByTimestamp(String sTime) {
        String time = "";
        try {
            long timestamp = Long.valueOf(sTime) * 1000;
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timestamp);
            time = android.text.format.DateFormat.format("HH:mm", cal).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    public static String convertDate(String date) {

        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;

        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf1 = new SimpleDateFormat("dd MMM yyyy");
            result = sdf1.format(sdf.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

}
