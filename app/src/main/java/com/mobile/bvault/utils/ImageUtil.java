package com.mobile.bvault.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.File;

/**
 * Created by diptif on 09/10/17.
 */

public class ImageUtil {

    private static final String TAG = "ImageUtil";

    public static File getOutputMediaFile() {


        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStorageDirectory(),
                Constants.DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(Constants.DIRECTORY_NAME, "Oops! Failed create "
                        + Constants.DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = String.valueOf(System.currentTimeMillis());

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + Constants.FILE_NAME_IMAGE + timeStamp + Constants.FILE_EXTENSION_PNG);

        return mediaFile;
    }

    public static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    public static int getPurchaseTypeIcon(String type) {
        int res = R.drawable.ic_star_silver_24dp;
        if (type.equalsIgnoreCase("platinum")){
            res = R.drawable.ic_star_platinum_24dp;
        }else if (type.equalsIgnoreCase("gold")){
            res = R.drawable.ic_star_gold_24dp;
        }else {
            res = R.drawable.ic_star_silver_24dp;
        }
        return res;
    }

    public static void openMediaSelector(int requestCode, Context context, Uri uri){
        try{
            Intent captureIntent = new Intent(
                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");

            // Create file chooser intent
            Intent chooserIntent = Intent.createChooser(i, "Select Profile Image");

            // Set camera intent to file chooser
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                    , new Parcelable[] { captureIntent });

            // On select image call onActivityResult method of activity
            ((AppCompatActivity)context).startActivityForResult(chooserIntent, requestCode);

        }
        catch(Exception e){
            Toast.makeText(context, "Exception:" + e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }

    }

    public static void startCropActivity(@NonNull Uri uri, Context context) {
//        UCrop.Options options = new UCrop.Options();
        CommonUtils.printErrorLog(TAG, uri.toString());
        Uri destination = Uri.fromFile(new File(context.getCacheDir(), generateFileName()));
        UCrop uCrop = UCrop.of(uri, destination);
        uCrop.withAspectRatio(1, 1);
//        options.setToolbarColor(ContextCompat.getColor(context, R.color.colorAccent));
//        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
//        uCrop.withOptions(options);
        uCrop.start(((AppCompatActivity)context));
    }

    public static void handleCropError(@NonNull Intent result,Context context) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(context, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    public static String generateFileName(){
        return Constants.FILE_NAME_IMAGE + "_" +String.valueOf(System.currentTimeMillis()) + Constants.FILE_EXTENSION_PNG;
    }

    public static Bitmap getBitmapFromFilePath(String path){
        return BitmapFactory.decodeFile(path);
    }

    public static void setImage(Context context,ImageView imageView, String url, int placeholderImage) {
        Picasso.with(context).load(url).placeholder(placeholderImage).into(imageView);
    }
}
