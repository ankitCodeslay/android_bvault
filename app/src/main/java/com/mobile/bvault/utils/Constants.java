package com.mobile.bvault.utils;

/**
 * Created by diptif on 17/08/17.
 */

public class Constants {


    //    Directory Name
    public static final String DIRECTORY_NAME = "BVault";
    public static final String FILE_NAME_IMAGE = "BVault";
    public static final String FILE_EXTENSION_PNG = ".png";

    //    KEY
    public static final String DRINK_TYPE = "drink_type";
    public static final String DRINK_TYPE_ID = "drink_type_id";
    public static final String BOTTLE = "bottle";
    public static final String HOTEL = "hotel";
    public static final String HOTEL_ID = "hotelId";
    public static final String RACK = "rack";
    public static final String MESSAGE = "message";
    public static final String PLAYER_ID = "playerId";
    public static final String HOTEL_NAME = "hotelName";
    public static final String BOTTLE_NAME = "bottleName";
    public static final String BOTTLE_CODE = "bottleCode";
    public static final String QUANTITY = "quantity";
    public static final String COST = "cost";
    public static final String TAX = "tax";
    public static final String PAYMENT_RECEIPT_ID = "paymentReceiptId";
    public static final String TRANSACTION_ID = "tnxId";


    //    Request Code
    public static final int LOCATION_REQUEST_CODE = 1001;
    public static final int REQUEST_CHANGE_IMAGE_CODE = 1002;
    public static final int REQUEST_PERMISSION_CODE = 1003;
    public static final int REQUEST_CALL_CODE = 1004;
    public static final int CONTACT_REQUEST_CODE = 1005;
    public static final int PAYTM_REQUEST_CODE = 1006;

    //    Date Format
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_MM_DD_YYYY = "MM/dd/yyyy";
    public static final String DATE_DD_MM_YYYY = "dd/MM/yyyy";
    public static final String DATE_DD_MMM = "dd MMM";
    public static final String DATE_hh_MM_a = "hh:mm a";

    //    AWS Folder Names
    public static final String FOLDER_PROFILE_PICTURE = "ProfilePictures";
    public static final String FOLDER_ADS = "Advertisements";
    public static final String FOLDER_BOTTLES = "Bottles";
    public static final String FOLDER_HOTELS = "Hotels";

    //    Notificatoin
    public static final String INTENT_FILTER_BROADCAST_NOTIFICATION = "com.notification.BVAULT";

    //    Messages
    public static final String NO_INTERNET_CONNECTION = "No internet connection";

    /*public static final int BOTTLE1_POINT = 250;
    public static final int BOTTLE2_POINT = 325;
    public static final int BOTTLE3_POINT = 350;
    public static final int BOTTLE4_POINT = 350;
*/
    public static final int BOTTLE1_POINT = 100;
    public static final int BOTTLE2_POINT = 100;
    public static final int BOTTLE3_POINT = 100;
    public static final int BOTTLE4_POINT = 120;

    public static final int BOTTLE_TYPE_1 = 1;
    public static final int BOTTLE_TYPE_2 = 2;
    public static final int BOTTLE_TYPE_3 = 3;
    public static final int BOTTLE_TYPE_4 = 4;

    public static final String IPL_IMAGE_URL = "https://s3.ap-south-1.amazonaws.com/bvault/IPL/";

    /* otp  */
    public static final long timeafterwhichRecieverUnregister = 900000l;
    public static final long longTimeAfterWhichButtonEnable = 31000;
    public static final long longTotalVerificationTime = 31000;
    public static final long longOneSecond = 1000;
    public static final String EVENT_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    public static final int PRIORITY_HIGH = 2147483647;
    public static final String keyOtp = "OtpIs";
    public static final String entityNotPresent = "notPresent";
    public static final String entityNotPresentInt = "0";
    public static final String keyMobileNo = "mobileNumber";
    public static final int RESET_LIMIT = 5;

}
