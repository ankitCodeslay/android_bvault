package com.mobile.bvault.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by diptif on 19/09/17.
 */

public class LocationUtils {

    private static final String TAG = "LocationUtils";

    private Context context;
    private OnLocationListener onLocationListener;

    private FusedLocationProviderClient mFusedLocationClient;

    public LocationUtils(Context context, OnLocationListener onLocationListener) {
        this.context = context;
        this.onLocationListener = onLocationListener;
    }

    public void getCurrentLocation() {
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.requestPermissions(((AppCompatActivity)context),Constants.LOCATION_REQUEST_CODE,PermissionUtil.LOCATION_PERMISSION);
            }else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(new com.google.android.gms.tasks.OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            onLocationListener.onLocationSuccess(location);
                        }else {
                            onLocationListener.onLocationFailure();
                        }

                    }
                });
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnLocationListener {

        void onLocationSuccess(Location location);

        void onLocationFailure();
    }


}
