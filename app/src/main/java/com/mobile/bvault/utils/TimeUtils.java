package com.mobile.bvault.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by diptif on 04/10/17.
 */

public class TimeUtils {

    private static final String TAG = "TimeUtils";

    public static int isEqualDate(String date1,String date2) {
        int res = -1;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.ISO_DATE_FORMAT);
        SimpleDateFormat dayMonthYearFormatter = new SimpleDateFormat(Constants.DATE_DD_MM_YYYY);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            Date d1 = dayMonthYearFormatter.parse(dayMonthYearFormatter.format(simpleDateFormat.parse(date1)));
            Date d2 = dayMonthYearFormatter.parse(dayMonthYearFormatter.format(simpleDateFormat.parse(date2)));
            res = d1.compareTo(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getHistoryDay(String date) {
        String strDate = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.ISO_DATE_FORMAT);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat dayMonthFormatter = new SimpleDateFormat(Constants.DATE_DD_MMM, Locale.ENGLISH);
        try {
            strDate = dayMonthFormatter.format(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }finally {
            return strDate.replace(" ","\n");
        }
    }

    public static String getFormattedDob(Date date) {
        String strDate = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_MM_DD_YYYY);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        strDate = simpleDateFormat.format(date);
        return strDate;
    }

    public static String getISOFormattedDOB(String date) {

        if (date == null || date.isEmpty())
            return "";

        String dob = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.ISO_DATE_FORMAT);
        SimpleDateFormat dayMonthFormatter = new SimpleDateFormat(Constants.DATE_DD_MM_YYYY);
        try {
            dob = dayMonthFormatter.format(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dob;
    }

    public static String getFormattedTime(String date) {
        String strTime = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.ISO_DATE_FORMAT);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat dayMonthFormatter = new SimpleDateFormat(Constants.DATE_hh_MM_a);

        try {
            strTime = dayMonthFormatter.format(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strTime;
    }



    public static long getDays(String date) {
        long days = 0;
        DateFormat dateFormat = new SimpleDateFormat(Constants.ISO_DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date formattedDate = dateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(formattedDate);
            long remainingDays = calendar.getTimeInMillis() - System.currentTimeMillis();
            days = remainingDays / 86400000;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }
}
