package com.mobile.bvault.utils;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.bvault.BuildConfig;
import com.mobile.bvault.R;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.Price;

import java.text.NumberFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.facebook.GraphRequest.TAG;

/**
 * Created by diptif on 16/08/17.
 */

public class CommonUtils {

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    /**
     * Display the loading dialog while waiting for api response
     *
     * @param context
     * @return progressDialog
     */
    public static Dialog showLoadingBar(Context context) {
        Dialog progressDialog = new Dialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    /**
     * Hide the progress dialog
     *
     * @param progressDialog
     */
    public static void hideLoadingBar(Dialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    /**
     * Check email is valid or not
     *
     * @param email entered email id
     * @return if valid email returns true else false
     */
    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Print error logs on console
     *
     * @param tag     key value or class name
     * @param message error message to print
     */
    public static void printErrorLog(String tag, String message) {
        if (BuildConfig.DEBUG && message != null) {
            Log.e(tag, message);
        }
    }

    /**
     * Print message for debugging purpose on console
     *
     * @param tag     key value or class name
     * @param message message to print
     */
    public static void printMessageLog(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message);
    }

    public static int getCollectionSize(Collection collection) {
        return collection != null && collection.size() > 0 ? collection.size() : 0;
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    public static String[] getFirstNameAndLastName(String userName) {
        String[] nameArray = new String[2];
        if (userName == null || userName.isEmpty()) {
            nameArray[0] = "";
            nameArray[1] = "";
        } else {
            nameArray = new String[2];
            String[] tempArray = userName.split(" ");
            nameArray[0] = tempArray[0];
            nameArray[1] = "";
            if (tempArray.length > 1) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 1; i < tempArray.length; i++) {
                    stringBuilder.append(tempArray[i] + " ");
                }
                nameArray[1] = stringBuilder.toString().trim();
            }
        }
        return nameArray;
    }

    public static int getMinimumPrice(List<Price> priceList) {
        CommonUtils.printErrorLog("size", "" + priceList.size());
        Comparator<Price> comparator = new Comparator<Price>() {
            @Override
            public int compare(Price o1, Price o2) {
                return Integer.valueOf(o1.getPrice()).compareTo(Integer.valueOf(o2.getPrice()));
            }
        };
        Collections.sort(priceList, comparator);
        return priceList.get(0).getPrice();
    }

    public static void makePhoneCall(Context context, String number) {
        CommonUtils.printErrorLog(TAG, "inside make phone call function");
        String uri = "tel:" + number;
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtil.requestPermissions(((AppCompatActivity) context), Constants.REQUEST_CALL_CODE, PermissionUtil.CALL_PERMISSION);
        } else {
            CommonUtils.printErrorLog(TAG, "inside start activity");
            ((AppCompatActivity) context).startActivity(intent);
        }
    }

    public static String commaFormatter(Number number) {
        return NumberFormat.getInstance().format(number);
    }

    public static boolean hasMobAndDob(Context context) {
        boolean hasMobAndDob = false;
        if (context != null) {
            String number = AppPreferencesHelper.getInstance(context).getPhoneNumber();
            String dob = AppPreferencesHelper.getInstance(context).getDOB();
            if (number != null && !number.isEmpty() && dob != null && !dob.isEmpty()) {
                hasMobAndDob = true;
            } else {
                hasMobAndDob = false;
            }
        }
        return hasMobAndDob;
    }

    public static void showToastMessage(String message, Context context) {
        if (message != null && context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static void showTermsAndCondition(Context context) {
        if (context != null) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.BASE_URL + "termsAndConditions")));
        }
    }

    public static void setFont(TextView textView, Context context) {
        if (textView != null && context != null) {
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/Tryst-Regular.ttf");
            textView.setTypeface(type);
        }
    }

}
