package com.mobile.bvault.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.presenter.AcceptPaymentPresenter;
import com.mobile.bvault.ui.view.AcceptPaymentView;

/**
 * Created by diptif on 17/11/17.
 */

public class NotificationReceiver extends BroadcastReceiver implements AcceptPaymentView {

    private static final String TAG = "NotificationReceiver";

    private Context context;
    AcceptPaymentPresenter acceptPaymentPresenter;

    public NotificationReceiver(Context context) {
        this.context = context;
    }

    public void registerReceiver() {
        LocalBroadcastManager.getInstance(context).registerReceiver(this,new IntentFilter(Constants.INTENT_FILTER_BROADCAST_NOTIFICATION));
    }

    public void unregisterReceiver() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        CommonUtils.printErrorLog(TAG,"Payment Received");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String paymentId = bundle.getString(Constants.PAYMENT_RECEIPT_ID);
            acceptPaymentPresenter = new AcceptPaymentPresenter(NotificationReceiver.this.context,this);
            acceptPaymentPresenter.getPaymentRequestByID(paymentId);
        }
    }


    @Override
    public void onPaymentStatusChangeSuccessful(String message) {
        ((BaseActivity) NotificationReceiver.this.context).hideLoading();
        acceptPaymentPresenter.hideDialog();
        CommonUtils.showToastMessage(message,context);

    }

    @Override
    public void onPaymentStatusChangeFailure(String message) {
        ((BaseActivity) NotificationReceiver.this.context).hideLoading();
        acceptPaymentPresenter.hideDialog();
        CommonUtils.showToastMessage(message,context);
    }

    @Override
    public void onPaymentByIdSuccess(Payment payment) {
        ((BaseActivity) NotificationReceiver.this.context).hideLoading();
        acceptPaymentPresenter.setPayment(payment);
        acceptPaymentPresenter.showDialog();
    }

    @Override
    public void onPaymentByIdFailure(String message) {
        ((BaseActivity) NotificationReceiver.this.context).hideLoading();
        //CommonUtils.showToastMessage(message,context);
    }
}
