package com.mobile.bvault.data.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.mobile.bvault.model.Wishlist;
import com.mobile.bvault.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diptif on 28/08/17.
 */

public class DBHelper extends SQLiteOpenHelper implements DBHelperInterface{

    private static final String TAG = "DBHelper";

    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "iXarba_db";
    private static final String WISHLIST_TABLE = "wishlist";
    private static final String WISHLIST_ID = "wishlist_id";
    private static final String BOTTLE_ID = "bottle_id";
    private static final String BOTTLE_NAME = "bottle_name";
    private static final String BOTTLE_DESCRIPTION = "bottle_description";
    private static final String IMAGE_URL = "image_url";
    private static final String QTY = "qty";
    private static final String PRICE = "price";
    private static final String VOLUME = "volume";
    private static final String TYPE = "type";
    private static final String UNIT = "unit";

    private static DBHelper dbHelper;

    public static DBHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new DBHelper(context,DB_NAME,null,DB_VERSION);
        }
        return dbHelper;
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("oldVersion",""+oldVersion);
        Log.e("newVersion",""+newVersion);
        String query = "DROP TABLE IF EXISTS "+ WISHLIST_TABLE;
        db.execSQL(query);
        onCreate(db);
    }

    @Override
    public void createTable(SQLiteDatabase db) {
        String query = "CREATE TABLE "+ WISHLIST_TABLE +" (" +
                WISHLIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                BOTTLE_ID + " TEXT, " +
                BOTTLE_NAME + " TEXT NOT NULL, " +
                BOTTLE_DESCRIPTION + " TEXT NOT NULL, " +
                IMAGE_URL + " TEXT NOT NULL, " +
                QTY + " INTEGER DEFAULT 1, " +
                PRICE + " INTEGER DEFAULT 0, " +
                VOLUME + " INTEGER DEFAULT 0, " +
                TYPE + " TEXT NOT NULL, " +
                UNIT + " TEXT NOT NULL);";
        try {
            SQLiteStatement statement = db.compileStatement(query);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void upgradeDB(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public int getItemCount() {
        int count = 0;
        String query = "SELECT COUNT (*) FROM "+ WISHLIST_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(query,null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            CommonUtils.printErrorLog(TAG,"Row Count: "+count);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeDB();
            return count;
        }
    }

    @Override
    public long insertWishlist(Wishlist wishlist) {
        long result = -1;
        String insertQuery = "INSERT INTO "+ WISHLIST_TABLE +" ( " +
                BOTTLE_ID + ", " +
                BOTTLE_NAME + ", " +
                BOTTLE_DESCRIPTION + ", " +
                IMAGE_URL + ", " + QTY + ", " +
                PRICE + ", " +
                VOLUME + ", " +
                TYPE + ", " +
                UNIT +
                ") VALUES (?,?,?,?,?,?,?,?,?)";
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            SQLiteStatement statement = db.compileStatement(insertQuery);
            statement.bindString(1, wishlist.getBottleId());
            statement.bindString(2, wishlist.getName());
            statement.bindString(3, wishlist.getDescription());
            statement.bindString(4, wishlist.getImageURL());
            statement.bindString(5, ""+ wishlist.getQty());
            statement.bindString(6, ""+ wishlist.getPrice());
            statement.bindString(7, ""+wishlist.getVolume());
            statement.bindString(8, ""+wishlist.getType());
            statement.bindString(9, ""+wishlist.getUnit());
            result = statement.executeInsert();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeDB();
            return result;
        }
    }

    @Override
    public List<Wishlist> getWishlists() {
        List<Wishlist> wishlists = null;
        String getQuery = "SELECT * FROM " + WISHLIST_TABLE;
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(getQuery,null);
            if (cursor.getCount() > 0) {
                wishlists = new ArrayList<Wishlist>();
                cursor.moveToFirst();
                do {
                    int wishlistId = cursor.getInt(0);
                    String bottleId = cursor.getString(1);
                    String name = cursor.getString(2);
                    String desc = cursor.getString(3);
                    String imageUrl = cursor.getString(4);
                    int qty = cursor.getInt(5);
                    CommonUtils.printErrorLog(TAG,"qty: "+qty);
                    int price = cursor.getInt(6);
                    int volume = cursor.getInt(7);
                    String type = cursor.getString(8);
                    String unit = cursor.getString(9);
                    wishlists.add(
                            new Wishlist(
                                wishlistId,bottleId,name,price,desc,imageUrl,qty,volume,type,unit
                            )
                    );
                }while (cursor.moveToNext());
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeDB();
            return wishlists;
        }
    }

    @Override
    public int updateQuantityPriceAndVolume(int wishlistId, int qty, int price, int volume) {
        int result = -1;
        String updateQuery = "UPDATE "+ WISHLIST_TABLE + " SET "+ QTY + "= ?, " + PRICE + " = ? WHERE " + WISHLIST_ID + " = ?";

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            SQLiteStatement statement = db.compileStatement(updateQuery);
            statement.bindString(1, ""+qty);
            statement.bindString(2, ""+price);
            statement.bindString(3, ""+wishlistId);
            result = statement.executeUpdateDelete();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeDB();
            CommonUtils.printErrorLog(TAG,"result: "+result);
            return result;
        }
    }

    @Override
    public int getTotalAmount() {
        int amount = 0;
        String query = "SELECT SUM("+ PRICE +") AS "+ PRICE +" FROM " + WISHLIST_TABLE +";";
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(query,null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                amount = cursor.getInt(0);
                CommonUtils.printErrorLog(TAG,"Total Amount : "+amount);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeDB();
            return amount;
        }
    }

    @Override
    public int deleteItem(int id) {
        int result = -1;
        String deleteQuery = "DELETE FROM " + WISHLIST_TABLE + " WHERE " + WISHLIST_ID + " = ?";
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            SQLiteStatement statement = db.compileStatement(deleteQuery);
            statement.bindString(1, ""+id);
            result = statement.executeUpdateDelete();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeDB();
            return result;
        }
    }

    @Override
    public boolean isAlreadyAdded(String bottleId, int volume) {
        boolean isAdded = false;
        String query = "SELECT 1 FROM " + WISHLIST_TABLE + " WHERE " + BOTTLE_ID +" = '"+bottleId + "'" + " and " + VOLUME + " = " + volume;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(query,null);
            if (cursor.getCount() > 0) isAdded = true;
            else isAdded = false;
            CommonUtils.printErrorLog(TAG,"Is Added: "+isAdded);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeDB();
            return isAdded;
        }
    }

    @Override
    public void clearWishlist() {
        String deleteQuery = "DELETE FROM " + WISHLIST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(deleteQuery);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeDB();
        }
    }

    @Override
    public void closeDB() {
        this.close();
    }

}
