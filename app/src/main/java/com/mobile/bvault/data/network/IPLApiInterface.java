package com.mobile.bvault.data.network;

import com.mobile.bvault.model.DateTimeModel;
import com.mobile.bvault.model.GetPlayerModel;
import com.mobile.bvault.model.IPLMatchModel;
import com.mobile.bvault.model.IPLTeamModel;
import com.mobile.bvault.model.SubmitIPLAnsModel;
import com.mobile.bvault.model.UserEarningModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by diptif on 18/09/17.
 */

public interface IPLApiInterface {

    /**
     * Retrofit api interface for fetching the match list from server
     *
     * @return list of match
     */
    @GET("api/getMatchList")
    Call<List<IPLMatchModel>> getAllMatches();

    /**
     * Retrofit api interface for fetching the earning points from server
     */
    @GET("api/getUserEarningPoint")
    Call<UserEarningModel> getUserEarningPoints(@Query("userId") String userId);

    /**
     * Retrofit api interface for update the earning points to server
     */
    @POST("api/updateUserEarningPoint")
    Call<UserEarningModel> updateUserEarningPoints(@Body UserEarningModel userEarningModel);

    /**
     * Fetches the list of players
     *
     * @return list of players
     */
    @POST("api/getTeamPlayerLists")
    Call<IPLTeamModel> getPlayerList(@Body GetPlayerModel getPlayerModel);

    /**
     * Submit the ans of match
     */
    @POST("api/submitUserAns")
    Call<SubmitIPLAnsModel> submitAnswer(@Body SubmitIPLAnsModel submitIPLAnsModel);

    /**
     * VALIDATE THE ANS
     */
    @POST("api/validateUserAns")
    Call<SubmitIPLAnsModel> validateAnswer(@Body SubmitIPLAnsModel submitIPLAnsModel);

    /**
     * Retrofit api interface for fetching the currentDateTime from server
     */
    @GET("api/getCurrentTimestamp")
    Call<DateTimeModel> getCurrentDateTime();

}
