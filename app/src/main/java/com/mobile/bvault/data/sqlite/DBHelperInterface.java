package com.mobile.bvault.data.sqlite;

import android.database.sqlite.SQLiteDatabase;

import com.mobile.bvault.model.Wishlist;

import java.util.List;

/**
 * Created by diptif on 28/08/17.
 */

public interface DBHelperInterface {

    /**
     * Creates the Wishlist table
     * @param db
     */
    void createTable(SQLiteDatabase db);

    /**
     * Called after db version updated
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    void upgradeDB(SQLiteDatabase db, int oldVersion, int newVersion);


    /**
     * Gets the number of bottles added by the user to the wishlist
     * @return number of bottle
     */
    int getItemCount();

    /**
     * Inserts the wishlist items into SQlite db
     * @param wishlist
     */
    long insertWishlist(Wishlist wishlist);

    /**
     * Load the wishlist from the sqlite db
     * @return list of items added to cart
     */
    List<Wishlist> getWishlists();

    /**
     * Gets total amount from sqlite
     * @return total amount
     */
    int getTotalAmount();

    /**
     * Updates the quantity
     * @param qty number of qty
     * @param wishlistId Wishlist table id
     * @return if fails -1 otherwise positive number
     */
    int updateQuantityPriceAndVolume(int wishlistId, int qty, int price, int volume);

    /**
     * Removes the wishlist from the table
     * @param id wishlist table id
     * @return if failure -1 else positive number
     */
    int deleteItem(int id);

    /**
     * Checks whether bottle is already added to the order list;
     * @param bottleId bottle Id
     * @param volume volume/ml
     * @return if added returns true otherwise false
     */
    boolean isAlreadyAdded(String bottleId, int volume);

    /**
     * Delete all the data from wishlist table
     */
    void clearWishlist();

    /**
     * Closes the database connection
     */
    void closeDB();

}
