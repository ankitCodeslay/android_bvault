package com.mobile.bvault.data.pref;

import com.google.android.gms.maps.model.LatLng;
import com.mobile.bvault.model.User;

/**
 * Created by diptif on 17/08/17.
 */

public interface PreferencesHelper {

    void setLatLng(LatLng latLng);

    LatLng getLatLng();

    void setCurrentCity(String currentCity);

    String getCurrentCity();

    void setUser(User user);

    User getUser();

    void removeUser();

    void setUserName(String username);

    String getUserName();

    void setEmail(String email);

    String getEmail();

    void setUserId(String userId);

    String getUserId();

    void setDisplayName(String displayName);

    String getDisplayName();

    void setFirstName(String firstName);

    String getFirstName();

    void setLastName(String lastName);

    String getLastName();

    void setPhoneNumber(String phoneNumber);

    String getPhoneNumber();

    void setLoggedIn(Boolean isLoggedIn);

    boolean isLoggedIn();

    void setFavouriteDrink(String favouriteDrink);

    String getFavouriteDrink();

    void setDOB(String dob);

    String getDOB();

    void setImageUrl(String imageUrl);

    String getImageUrl();

    void setFirstTimeLaunch(boolean isFirstTime);

    boolean isFirstTimeLaunch();

    void setDeviceToken(String deviceToken);

    String getDeviceToken();

    void setFacebookOrGoogleLogin(boolean isFacebookOrGoogleLogin);

    boolean isFacebookOrGoogleLogin();
}
