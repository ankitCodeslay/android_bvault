package com.mobile.bvault.data.network;

import com.mobile.bvault.model.Advertisement;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.model.NotificationCount;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.model.PaymentResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by diptif on 18/09/17.
 */

public interface HotelApiInterface {

    /**
     * Retrofit api interface for fetching the hotel list from server
     * @return list of hotels
     */
    @GET("api/hotels/mobile")
    Call<List<Hotel>> getHotels();

    /**
     * Fetches the list of hotels near the location
     * @param lat latitude
     * @param lng longitude
     * @return list of hotels
     */
    @GET("api/hotels/mobile")
    Call<List<Hotel>> getHotels(@Query("lat") Double lat, @Query("lng") Double lng);

    /**
     * Retrofit rest api call for fetching hotels based on bottle id
     * @return list of advertisements
     */
    @GET("api/hotelsByBottleId")
    Call<List<Hotel>> getHotels(@Query("bottleId") String bottleId,@Query("lat") Double lat, @Query("lng") Double lng);

    /**
     * Retrofit rest api call for fetching hotels based on bottle id without location
     * @return list of advertisements
     */
    @GET("api/hotelsByBottleId")
    Call<List<Hotel>> getHotels(@Query("bottleId") String bottleId);

    /**
     * Retrofit rest api call for fetching the list of advertisements
     * @return list of advertisements
     */
    @GET("api/advertisementsByDate")
    Call<List<Advertisement>> getAdvertisement();

    /**
     * Retrofit rest api call for fetching the list of requested payments
     * @return list of advertisements
     */
    @GET("api/payments")
    Call<List<Payment>> getRequestedPayment(@Query("userId") String userId);


    /**
     * Api to fetch number of pending payment request count
     * @param userId
     * @return
     */
    @GET("api/payment/count")
    Call<NotificationCount> getRequestedPaymentCount(@Query("userId") String userId);

    /**
     * Retrofit rest api call for updating the payment status
     * @return list of advertisements
     */
    @PUT("api/updatePaymentStatus/{paymentReceiptId}")
    @FormUrlEncoded
    Call<Payment> updatePaymentStatus(@Path("paymentReceiptId") String paymentId, @Field("status") String status);

    /**
     * Retrofit rest api call for fetching payment by payment id
     * @return list of advertisements
     */
    @GET("api/paymentStatusById/{paymentReceiptId}")
    Call<Payment> getPaymentStatus(@Path("paymentReceiptId") String paymentId);


}
