package com.mobile.bvault.data.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;
import com.mobile.bvault.model.User;

/**
 * Created by diptif on 17/08/17.
 */

public class AppPreferencesHelper implements PreferencesHelper {

    //    TAGs
    private static final String PREF_NAME = "ixarba_pref";
    private static final String PREF_KEY_USER_ID = "user_id";
    private static final String PREF_KEY_USERNAME = "username";
    private static final String PREF_KEY_EMAIL = "email";
    private static final String PREF_KEY_FIRST_NAME = "first_name";
    private static final String PREF_KEY_LAST_NAME = "last_name";
    private static final String PREF_KEY_DISPLAY_NAME = "display_name";
    private static final String PREF_KEY_PHONE_NUMBER = "phone_number";
    private static final String PREF_KEY_IS_LOGGED_IN = "is_logged_in";
    private static final String PREF_KEY_FAVOURITE_DRINK = "favourite_drink";
    private static final String PREF_KEY_DOB = "dob";
    private static final String PREF_KEY_IMAGE_URL = "image_url";
    private static final String PREF_FIRST_TIME = "first_time";
    private static final String PREF_FACEBOOK_OR_GOOGLE_LOGIN = "facebook_or_google_login";
    private static final String PREF_DEVICE_TOKEN = "device_token";
    private static final String PREF_KEY_LATITUDE = "latitude";
    private static final String PREF_KEY_LONGITUDE = "longitude";
    private static final String PREF_KEY_CURRENT_CITY = "currentcity";
    private static final String PREF_KEY_IS_IPL_FIRST_LAUNCH = "is_ipl_first_launch";
    private static final String OTP_IS = "otpIs";
    private static final String REFERRAL_CODE = "referralCode";
    private static final String IS_CONTACT_SYNCED = "isContactSynced";

    static SharedPreferences mPrefs;
    static AppPreferencesHelper appPreferencesHelper;

    public static AppPreferencesHelper getInstance(Context context) {
        if (appPreferencesHelper == null) {
            appPreferencesHelper = new AppPreferencesHelper();
            if (context != null)
                mPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }
        return appPreferencesHelper;
    }

    @Override
    public void setLatLng(LatLng latLng) {
        if (latLng == null) return;
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_LATITUDE, latLng.latitude + "");
        editor.putString(PREF_KEY_LONGITUDE, latLng.longitude + "");
        editor.commit();
    }

    @Override
    public LatLng getLatLng() {
        LatLng latLng = null;
        double latitude = Double.parseDouble(mPrefs.getString(PREF_KEY_LATITUDE, "0"));
        double longitude = Double.parseDouble(mPrefs.getString(PREF_KEY_LONGITUDE, "0"));
        latLng = new LatLng(latitude, longitude);
        return latLng;
    }

    @Override
    public void setCurrentCity(String currentCity) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_CURRENT_CITY, currentCity);
        editor.commit();
    }

    @Override
    public String getCurrentCity() {
        String currentCity = null;
        if (mPrefs != null) {
            currentCity = mPrefs.getString(PREF_KEY_CURRENT_CITY, "");
        }
        return currentCity;
    }

    @Override
    public void setUser(User user) {
        if (mPrefs != null) {
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(PREF_KEY_USER_ID, user.getUserId());
            editor.putString(PREF_KEY_USERNAME, user.getUsername());
            editor.putString(PREF_KEY_EMAIL, user.getEmail());
            editor.putString(PREF_KEY_DISPLAY_NAME, user.getDisplayName());
            editor.putString(PREF_KEY_FIRST_NAME, user.getFirstName());
            editor.putString(PREF_KEY_LAST_NAME, user.getLastName());
            editor.putString(PREF_KEY_PHONE_NUMBER, user.getPhoneNumber());
            editor.putString(PREF_KEY_DOB, user.getDob());
            editor.putString(PREF_KEY_FAVOURITE_DRINK, user.getFavoriteDrink());
            editor.putString(PREF_KEY_IMAGE_URL, user.getImageUrl());
            editor.putBoolean(PREF_KEY_IS_LOGGED_IN, true);

            editor.commit();
        }
    }

    @Override
    public User getUser() {
        String name = getDisplayName();
        String userEmail = getEmail();
        String number = getPhoneNumber();
        String favourite = getFavouriteDrink();
        String dob = getDOB();
        String imageURl = getImageUrl();
        String deviceToken = getDeviceToken();
        return new User("", userEmail, "", name, "", "", number, dob, favourite, imageURl, deviceToken,"");
    }

    @Override
    public void removeUser() {
        if (mPrefs != null) {
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.remove(PREF_KEY_USER_ID);
            editor.remove(PREF_KEY_USERNAME);
            editor.remove(PREF_KEY_EMAIL);
            editor.remove(PREF_KEY_DISPLAY_NAME);
            editor.remove(PREF_KEY_FIRST_NAME);
            editor.remove(PREF_KEY_LAST_NAME);
            editor.remove(PREF_KEY_PHONE_NUMBER);
            editor.remove(PREF_KEY_IMAGE_URL);
            editor.remove(PREF_KEY_IS_LOGGED_IN);
            editor.remove(PREF_FACEBOOK_OR_GOOGLE_LOGIN);
            editor.commit();
        }
    }

    @Override
    public void setUserName(String username) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_USERNAME, username).commit();
        }
    }

    @Override
    public String getUserName() {
        String username = null;
        if (mPrefs != null) {
            username = mPrefs.getString(PREF_KEY_USERNAME, null);
        }
        return username;
    }

    @Override
    public void setEmail(String email) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_EMAIL, email).commit();
        }
    }

    @Override
    public String getEmail() {
        String email = null;
        if (mPrefs != null) {
            email = mPrefs.getString(PREF_KEY_EMAIL, null);
        }
        return email;
    }

    @Override
    public void setUserId(String userId) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_USER_ID, userId).commit();
        }
    }

    @Override
    public String getUserId() {
        String userId = null;
        if (mPrefs != null) {
            userId = mPrefs.getString(PREF_KEY_USER_ID, null);
        }
        return userId;
    }

    @Override
    public void setDisplayName(String displayName) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_DISPLAY_NAME, displayName).commit();
        }
    }

    @Override
    public String getDisplayName() {
        String displayName = null;
        if (mPrefs != null) {
            displayName = mPrefs.getString(PREF_KEY_DISPLAY_NAME, null);
        }
        return displayName;
    }

    @Override
    public void setFirstName(String firstName) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_FIRST_NAME, firstName).commit();
        }
    }

    @Override
    public String getFirstName() {
        String firstName = null;
        if (mPrefs != null) {
            firstName = mPrefs.getString(PREF_KEY_FIRST_NAME, null);
        }
        return firstName;
    }

    @Override
    public void setLastName(String lastName) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_LAST_NAME, lastName).commit();
        }
    }

    @Override
    public String getLastName() {
        String lastName = null;
        if (mPrefs != null) {
            lastName = mPrefs.getString(PREF_KEY_LAST_NAME, null);
        }
        return lastName;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_PHONE_NUMBER, phoneNumber).commit();
        }
    }

    @Override
    public String getPhoneNumber() {
        String phoneNumber = null;
        if (mPrefs != null) {
            phoneNumber = mPrefs.getString(PREF_KEY_PHONE_NUMBER, null);
        }
        return phoneNumber;
    }

    @Override
    public void setLoggedIn(Boolean isLoggedIn) {
        if (mPrefs != null) {
            mPrefs.edit().putBoolean(PREF_KEY_IS_LOGGED_IN, isLoggedIn).commit();
        }
    }

    @Override
    public boolean isLoggedIn() {
        return mPrefs.getBoolean(PREF_KEY_IS_LOGGED_IN, false);
    }

    @Override
    public void setFavouriteDrink(String favouriteDrink) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_FAVOURITE_DRINK, favouriteDrink).commit();
        }
    }

    @Override
    public String getFavouriteDrink() {
        return mPrefs.getString(PREF_KEY_FAVOURITE_DRINK, null);
    }

    @Override
    public void setDOB(String dob) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_DOB, dob).commit();
        }
    }

    @Override
    public String getDOB() {
        return mPrefs.getString(PREF_KEY_DOB, null);
    }

    @Override
    public void setImageUrl(String imageUrl) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_KEY_IMAGE_URL, imageUrl).commit();
        }
    }

    @Override
    public String getImageUrl() {
        return mPrefs.getString(PREF_KEY_IMAGE_URL, null);
    }

    @Override
    public void setFirstTimeLaunch(boolean isFirstTime) {
        if (mPrefs != null) {
            mPrefs.edit().putBoolean(PREF_FIRST_TIME, isFirstTime).commit();
        }
    }

    @Override
    public boolean isFirstTimeLaunch() {
        return mPrefs.getBoolean(PREF_FIRST_TIME, true);
    }

    @Override
    public void setDeviceToken(String deviceToken) {
        if (mPrefs != null) {
            mPrefs.edit().putString(PREF_DEVICE_TOKEN, deviceToken).commit();
        }
    }

    @Override
    public String getDeviceToken() {
        return mPrefs.getString(PREF_DEVICE_TOKEN, null);
    }

    @Override
    public void setFacebookOrGoogleLogin(boolean isFacebookOrGoogleLogin) {
        if (mPrefs != null) {
            mPrefs.edit().putBoolean(PREF_FACEBOOK_OR_GOOGLE_LOGIN, isFacebookOrGoogleLogin).commit();
        }
    }

    @Override
    public boolean isFacebookOrGoogleLogin() {
        return mPrefs.getBoolean(PREF_FACEBOOK_OR_GOOGLE_LOGIN, false);
    }

    public void setIplFirstLaunch(Boolean isLoggedIn) {
        if (mPrefs != null) {
            mPrefs.edit().putBoolean(PREF_KEY_IS_IPL_FIRST_LAUNCH, isLoggedIn).commit();
        }
    }

    public boolean getIplFirstLaunch() {
        return mPrefs.getBoolean(PREF_KEY_IS_IPL_FIRST_LAUNCH, true);
    }

    public void setOtp(int otp) {
        if (mPrefs != null) {
            mPrefs.edit().putInt(OTP_IS, otp).commit();
        }
    }

    public int getOtp() {
        return mPrefs.getInt(OTP_IS, 0);
    }

    public void setUserReferralCode(String referralCode) {
        if (mPrefs != null) {
            mPrefs.edit().putString(REFERRAL_CODE, referralCode).commit();
        }
    }

    public String getUserReferralCode() {
        return mPrefs.getString(REFERRAL_CODE, "");
    }

    public void setContactSynced(boolean isSynced) {
        if (mPrefs != null) {
            mPrefs.edit().putBoolean(IS_CONTACT_SYNCED, isSynced).commit();
        }
    }

    public boolean isContactSynced() {
        return mPrefs.getBoolean(IS_CONTACT_SYNCED, false);
    }

}
