package com.mobile.bvault.data.network;

import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.DrinkType;
import com.mobile.bvault.model.PayUPaymentResponse;
import com.mobile.bvault.model.PaytmChecksum;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.PurchaseRequest;
import com.mobile.bvault.model.Rack;
import com.mobile.bvault.model.RefPromoCodeModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by diptif on 18/09/17.
 */

public interface BottleApiInterface {

    /**
     * Retrofit api interface for fetching the bottle list from server
     *
     * @return list of bottle
     */
    @GET("api/all-bottles")
    Call<List<Bottle>> getAllBottles();


    /**
     * Retrofit api interface for fetching the bottle list from server
     *
     * @param typeId drink type like Wine, Rum etc.
     * @return list of bottle based on the selected type.
     */
    @GET("api/bottlesByType")
    Call<List<Bottle>> getBottleList(@Query("typeId") String typeId);

    /**
     * Fetches drink type from the server
     *
     * @return
     */
    @GET("api/drinkType")
    Call<List<DrinkType>> getDrinkTypes();

    /**
     * Save the purchase bottles to the server
     *
     * @param purchaseRequest purchased bottle request data
     * @return bottle save response
     */

    @POST("api/rack")
    Call<Void> purchaseBottle(@Body PurchaseRequest purchaseRequest);

    /**
     * Save the pay u payment response
     *
     * @param payUPaymentResponse purchased bottle request data
     * @return bottle save response
     */

    @POST("api/payu_transaction")
    Call<Void> payUResponse(@Body PayUPaymentResponse payUPaymentResponse);

    /**
     * Retrofit api interface for fetching the bottle list from server
     *
     * @return list of bottle based on the selected type.
     */
    @GET("api/rack")
    Call<List<Rack>> getRackList(@Query("userId") String userId);

    /**
     * Retrofit api interface for fetching discounts based on promo code
     *
     * @param promoCode promoCode
     * @param userId    current user id
     * @return promo code object
     */
    @GET("api/promoCode")
    Call<PromoCode> applyPromoCode(@Query("promoCode") String promoCode, @Query("userId") String userId);

    @POST("api/saveRedeemedBottle")
    Call<Void> saveRedeemedBottle(@Body PurchaseRequest purchaseRequest);

    @POST("api/validateReferralPromoCode")
    Call<RefPromoCodeModel> applyRefPromoCode(@Body RefPromoCodeModel refPromoCodeModel);

    @POST("api/usedReferralPromoCode")
    Call<RefPromoCodeModel> usedReferralPromoCode(@Body RefPromoCodeModel refPromoCodeModel);

    @POST("api/generatePaytmChecksum")
    Call<PaytmChecksum> generatePaytmChecksum(@Body PaytmChecksum paytmChecksum);
}
