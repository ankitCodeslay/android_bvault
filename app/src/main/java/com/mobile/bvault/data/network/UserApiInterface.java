package com.mobile.bvault.data.network;

import com.mobile.bvault.aws.AWS;
import com.mobile.bvault.model.AppVersion;
import com.mobile.bvault.model.ForgotPassword;
import com.mobile.bvault.model.History;
import com.mobile.bvault.model.PromoCodeModel;
import com.mobile.bvault.model.ReferralCodeModel;
import com.mobile.bvault.model.UploadContactModel;
import com.mobile.bvault.model.User;
import com.mobile.bvault.model.UserEarningModel;
import com.mobile.bvault.model.UserRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by diptif on 18/09/17.
 */

public interface UserApiInterface {

    /**
     * Login the user
     *
     * @param email    registered email
     * @param password user password
     * @return User object containing user info
     */
    @FormUrlEncoded
    @POST("api/auth/signin")
    Call<User> login(@Field("email") String email, @Field("password") String password);

    /**
     * Retrofit api call for signing up the user
     *
     * @param userRequest object of user info
     * @return User object containing user detail like name, email etc.
     */
    @POST("api/auth/signup")
    Call<User> signupUser(@Body UserRequest userRequest);

    @GET("api/history")
    Call<List<History>> getHistory(@Query("userId") String userId, @Query("pageNumber") int page);

    /**
     * Retrofit api call for forgot password
     *
     * @param username email ID
     * @return message
     */
    @POST("api/auth/forgot")
    @FormUrlEncoded
    Call<ForgotPassword> forgotPassword(@Field("username") String username);

    @PUT("api/users/{userId}")
    @FormUrlEncoded
    Call<User> updateUser(@Path("userId") String userId,
                          @Field("firstName") String firstName,
                          @Field("lastName") String lastName,
                          @Field("email") String email,
                          @Field("username") String username,
                          @Field("phone") String phone,
                          @Field("dateOfBirth") String dob,
                          @Field("favouriteDrinks") String favourite,
                          @Field("city") String city,
                          @Field("referralCode") String referralCode,
                          @Field("isOtherLogin") boolean isOtherLogin);

    @POST("api/users/password")
    @FormUrlEncoded
    Call<User> changePassword(@Field("userId") String userId,
                              @Field("currentPassword") String currentPassword,
                              @Field("newPassword") String newPassword);

    @PUT("api/users/{userId}")
    @FormUrlEncoded
    Call<User> updateUserProfilePic(@Path("userId") String userId,
                                    @Field("imageUrl") String imageName);

    @PUT("api/users/{userId}")
    @FormUrlEncoded
    Call<User> updateUserDeviceToken(@Path("userId") String userId,
                                     @Field("deviceToken") String deviceToken);

    /**
     * Api for contact us
     *
     * @param userId
     * @param message
     * @return
     */
    @POST("api/users/contactus/{userId}")
    @FormUrlEncoded
    Call<User> contactUs(@Path("userId") String userId,
                         @Field("message") String message);


    /**
     * Fetch the aws access key and secret key
     */
    @GET("api/credentials")
    Call<AWS> getAWSAccessKey(@Query("userId") String userId);

    /**
     * Fetch the aws access key and secret key
     */
    @GET("api/appVersion")
    Call<AppVersion> getAppVersionCode();

    @GET()
    Call<String> sendOtpRequest(@Url String url);

    @POST("api/ValidateReferralCode")
    Call<ReferralCodeModel> validateReferralCode(@Body ReferralCodeModel referralCodeModel);

    /**
     * Fetch the user referralCode
     */
    @GET("api/getUserReferralCode")
    Call<User> getUserReferralCode(@Query("userId") String userId);

    /**
     * Fetch the user EarningHistory
     */
    @GET("api/getUserEarningHistory")
    Call<List<PromoCodeModel>> getUserEarningHistory(@Query("userId") String userId);

    @POST("api/uploadContactList")
    Call<Void> uploadContactList(@Body UploadContactModel uploadContactModel);
}
