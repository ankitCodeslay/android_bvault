package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.nfc.Tag;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.Rack;
import com.mobile.bvault.ui.custom_view.BottleMeterView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.TimeUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 21/08/17.
 */

public class RackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Rack> rackList;
    private OnItemClickListener onItemClickListener;

    public RackListAdapter(Context context, List<Rack> rackList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.rackList = rackList;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_rack_list,parent,false);
            return new ViewHolder(view);
        }else {
            return EmptyViewHolder.getEmptyViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder viewHolder = (ViewHolder) holder;
                if (viewHolder != null) {
                    Rack rack = rackList.get(position);
                    Bottle bottle = rack.getBottle();
                    if (rack != null && bottle != null ) {
                        int remainingVolume = rack.getRemainingVolume();
                        int perDegree = (int) (((remainingVolume * 100) / rack.getTotalVolume()) * 3.6);
                        viewHolder.nameTextView.setText(bottle.getName());
                        viewHolder.descTextView.setText(bottle.getDescription());
                        viewHolder.bottleMeterView.setAngle(perDegree);
                        viewHolder.codeTextView.setText(rack.getUniqueCode());
                        viewHolder.volTextView.setText(remainingVolume + " "+bottle.getUnit().toUpperCase());
                        if (rack.getExpiryDate() != null) {
                            CommonUtils.printErrorLog("test","testing"+rack.getExpiryDate());
                            long day = TimeUtils.getDays(rack.getExpiryDate());
                            String days =  day > 1
                                    ? "" + day + " days"
                                    : "" + day + " day";
                            viewHolder.expireTextView.setText("Expires in "+  days);
                            viewHolder.expireTextView.setVisibility(View.VISIBLE);
                        }else {
                            viewHolder.expireTextView.setVisibility(View.GONE);
                        }
                        String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_BOTTLES,bottle.getImageURL());
                        ImageUtil.setImage(context,viewHolder.bottleImageView,imgUrl,R.drawable.bottles);
                        viewHolder.onClick(rack,this.onItemClickListener);
                    }

                }
                break;
            case 1:
                EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
                if (emptyViewHolder != null) {
                    emptyViewHolder.setEmptyImageView(R.drawable.bottles);
                    emptyViewHolder.setMessage(R.string.vault_empty_message);
                    emptyViewHolder.hideButton(true);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (CommonUtils.getCollectionSize(rackList) > 0) return 0;
        else return 1;
    }

    @Override
    public int getItemCount() {
        int size = CommonUtils.getCollectionSize(this.rackList);
        return size > 0 ? size : 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bottle_name) TextView nameTextView;
        @BindView(R.id.expires_date_text_view) TextView expireTextView;
        @BindView(R.id.bottle_description) TextView descTextView;
        @BindView(R.id.bottle_meter) BottleMeterView bottleMeterView;
        @BindView(R.id.vol_text_view) TextView volTextView;
        @BindView(R.id.code_text_view) TextView codeTextView;
        @BindView(R.id.rack_bottle_image_view) ImageView bottleImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void onClick(final Rack rack, final OnItemClickListener onItemClick){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnItemClick(rack);
                }
            });
        }
    }


    public interface OnItemClickListener {

        void OnItemClick(Rack rack);
    }
}
