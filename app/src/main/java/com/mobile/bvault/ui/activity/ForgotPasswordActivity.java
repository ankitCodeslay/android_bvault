package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.ForgotPassword;
import com.mobile.bvault.presenter.ForgotPasswordPresenter;
import com.mobile.bvault.ui.view.ForgotPasswordView;
import com.mobile.bvault.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordView {

    @BindView(R.id.et_username)
    EditText etUsername;
    ForgotPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        presenter = new ForgotPasswordPresenter(this,this);
    }

    @OnClick(R.id.btn_forgot_password)
    void forgetPassword() {
        String userName = etUsername.getText().toString().trim();
        if (userName.isEmpty()) {
            showToastMessage("Please enter username");
        }else {
            if (CommonUtils.isEmailValid(userName)) {
                showLoading();
                presenter.forgotPassword(userName);
//                showToastMessage("Email has been sent successfully");
            }else {
                showToastMessage(getString(R.string.error_invalid_email));
            }
        }
    }

    @OnClick(R.id.sign_in_text_view)
    void gotoLogin() {
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }

    @Override
    public void onSuccess(ForgotPassword forgotPassword) {
        hideLoading();
        finish();
        showToastMessage("Email has been sent successfully");
    }

    @Override
    public void onFailure(String message) {
        hideLoading();
        showToastMessage(message);
    }
}
