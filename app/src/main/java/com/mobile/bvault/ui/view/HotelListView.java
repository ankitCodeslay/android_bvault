package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;
import com.mobile.bvault.model.Hotel;

import java.util.List;

/**
 * Created by diptif on 21/08/17.
 */

public interface HotelListView extends BaseView {

    /**
     * Indicate list of bottle are fetched successfully
     * @param hotelList list of hotels
     */
    void onHotelListSuccessListener(List<Hotel> hotelList);

    /**
     * Indicate error occured while fetching the bottle list
     * @param message error message
     */
    void onHotelListFailureListener(String message);
}
