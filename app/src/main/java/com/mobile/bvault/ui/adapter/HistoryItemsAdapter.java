package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.HistoryItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 11/09/17.
 */

public class HistoryItemsAdapter extends RecyclerView.Adapter<HistoryItemsAdapter.ViewHolder> {

    private List<HistoryItem> historyItems;
    private Context context;

    public HistoryItemsAdapter(Context context, List<HistoryItem> historyItems) {
        this.historyItems = historyItems;
        this.context = context;
    }

    @Override
    public HistoryItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_history_detail,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryItemsAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;//CommonUtils.getCollectionSize(histories);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.history_type_image_view) ImageView historyTypeImageView;
        @BindView(R.id.history_type_text_view) TextView historyTypeTextView;
        @BindView(R.id.history_time_text_view) TextView timeTextView;
        @BindView(R.id.bottle_name) TextView bottleNameTextView;
        @BindView(R.id.purchase_type_image_view) ImageView purchaseTypeImageView;
        @BindView(R.id.purchase_type_text_view) TextView purchaseTypeTextView;
        @BindView(R.id.vol_text_view) TextView volTextView;
        @BindView(R.id.code_text_view) TextView codeTextView;
        @BindView(R.id.history_location_container) LinearLayout locationContainer;
        @BindView(R.id.address_text_view) TextView addressTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
