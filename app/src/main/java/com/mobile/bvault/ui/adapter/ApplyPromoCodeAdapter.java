package com.mobile.bvault.ui.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.mobile.bvault.model.PromoCodeModel;
import com.mobile.bvault.model.User;
import com.mobile.bvault.ui.activity.PromoCodeActivity;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.DateTimeUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApplyPromoCodeAdapter extends RecyclerView.Adapter<ApplyPromoCodeAdapter.ViewHolder> {

    private static final String TAG = "PromoCodeModelAdapter";
    private Context context;
    private List<PromoCodeModel> promoCodeList;

    public ApplyPromoCodeAdapter(Context context, List<PromoCodeModel> promoCodeList) {
        this.context = context;
        this.promoCodeList = promoCodeList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_apply_promo_code, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PromoCodeModel promoCodeModel = promoCodeList.get(position);
        if (promoCodeModel == null) return;
        holder.promoCodeTextView.setText(promoCodeModel.getPromoCode());
        if (!TextUtils.isEmpty(promoCodeModel.getReferType())) {
            String descriptionText = "";
            if (!TextUtils.isEmpty(promoCodeModel.getPromoCodeAmt())) {
                descriptionText = "Use promocode " + promoCodeModel.getPromoCode() + " to get RS" + String.format("%.2f", Double.parseDouble(promoCodeModel.getPromoCodeAmt())) + " discount. This promocode is ";
            }
            if (promoCodeModel.getReferType().equalsIgnoreCase("to")) {
                User user = promoCodeModel.getReferByUser();
                if (user != null) {
                    holder.descTextView.setText(descriptionText + " reffered By " + user.getDisplayName() + ". This code will be valid for only first transaction. " + "\nExpiry on- " + DateTimeUtils.convertDate(promoCodeModel.getExpiryDate().substring(0, 10)));
                }
            } else if (promoCodeModel.getReferType().equalsIgnoreCase("by")) {
                User user = promoCodeModel.getReferToUser();
                if (user != null) {
                    holder.descTextView.setText(descriptionText + " reffered To " + user.getDisplayName() + ".\nExpiry on- " + DateTimeUtils.convertDate(promoCodeModel.getExpiryDate().substring(0, 10)));
                }
            }
        }
        final PromoCodeModel promoCodeModel1 = promoCodeModel;
        holder.promoCodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyPromoCode(promoCodeModel1.getPromoCode());
            }
        });

        holder.applyPromocodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PromoCodeActivity) context).applyPromCode(promoCodeModel1.getPromoCode());
            }
        });
    }


    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(this.promoCodeList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.promocode_text_view)
        TextView promoCodeTextView;
        @BindView(R.id.description_text_view)
        TextView descTextView;
        @BindView(R.id.btn_apply_promocode)
        Button applyPromocodeBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PromoCodeModel PromoCodeModel);
    }

    public void copyPromoCode(String code) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", code);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, "Coppied to Clipboard!!", Toast.LENGTH_SHORT).show();
    }

}
