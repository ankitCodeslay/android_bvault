package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Wishlist;
import com.mobile.bvault.presenter.WishlistPresenter;
import com.mobile.bvault.ui.activity.WishlistActivity;
import com.mobile.bvault.presenter.WishlistAdapterPresenter;
import com.mobile.bvault.ui.view.WishlistAdapterView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 28/08/17.
 */

public class WishlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements WishlistAdapterView {

    private static final String TAG = "WishlistAdapter";

    private Context context;
    private List<Wishlist> wishlists;
    private WishlistAdapterPresenter wishlistAdapterPresenter;
    private WishlistAdapterListener wishlistAdapterListener;
    private WishlistPresenter wishlistPresenter;

    public WishlistAdapter(Context context, List<Wishlist> wishlists, WishlistAdapterListener wishlistAdapterListener, WishlistPresenter wishlistPresenter) {
        this.context = context;
        this.wishlists = wishlists;
        wishlistAdapterPresenter = new WishlistAdapterPresenter(this.context,this);
        this.wishlistAdapterListener = wishlistAdapterListener;
        this.wishlistPresenter  = wishlistPresenter;
        setTotalAmount();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_wishlist, parent, false);
            return new ViewHolder(view);
        } else {
            return EmptyViewHolder.getEmptyViewHolder(parent, viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (CommonUtils.getCollectionSize(wishlists) > 0)
            return 0;
        else
            return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder viewHolder = (ViewHolder) holder;
                if (viewHolder != null) {
                    Wishlist wishlist = this.wishlists.get(position);
                    Picasso.with(context).load(wishlist.getImageURL()).placeholder(R.drawable.bottles).into(viewHolder.bottleImage);
                    viewHolder.bottleName.setText(wishlist.getName());
                    viewHolder.bottleDesc.setText(wishlist.getDescription());
                    viewHolder.priceTextView.setText("\u20B9 "+CommonUtils.commaFormatter(wishlist.getPrice()));
                    CommonUtils.printErrorLog(TAG,""+wishlist.getQty());
                    viewHolder.wishlistQTY.setText(""+String.format("%02d", wishlist.getQty()));
                    viewHolder.mlTextView.setText(""+wishlist.getVolume() + " " +wishlist.getUnit());
                    viewHolder.typeTextView.setText(wishlist.getType().toUpperCase() + " BOND");
                    String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_BOTTLES,wishlist.getImageURL());
                    ImageUtil.setImage(context,viewHolder.bottleImage,imgUrl,R.drawable.bottles);
                    addQuantityAndPrice(viewHolder.wishlistQTY,viewHolder.addTextView,viewHolder.priceTextView,position);
                    minusQuantity(viewHolder.wishlistQTY,viewHolder.minusTextView,viewHolder.priceTextView,position);
                    removeItemClick(viewHolder.deleteImageView,wishlist.getWishlistId(),position);
                    viewHolder.onClick(wishlist,wishlistAdapterListener);
                }
                break;
            case 1:
                EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
                if (emptyViewHolder != null) {
                    emptyViewHolder.setEmptyImageView(R.drawable.ic_shopping_cart_black_24dp);
                    emptyViewHolder.setMessage(R.string.bucket_empty_message);
                    emptyViewHolder.onClick(context);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        int size = CommonUtils.getCollectionSize(this.wishlists);
        return size > 0 ? size : 1;
    }

    public void removeItemClick(View view, final int wishlistId, final int position) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItemDialog(wishlistId,position);
            }
        });
    }

    public void deleteItemDialog(final int wishlistId,final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to delete this item?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                wishlistAdapterPresenter.removeItemFromWishlist(wishlistId,position);
            }
        });

        builder.show();
    }

    public void addQuantityAndPrice(final TextView view, final TextView addTextView, final TextView priceTextView, final int position) {
        addTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.printErrorLog(TAG,"Added Quantity");
                int qty = Integer.parseInt(view.getText().toString());
                Wishlist wishlist = wishlists.get(position);
                int price = wishlist.getPrice();
                int newQty = qty + 1;
                int calPrice = (price / qty) * newQty;
                int calculatedMl = wishlist.getVolume() * newQty;
                CommonUtils.printErrorLog(TAG,"Calculated New Price: "+calPrice);
                CommonUtils.printErrorLog(TAG,"Calculated New Price: "+calPrice);
                CommonUtils.printErrorLog(TAG, "Calculated Ml: "+ calculatedMl);
                CommonUtils.printErrorLog(TAG,"Quantity: "+newQty);

                wishlistAdapterPresenter.updateQtyPriceAndVolume(newQty,calPrice,wishlist.getWishlistId(),view,priceTextView,position,calculatedMl);
            }
        });
    }

    public void minusQuantity(final TextView view, final TextView minusTextView,final TextView priceTextView,final int position) {
        minusTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.printErrorLog(TAG,"Minus Quantity");
                Wishlist wishlist = wishlists.get(position);
                int qty = Integer.parseInt(view.getText().toString());
                if (qty >= 2) {
                    int price = wishlist.getPrice();
                    int newQty = qty - 1;
                    int calPrice = (price / qty) * newQty;
                    int calculatedMl = wishlist.getVolume() * newQty;
                    wishlistAdapterPresenter.updateQtyPriceAndVolume(newQty,calPrice,wishlist.getWishlistId(),view,priceTextView,position, calculatedMl);
                }

            }
        });
    }

    @Override
    public void updateQuantityCallback(int result, TextView textView,final TextView priceTextView,int qty, int price, int position) {
        if (result > 0) {
            textView.setText(""+ String.format("%02d", qty));
            priceTextView.setText("\u20B9 "+price);
            wishlists.get(position).setPrice(price);
            setTotalAmount();
        }else {
            ((WishlistActivity) context).showToastMessage("Something went wrong, Please try again.");
        }
    }

    @Override
    public void removeItemCallback(int result, int position) {
        if (result > 0) {
            wishlists.remove(position);
            notifyDataSetChanged();
//            notifyItemRemoved(position);
            ((WishlistActivity) context).updateWishlistCount();
            setTotalAmount();
        }else {
            ((WishlistActivity) context).showToastMessage("Something went wrong, Please try again.");
        }
    }

    @Override
    public void setTotalAmount() {
        if (wishlistPresenter != null)
            wishlistPresenter.getTotalAmount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.wishlist_bottle_image) ImageView bottleImage;
        @BindView(R.id.bottle_name) TextView bottleName;
        @BindView(R.id.bottle_description) TextView bottleDesc;
        @BindView(R.id.price_text_view) TextView priceTextView;
        @BindView(R.id.wishlist_ml_text_view) TextView mlTextView;
        @BindView(R.id.wishlist_add_text_view) TextView addTextView;
        @BindView(R.id.wishlist_quantity) TextView wishlistQTY;
        @BindView(R.id.wishlist_minus_btn) TextView minusTextView;
        @BindView(R.id.wishlist_type_label) TextView typeTextView;
        @BindView(R.id.wishlist_delete_img_view) ImageView deleteImageView;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void onClick(final Wishlist wishlist, final WishlistAdapterListener wishlistAdapterListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    wishlistAdapterListener.onItemClick(wishlist);
                }
            });
        }
    }

    public interface WishlistAdapterListener{

        void onItemClick(Wishlist wishlist);
    }


}
