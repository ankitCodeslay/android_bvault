package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;

/**
 * Created by diptif on 06/10/17.
 */

public interface ChangePasswordView extends BaseView {

    void onPasswordChangedSuccessful();

    void onPasswordChangeFailure(String message);
}
