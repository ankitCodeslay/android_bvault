package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.Payment;

import java.util.List;

/**
 * Created by diptif on 25/10/17.
 */

public interface NotificationView {

    void onPaymentRequestLoadedSuccessfully(List<Payment> payments);

    void onPaymentRequestFailure(String message);

}
