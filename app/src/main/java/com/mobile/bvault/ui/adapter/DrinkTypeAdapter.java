package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.DrinkType;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 18/08/17.
 */

public class DrinkTypeAdapter extends RecyclerView.Adapter<DrinkTypeAdapter.ViewHolder> {

    private static final String TAG = "DrinkTypeAdapter";
    private Context context;
    private List<DrinkType> typeList;
    private OnItemClickListener onItemClickListener;

    public DrinkTypeAdapter(Context context, List<DrinkType> typeList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
        this.typeList = typeList;
//        setTypeList();
    }

    private void setTypeList() {
//        typeList = new ArrayList<>();
//        typeList.add("Rum");
//        typeList.add("Whiskey");
//        typeList.add("Vodka");
//        typeList.add("Wine");
//        typeList.add("Gin");
//        typeList.add("Beer");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_drink_type, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String drinkType = typeList.get(position).getType();
        String bigText = drinkType.toUpperCase().charAt(0) + "";
        holder.bigTextView.setText(bigText);
        holder.titleTextView.setText("" + drinkType);

        if (drinkType != null && drinkType.equalsIgnoreCase("Combos")) {
            holder.bigTextView.setTextColor(ContextCompat.getColor(context, R.color.combo_text_color));
            holder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.combo_text_color));
        } else if (drinkType != null && drinkType.equalsIgnoreCase("Shakes")) {
            holder.bigTextView.setTextColor(ContextCompat.getColor(context, R.color.shakes_text_color));
            holder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.shakes_text_color));
        } else {
            holder.bigTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }

        holder.click(typeList.get(position), onItemClickListener);
    }


    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(this.typeList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.drink_type_container)
        RelativeLayout mainContainer;
        @BindView(R.id.big_letter_text_view)
        TextView bigTextView;
        @BindView(R.id.drink_type_text_view)
        TextView titleTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            CommonUtils.setFont(bigTextView, context);
        }

        public void click(final DrinkType drinkType, final OnItemClickListener itemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG, drinkType.getType());
                    itemClickListener.onItemClick(drinkType);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DrinkType drinkType);
    }
}
