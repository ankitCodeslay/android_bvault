package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by diptif on 21/08/17.
 */

public class AvailableAtAdapter extends RecyclerView.Adapter<AvailableAtAdapter.ViewHolder> {

    private static final String TAG = "AvailableAtAdapter";

    private Context context;
    private List<Hotel> hotelList;
    private OnItemClickListener onItemClickListener;

    public AvailableAtAdapter(Context context, List<Hotel> hotelList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.hotelList = hotelList;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public AvailableAtAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_hotel_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AvailableAtAdapter.ViewHolder holder, int position) {
        Hotel hotel = hotelList.get(position);
        holder.nameTextView.setText(hotel.getName());
        holder.addressTextView.setText(hotel.getAddress());
        holder.onClick(hotelList.get(position),onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(this.hotelList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.hotel_name) TextView nameTextView;
        @BindView(R.id.hotel_address) TextView addressTextView;
        @BindView(R.id.hotel_list_image) CircleImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            setBackgroundColor();
        }

        public void setBackgroundColor() {
            itemView.setBackground(ContextCompat.getDrawable(context,R.drawable.list_selector));
        }

        public void onClick(final Hotel hotel, final OnItemClickListener onItemClick){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnItemClick(hotel);
                }
            });
        }
    }

    public interface OnItemClickListener {

        void OnItemClick(Hotel hotel);
    }
}
