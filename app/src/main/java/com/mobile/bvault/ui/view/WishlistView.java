package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.Wishlist;

import java.util.List;

/**
 * Created by diptif on 28/08/17.
 */

public interface WishlistView {

    /**
     * Pass list of order from wishlist presenter to wishlist activity
     * @param wishlists
     */
    void loadWishlist(List<Wishlist> wishlists);

    /**
     * Proceed to purchase
     */
    void proceedPurchase();

    /**
     * Sets the total amount on amount text filed
     * @param totalAmount
     */
    void setTotalAmount(double totalAmount);
}
