package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;

/**
 * Created by diptif on 24/08/17.
 */

public interface HotelDetailView extends BaseView {

    /**
     * Sets Hotel name.
     *
     * @param title hotel name
     */
    void setHotelName(String title);

    /**
     * Sets hotel description.
     *
     * @param desc hotel description
     */
    void setHotelAddress(String desc);

    /**
     * Sets hotel image.
     *
     * @param urls
     */
    void setHotelImage(String[] urls);

    /**
     * Sets hotel phone number.
     *
     * @param number
     */
    void setPhoneNumber(String number);


    /**
     * Sets distance.
     *
     * @param dist distance from hotel
     */
    void setDistance(float dist);

    /**
     * Sets the hotel title, hotel desc etc.
     */
    void setHotelDetails();

    /**
     * Sets the marker on map
     *
     * @param lat latitude
     * @param lng longitude
     */
    void setMarker(double lat, double lng);

    /**
     * Sets the music
     *
     * @param music type of music
     */
    void setMusic(String music);

    /**
     * Sets cuisine
     *
     * @param cuisine
     */
    void setCuisine(String cuisine);

    /**
     * Sets cost
     *
     * @param cost
     */
    void setCost(double cost);

    /**
     * Sets moreInfo
     *
     * @param moreInfo
     */
    void setMoreInfo(String moreInfo);
}
