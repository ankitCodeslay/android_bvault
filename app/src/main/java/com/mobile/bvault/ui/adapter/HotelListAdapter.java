package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by diptif on 21/08/17.
 */

public class HotelListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "BottleListAdapter";

    private Context context;
    private List<Hotel> hotelList;
    private OnItemClickListener onItemClickListener;

    public HotelListAdapter(Context context, List<Hotel> hotelList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.hotelList = hotelList;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_hotel_list,parent,false);
            return new ViewHolder(view);
        }else {
            return EmptyViewHolder.getEmptyViewHolder(parent, viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (CommonUtils.getCollectionSize(hotelList) > 0)
            return 0;
        else
            return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder viewHolder = (ViewHolder) holder;
                if (viewHolder != null) {
                    Hotel hotel = hotelList.get(position);
                    viewHolder.nameTextView.setText(hotel.getName());
                    viewHolder.addressTextView.setText(hotel.getAddress());
                    String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_HOTELS,hotel.getImageURL().length >= 1 ? hotel.getImageURL()[0] : "");
                    CommonUtils.printErrorLog(TAG,""+imgUrl);
                    ImageUtil.setImage(context,viewHolder.imageView,imgUrl,R.drawable.bottles);
                    CommonUtils.printErrorLog(TAG,""+hotel.getDistance());
                    if (hotel.getDistance() == 0.0) {
                        viewHolder.distanceTextView.setVisibility(View.GONE);
                    }else {
                        viewHolder.distanceTextView.setText(""+String.format("%.2f", hotel.getDistance()) + " KM");
                        viewHolder.distanceTextView.setVisibility(View.VISIBLE);
                    }

                    viewHolder.onClick(hotelList.get(position),onItemClickListener);
                }
                break;
            case 1:
                EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
                if (emptyViewHolder != null) {
                    emptyViewHolder.setEmptyImageView(R.drawable.bottles);
                    emptyViewHolder.setMessage(R.string.hangout_empty_message);
                    emptyViewHolder.onClick(context);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        int size = CommonUtils.getCollectionSize(this.hotelList);
        return size > 0 ? size : 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.hotel_name) TextView nameTextView;
        @BindView(R.id.hotel_address) TextView addressTextView;
        @BindView(R.id.hotel_list_image) CircleImageView imageView;
        @BindView(R.id.distance_text_view) TextView distanceTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            setBackgroundColor();
        }

        public void setBackgroundColor() {
            itemView.setBackground(ContextCompat.getDrawable(context,R.drawable.list_selector));
        }

        public void onClick(final Hotel hotel, final OnItemClickListener onItemClick){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnItemClick(hotel);
                }
            });
        }
    }



    public interface OnItemClickListener {

        void OnItemClick(Hotel hotel);
    }
}
