package com.mobile.bvault.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.presenter.ChangePasswordPresenter;
import com.mobile.bvault.presenter.ContactUsPresenter;
import com.mobile.bvault.presenter.ProfilePresenter;
import com.mobile.bvault.presenter.SignOutPresenter;
import com.mobile.bvault.ui.view.ContactUsView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by diptif on 01/11/17.
 */

public class ContactUsActivity extends BaseActivity implements ContactUsView{

    @BindView(R.id.et_message) EditText messageTextView;

    ContactUsPresenter contactUsPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.contact_us));
        initDrawer();
        contactUsPresenter = new ContactUsPresenter(this,this);
    }

    @OnClick(R.id.btn_send)
    void sendMessage() {
        String message = messageTextView.getText().toString().trim();
        if (message.isEmpty()) {
            showToastMessage("Please enter message");
        }else {
            if (contactUsPresenter != null) {
                showLoading();
                contactUsPresenter.contactUs(message);
            }
        }
    }

    @Override
    public void onContactUsSuccess(String message) {
        hideLoading();
        showToastMessage(message);
    }

    @Override
    public void onContactUsFailure(String message) {
        hideLoading();
        showToastMessage(message);
    }
}
