package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchasedMessageActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchased_message);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.checkout));
        initDrawer();
    }

    @OnClick(R.id.browse_dring_button)
    void goToDrinkScreen() {
        startActivity(new Intent(this,RackListActivity.class));
        finish();
    }


}
