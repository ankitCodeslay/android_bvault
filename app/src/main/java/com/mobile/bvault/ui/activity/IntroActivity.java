package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.ui.adapter.IntroAdapter;
import com.mobile.bvault.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by diptif on 16/10/17.
 */

public class IntroActivity extends BaseActivity implements ViewPager.OnPageChangeListener{

    private static final String TAG = "IntroActivity";

    @BindView(R.id.ftux_view_pager) ViewPager viewPager;
    @BindView(R.id.dot_container) LinearLayout dotContainer;
    @BindView(R.id.next_button) Button nextButton;
    @BindView(R.id.skip_button) Button skipButton;

    int[] ftuxLayout = {R.layout.ftux_1,R.layout.ftux_2,R.layout.ftux_3};
    TextView[] dots;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        initViewPager();
        addBottomDots(0);
    }

    private void initViewPager() {
        viewPager.setAdapter(new IntroAdapter(this,ftuxLayout));
        viewPager.addOnPageChangeListener(this);
    }

    @OnClick(R.id.next_button)
    void nextPage() {
        CommonUtils.printErrorLog(TAG,""+viewPager.getCurrentItem());
        if (viewPager.getCurrentItem() == (ftuxLayout.length - 1)) {
            launchLoginScreen();
        }else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
    }

    @OnClick(R.id.skip_button)
    void skip() {
        launchLoginScreen();
    }

    private void launchLoginScreen() {
        AppPreferencesHelper.getInstance(this).setFirstTimeLaunch(false);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[ftuxLayout.length];

        dotContainer.removeAllViews();

        for (int i = 0; i < ftuxLayout.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText("\u2022");
            dots[i].setTextSize(35);
            dots[i].setTextColor(ContextCompat.getColor(this,R.color.black_effective));
            dotContainer.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(ContextCompat.getColor(this,R.color.white));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        addBottomDots(position);

        if (position == (ftuxLayout.length - 1)) {
            skipButton.setVisibility(View.INVISIBLE);
            nextButton.setText(R.string.done);
        }else {
            skipButton.setVisibility(View.VISIBLE);
            nextButton.setText(R.string.next);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
