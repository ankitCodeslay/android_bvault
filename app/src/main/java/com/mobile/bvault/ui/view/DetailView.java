package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;
import com.mobile.bvault.model.Price;

import java.util.List;

/**
 * Created by diptif on 24/08/17.
 */

public interface DetailView extends BaseView {

    /**
     * Sets bottle title.
     * @param title bottle name
     */
    void setBottleTitle(String title);

    /**
     * Sets bottle description.
     * @param desc bottle description
     */
    void setBottleDescription(String desc);

    /**
     * Sets bottle save text.
     * @param saveText bottle save text
     */
    void setBottleSaveText(String saveText);

    /**
     * Sets bottle image.
     * @param url
     */
    void setBottleImage(String url);

    /**
     * Sets the price on price recycler view
     * @param priceList prices of volume
     * @param unit unit of measure
     */
    void setPrice(List<Price> priceList, String unit);

    void addToCart();

    /**
     * Sets the bottle title, bottle desc etc.
     */
    void setBottleDetails();

    /**
     * Indicates bottle is added to wishlist or not
     * @param isAdded if added returns true otherwise false.
     */
    void isAddedToCart(boolean isAdded);

    /**
     * Sets bottleId
     * @param bottleId
     */
    void setBottleId(String bottleId);

}
