package com.mobile.bvault.ui.view;

/**
 * Created by diptif on 31/08/17.
 */

public interface PurchaseView {

    void onPurchaseSuccess();

    void onPurchaseFailure(String message);
}
