package com.mobile.bvault.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.model.Price;
import com.mobile.bvault.presenter.DetailPresenter;
import com.mobile.bvault.presenter.HotelListPresenter;
import com.mobile.bvault.ui.adapter.PriceAdapter;
import com.mobile.bvault.ui.adapter.ViewPagerAdapter;
import com.mobile.bvault.ui.view.DetailView;
import com.mobile.bvault.ui.view.HotelListView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.LocationUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BottleDetailActivity extends BaseActivity implements DetailView, HotelListView, LocationUtils.OnLocationListener,PriceAdapter.OnItemClickListener, ViewPagerAdapter.OnCardClickListener{

    private static final String TAG = "BottleDetailActivity";

    @BindView(R.id.detail_image_View) ImageView bottleImageView;
    @BindView(R.id.bottle_detail_title_text_view) TextView titleTextView;
    @BindView(R.id.bottle_detail_desc_text_View) TextView descTextView;
    @BindView(R.id.bottle_detail_save_text_View) TextView saveTextView;
    @BindView(R.id.add_to_cart_button) Button addToCartButton;
    @BindView(R.id.hotel_view_pager) ViewPager viewPager;
    @BindView(R.id.price_recycler_view) RecyclerView priceRecyclerView;
    @BindView(R.id.hotel_container) LinearLayout hotelContainer;

    DetailPresenter detailPresenter;

//   use to check bottle is added or not.
    boolean isAdded;
    private Price selectedPrice = null;

    private ViewPagerAdapter viewPagerAdapter;
    private LocationUtils locationUtils;
    private HotelListPresenter hotelListPresenter;
    int viewPagerPadding = 50;
    int pageMargin = 10;

    String bottleId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.heading_bottle_detail));
        initDrawer();
        init();

    }

    private void init() {

        detailPresenter =  new DetailPresenter(this,this);
        locationUtils = new LocationUtils(this,this);
        hotelListPresenter = new HotelListPresenter(this,this);

        priceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        viewPager.setOffscreenPageLimit(3);
        viewPager.setClipToPadding(false);
        viewPagerPadding = getResources().getInteger(R.integer.available_at_padding);
        viewPager.setPadding(viewPagerPadding, 0, viewPagerPadding, 0);
        viewPager.setPageMargin(pageMargin);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        priceRecyclerView.setNestedScrollingEnabled(true);
        setBottleDetails();
        getCurrentLocation();
    }

    private void getCurrentLocation() {
        locationUtils.getCurrentLocation();
    }

    @Override
    public void setBottleTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void setBottleDescription(String desc) {
        descTextView.setText(desc);
    }

    @Override
    public void setBottleSaveText(String saveText) {
        saveTextView.setText(saveText);
    }

    @Override
    public void setBottleImage(String url) {
        String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_BOTTLES,url);
        ImageUtil.setImage(this,bottleImageView,imgUrl,R.drawable.bottles);
    }

    @Override
    public void setPrice(List<Price> priceList, String unit) {
        if (priceList != null && priceList.size() > 0) {
            this.selectedPrice = priceList.get(0);
            priceRecyclerView.setAdapter(new PriceAdapter(this, priceList, unit, selectedPrice, this));
        }
    }


    @OnClick(R.id.add_to_cart_button)
    @Override
    public void addToCart() {
        CommonUtils.printErrorLog(TAG,"Pressed add to cart button");
        if (isAdded) {
            showWishlist();
        }else {
            if (selectedPrice != null && detailPresenter != null) {
                    detailPresenter.addToCart(selectedPrice);
            }else {
                showSnackBar("Please select the price");
            }
        }
    }

    @Override
    public void setBottleDetails() {
        detailPresenter.setBottleDetail();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (detailPresenter != null)
            detailPresenter.isAddedToCart(selectedPrice);
    }

    @Override
    public void isAddedToCart(boolean isAdded) {
        this.isAdded = isAdded;
        if (isAdded) {
            this.addToCartButton.setText(getString(R.string.go_to_wishlish));
        }else {
            this.addToCartButton.setText(getString(R.string.add_to_yout_cart));
        }
    }

    @Override
    public void setBottleId(String bottleId) {
        this.bottleId = bottleId;
    }

//    @Override
//    public void nextHotel() {
//        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
//    }
//
//    @Override
//    public void previousHotel() {
//        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
//    }

    @Override
    public void onLocationSuccess(Location location) {
        hotelListPresenter.getHotelsByBottleId(location,bottleId);
    }

    @Override
    public void onLocationFailure() {
        hotelListPresenter.getHotelsByBottleId(null,bottleId);
    }

    @Override
    public void onHotelListSuccessListener(List<Hotel> hotelList) {
        CommonUtils.printErrorLog(TAG,"Hotel fetched successfully " + hotelList.size());
        if (hotelList != null && hotelList.size() > 0) {
            viewPagerAdapter = new ViewPagerAdapter(this,viewPager,hotelList,this);
            viewPager.setAdapter(viewPagerAdapter);
            hotelContainer.setVisibility(View.VISIBLE);
        }else {
            hotelContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onHotelListFailureListener(String message) {
        hotelContainer.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.LOCATION_REQUEST_CODE) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                hotelListPresenter.getHotelsByBottleId(null,bottleId);
            }else {
                getCurrentLocation();
            }
        }
    }

    @Override
    public void OnItemClick(Price price) {
        selectedPrice = price;
        detailPresenter.isAddedToCart(selectedPrice);
    }

    @Override
    public void onClick(Hotel hotel) {
        CommonUtils.printErrorLog(TAG,"Hotel selected from the hotels list");
        Intent intent = new Intent(this, HotelDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.HOTEL,hotel);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
