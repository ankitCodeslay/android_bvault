package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.HomeOption;

import java.util.List;

/**
 * Created by diptif on 11/09/17.
 */

public interface HomeView {

    void setupRecyclerView();

    void setHomeOptions(List<HomeOption> homeOptions);
}
