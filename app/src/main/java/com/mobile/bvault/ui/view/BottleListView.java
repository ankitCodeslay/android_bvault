package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;
import com.mobile.bvault.model.Bottle;

import java.util.List;

/**
 * Created by diptif on 21/08/17.
 */

public interface BottleListView extends BaseView {

    /**
     * Indicate list of bottle are fetched successfully
     * @param bottleList list of hotels
     */
    void onSuccess(List<Bottle> bottleList);

    /**
     * Indicate error occured while fetching the bottle list
     * @param message error message
     */
    void onFailure(String message);
}
