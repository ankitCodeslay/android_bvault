package com.mobile.bvault.ui.activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.presenter.LoginPresenter;
import com.mobile.bvault.ui.view.HomeView;
import com.mobile.bvault.ui.view.LoginView;
import com.mobile.bvault.ui.view.SignupView;
import com.mobile.bvault.utils.CommonUtils;
import com.onesignal.OneSignal;
import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.presenter.SignupPresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements LoginView,SignupView,GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LoginActivity";

    // UI references.
    @BindView(R.id.et_username) EditText mEmailView;
    @BindView(R.id.et_password) EditText mPasswordView;
    @BindView(R.id.sign_up_text_view) TextView signUpTextView;
//    @BindView(R.id.sign_in_button) SignInButton signInButton;

    private LoginPresenter loginPresenter;
    CallbackManager callbackManager;
    SignupPresenter signupPresenter;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    boolean isFacebookOrGoogleLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setUserName();
        setupFacebookLogin();
        setupGoogleSignIn();

        loginPresenter = new LoginPresenter(this,this);
        signupPresenter = new SignupPresenter(this,this);
    }

    @OnClick(R.id.btn_sign_in)
    void login(){
        loginPresenter.validate(
                mEmailView.getText().toString().trim(),
                mPasswordView.getText().toString().trim()
        );
    }

    @OnClick(R.id.btn_sign_in_with_facebook)
    void loginWithFacebook() {
        CommonUtils.printErrorLog(TAG,"facebook login button clicked");
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
    }

    @OnClick(R.id.sign_in_with_google)
    void loginWithGoogle() {
        CommonUtils.printErrorLog(TAG,"google login button clicked");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onLoginSuccess(User user) {
        updateDeviceToken();
    }

    void goToHomePage() {
        if (CommonUtils.hasMobAndDob(this)) {
            startActivity(new Intent(this, HomeActivity.class));
        }else {
            startActivity(new Intent(this, ProfileActivity.class));
        }
        finish();
    }

    @Override
    public void onLoginFailure(String message) {
        hideLoading();
        CommonUtils.printErrorLog(TAG,message);
        showSnackBar(message);
    }

    @Override
    public void singUP() {

    }

    @Override
    public void goToSignIn() {

    }

    @Override
    public void onSignupSuccess(User user) {
        isFacebookOrGoogleLogin = true;
        updateDeviceToken();
    }

    @Override
    public void onSignupFailure(String message) {
        hideLoading();
        isFacebookOrGoogleLogin = false;
        showToastMessage(message);
    }

    @Override
    public void onValidationFail(String message) {
        showSnackBar(message);
    }

    @Override
    public void onValidationSuccess() {
        showLoading();
        loginPresenter.loginAPICall(
                mEmailView.getText().toString().trim(),
                mPasswordView.getText().toString().trim()
        );
    }

    @Override
    public void onRefCodeValidationFail(String message) {

    }

    @Override
    public void onRefCodeValidationSuccess() {

    }

    @Override
    public void setUserName() {
        String userName = AppPreferencesHelper.getInstance(this).getEmail();
        mEmailView.setText(userName != null ? userName : "");
    }

    @OnClick(R.id.sign_up_text_view)
    @Override
    public void goToSignUP() {
        startActivity(new Intent(this, SignupActivity.class));
    }

    @Override
    public void onDeviceTokenUpdatedSuccessfully(String deviceToken) {
        hideLoading();
        CommonUtils.printErrorLog(TAG,"Login Success");
        showToastMessage("login successfully");
        goToHomePage();
    }

    @Override
    public void onDeviceTokenUpdatedFailure(String message) {
        hideLoading();
        goToHomePage();
    }

    @OnClick(R.id.forgot_password)
    void forgotPassword() {
        startActivity(new Intent(this,ForgotPasswordActivity.class));
    }

    void setupGoogleSignIn() {

//        signInButton.setSize(SignInButton.SIZE_STANDARD);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    void setupFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                CommonUtils.printErrorLog("tag","FF fb onSuccess");
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            String email = "";
                            String name = "";

                            if (object != null && object.has("email"))
                                email =  object.getString("email");

                            if (object != null && object.has("name"))
                                name =  object.getString("name");

                            saveUser(email,name);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,birthday,picture,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                CommonUtils.printErrorLog("tag","fb onCancel");
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                CommonUtils.printErrorLog("tag","fb onError: "+exception.getMessage());
                showToastMessage("Something went wrong, Please try again");
                // App code
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        CommonUtils.printErrorLog(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            saveUser(acct.getEmail(),acct.getDisplayName());
        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showToastMessage(connectionResult.getErrorMessage());
    }

    private void saveUser(String email, String name) {
        showLoading();
        CommonUtils.printErrorLog(TAG,"Name: "+name);
        CommonUtils.printErrorLog(TAG,"Email: "+email);
        String[] nameArray = CommonUtils.getFirstNameAndLastName(name);
        String username = email.split("@")[0];
        signupPresenter.signup(nameArray[0],nameArray[1],email,username,"","","",true,"");
    }

    private void updateDeviceToken() {
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                CommonUtils.printErrorLog(TAG,"USERID: "+userId);
                CommonUtils.printErrorLog(TAG,"REGISTRATIONID"+registrationId);
                loginPresenter.updateDeviceToken(userId);
            }
        });
    }

}

