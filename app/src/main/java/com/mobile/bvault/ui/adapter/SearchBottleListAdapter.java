package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 21/08/17.
 */

public class SearchBottleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "BottleListAdapter";

    private Context context;
    private List<Bottle> bottleList;
    private OnItemClickListener onItemClickListener;

    public SearchBottleListAdapter(Context context, List<Bottle> bottleList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.bottleList = bottleList;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_bottle_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolder viewHolder = (ViewHolder) holder;
        if (viewHolder != null) {
            if (bottleList != null && bottleList.size() > position) {
                Bottle bottle = bottleList.get(position);
                viewHolder.nameTextView.setText(bottle.getName());
                viewHolder.descTextView.setText(bottle.getDescription());
                viewHolder.priceTextView.setText("\u20B9 " + CommonUtils.commaFormatter(CommonUtils.getMinimumPrice(bottle.getPrices())));
                CommonUtils.printErrorLog(TAG, "volume: " + bottle.getVolume());
                String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_BOTTLES, bottle.getImageURL());
                ImageUtil.setImage(context, viewHolder.imageView, imgUrl, R.drawable.bottles);
                viewHolder.onClick(bottleList.get(position), onItemClickListener);
            }
        }
    }

    @Override
    public int getItemCount() {
        int size = CommonUtils.getCollectionSize(this.bottleList);
        return size > 0 ? size : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bottle_name)
        TextView nameTextView;
        @BindView(R.id.bottle_description)
        TextView descTextView;
        @BindView(R.id.bottle_list_image)
        ImageView imageView;
        @BindView(R.id.price_text_view)
        TextView priceTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setBackgroundColor();
        }

        public void setBackgroundColor() {
            itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.list_selector));
        }

        public void onClick(final Bottle bottle, final OnItemClickListener onItemClick) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnItemClick(bottle);
                }
            });
        }
    }


    public interface OnItemClickListener {

        void OnItemClick(Bottle bottle);
    }
}
