package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.HomeOption;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 11/09/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private List<HomeOption> homeOptions;
    private Context context;
    private ItemClickListener itemClickListener;

    public HomeAdapter(List<HomeOption> homeOptions, Context context, ItemClickListener itemClickListener) {
        this.homeOptions = homeOptions;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_home_options, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {
        HomeOption homeOption = this.homeOptions.get(position);
        holder.textView.setText(homeOption.getTitle());

        holder.homeImageView.setImageResource(homeOption.getImage());
        holder.onClick(homeOption, itemClickListener);

        if (position == 3) {
            holder.homeImageView.setVisibility(View.GONE);
            holder.imageViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_play_fifa));
        } else {
            holder.homeImageView.setVisibility(View.VISIBLE);
            holder.imageViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.bottle_list_img_outline));
        }
    }

    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(homeOptions);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view_container)
        FrameLayout imageViewContainer;
        @BindView(R.id.home_option_text_view)
        TextView textView;
        @BindView(R.id.descTV)
        TextView descTV;
        @BindView(R.id.home_option_image)
        ImageView homeImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setBackgroundColor();
            CommonUtils.setFont(textView, context);
        }

        public void setBackgroundColor() {
            itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.list_selector));
        }

        public void onClick(final HomeOption homeOption, final ItemClickListener itemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(homeOption);
                }
            });
        }
    }

    public interface ItemClickListener {

        void onItemClick(HomeOption homeOption);
    }
}
