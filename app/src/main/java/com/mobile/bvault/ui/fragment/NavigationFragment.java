package com.mobile.bvault.ui.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.bvault.BuildConfig;
import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.NavOption;
import com.mobile.bvault.model.NotificationCount;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.presenter.NavPresenter;
import com.mobile.bvault.presenter.NotificationCountPresenter;
import com.mobile.bvault.presenter.NotificationPresenter;
import com.mobile.bvault.ui.activity.ProfileActivity;
import com.mobile.bvault.ui.adapter.NavAdapter;
import com.mobile.bvault.ui.view.DrawerInterface;
import com.mobile.bvault.ui.view.NavView;
import com.mobile.bvault.ui.view.NotificationCountView;
import com.mobile.bvault.ui.view.NotificationView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationFragment extends Fragment implements NavView, NavAdapter.ItemClickListener{

    private static final String TAG = "NavigationFragment";

    @BindView(R.id.user_name_text_view) TextView userNameTextView;
    @BindView(R.id.user_container) LinearLayout userNameContainer;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.user_logo) ImageView imageView;

    DrawerInterface drawerInterface;
    NotificationCountPresenter notificationCountPresenter;
    NavPresenter navPresenter;
    NavAdapter navAdapter;


    public NavigationFragment() {
        // Required empty public constructor
    }

    public void setDrawerInterface(DrawerInterface drawerInterface) {
        this.drawerInterface = drawerInterface;
    }

    @OnClick(R.id.user_container)
    void goToProfile() {
        this.drawerInterface.closeDrawer();
        startActivity(new Intent(getActivity(), ProfileActivity.class));
    }

    public void updateDrawerInfo() {

        String userLogo = AppPreferencesHelper.getInstance(getActivity()).getImageUrl();
        CommonUtils.printErrorLog(TAG,""+userLogo);
        if (userLogo != null && !userLogo.isEmpty()) {

            String url = AWSUtil.generateAmazonImageURL(Constants.FOLDER_PROFILE_PICTURE,userLogo);
            CommonUtils.printErrorLog(TAG,"Profile Image url: "+ url);
            if (imageView != null) {
                ImageUtil.setImage(getActivity(),imageView,url,R.drawable.ic_person_gray_24dp);
            }
        }

        String userName = AppPreferencesHelper.getInstance(getActivity()).getDisplayName();
        if (userName != null) {
            userNameTextView.setText("Hello,\n" + userName);
        }

//        if (notificationCountPresenter != null) {
//            notificationCountPresenter.getRequestedPaymentCount();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        updateDrawerInfo();
    }

    private void initView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        navAdapter = new NavAdapter(getActivity(),this);
        recyclerView.setAdapter(navAdapter);
        navPresenter = new NavPresenter(getActivity(),this);
        navPresenter.setOptions();
//        notificationCountPresenter = new NotificationCountPresenter(getActivity(),this);
    }

    @Override
    public void setOptions(List<NavOption> navOptionList) {
        if (navAdapter != null) {
            navAdapter.setNavOptions(navOptionList);
        }
    }

    @Override
    public void onItemClick(NavOption navOption) {
        if (navOption != null)
            if (navOption.getActivityName() != null) {
                drawerInterface.closeDrawer();
                startActivity(new Intent(getActivity(), navOption.getActivityName()));
            }else {
                if (navOption.getTitle().equalsIgnoreCase(getActivity().getString(R.string.terms_condition))) {
                    CommonUtils.showTermsAndCondition(getActivity());
                    drawerInterface.closeDrawer();
                }
            }
    }

//    @Override
//    public void onNotificationCountLoadedSuccessfully(NotificationCount notificationCount) {
//        if (notificationCount != null && navAdapter != null) {
//            CommonUtils.printErrorLog(TAG,""+notificationCount.getCount());
//            navAdapter.setCount(notificationCount.getCount());
//        }
//    }

//    @Override
//    public void onNotificationCountFailure(String message) {
//        CommonUtils.showToastMessage(message,getActivity());
//    }
}
