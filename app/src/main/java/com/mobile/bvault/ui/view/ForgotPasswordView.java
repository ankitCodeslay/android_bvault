package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.ForgotPassword;

/**
 * Created by diptif on 28/09/17.
 */

public interface ForgotPasswordView {

    void onSuccess(ForgotPassword forgotPassword);

    void onFailure(String message);

}
