package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.IPLMatchModel;
import com.mobile.bvault.ui.activity.HomeActivity;
import com.mobile.bvault.ui.activity.IPLMatchDetalsActivity;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.ImageUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mobile.bvault.utils.Constants.IPL_IMAGE_URL;

public class IplMatchAdapter extends RecyclerView.Adapter<IplMatchAdapter.ViewHolder> {

    private static final String TAG = "PromoCodeModelAdapter";
    private Context context;
    private List<IPLMatchModel> iplMatchList;

    public IplMatchAdapter(Context context, List<IPLMatchModel> iplMatchList) {
        this.context = context;
        this.iplMatchList = iplMatchList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ipl_match, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        IPLMatchModel iplMatchModel = iplMatchList.get(position);
        if (iplMatchModel == null) return;
        holder.titleTV.setText("Match-" + ++position);
        holder.timeTV.setText(convertTime(iplMatchModel.getMatchStartTime()).toUpperCase());
        holder.dateTV.setText(convertDate(iplMatchModel.getMatchStartDate().substring(0, 10)));
        holder.team1TV.setText(iplMatchModel.getTeamOne());
        holder.team2TV.setText(iplMatchModel.getTeamTwo());

        String imageUrl1 = IPL_IMAGE_URL + iplMatchModel.getTeamOneImageUrl();
        String imageUrl2 = IPL_IMAGE_URL + iplMatchModel.getTeamTwoImageUrl();
        ImageUtil.setImage(context, holder.team1IV, imageUrl1, R.drawable.ipl_banner);
        ImageUtil.setImage(context, holder.team2IV, imageUrl2, R.drawable.ipl_banner);

        final IPLMatchModel iplMatchModel1 = iplMatchModel;
        holder.iplContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("iplMatchModel", iplMatchModel1);
                Intent intent = new Intent(context, IPLMatchDetalsActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(this.iplMatchList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleTV)
        TextView titleTV;
        @BindView(R.id.timeTV)
        TextView timeTV;
        @BindView(R.id.dateTV)
        TextView dateTV;
        @BindView(R.id.team1TV)
        TextView team1TV;
        @BindView(R.id.team2TV)
        TextView team2TV;
        @BindView(R.id.team1IV)
        ImageView team1IV;
        @BindView(R.id.team2IV)
        ImageView team2IV;
        @BindView(R.id.ipl_container)
        LinearLayout iplContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static String convertDate(String date) {

        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;

        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf1 = new SimpleDateFormat("dd MMMM yyyy");
            result = sdf1.format(sdf.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

    public static String convertTime(String time) {

        String result = "";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            final Date dateObj = sdf.parse(time);
            result = new SimpleDateFormat("hh:mm aa").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

}
