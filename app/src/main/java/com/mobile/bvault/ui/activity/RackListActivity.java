package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.Rack;
import com.mobile.bvault.ui.adapter.RackListAdapter;
import com.mobile.bvault.presenter.RackListPresenter;
import com.mobile.bvault.ui.view.RackListView;
import com.mobile.bvault.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RackListActivity extends BaseActivity implements RackListView, RackListAdapter.OnItemClickListener {

    private static final String TAG = "RackListActivity";

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rack_list);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.my_rack));
        initDrawer();
        setRecyclerView();
        getBottleList();
    }

    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.toolbar_left_layer)
    void showDrawer() {
        openDrawer();
    }

    private void getBottleList() {
        showLoading();
        new RackListPresenter(this,this).getBottleList();
    }

    @Override
    public void onSuccess(List<Rack> rackList) {
        hideLoading();
        recyclerView.setAdapter(new RackListAdapter(this,rackList,this));
    }

    @Override
    public void onFailure(String message) {
        hideLoading();
        showSnackBar(message);
    }

    @OnClick(R.id.browse_dring_button)
    @Override
    public void browseDrinkCategory() {
        startActivity(new Intent(this, DrinkTypeActivity.class));
    }

    @Override
    public void OnItemClick(Rack rack) {
//        startActivity(new Intent(this, HotelListActivity.class));
        Intent intent = new Intent(this, HotelListActivity.class);
        intent.putExtra(Constants.BOTTLE,rack.getBottle().getBottleId());
        startActivity(intent);
    }
}
