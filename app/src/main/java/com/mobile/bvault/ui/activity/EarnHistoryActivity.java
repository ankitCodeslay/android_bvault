package com.mobile.bvault.ui.activity;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.PromoCodeModel;
import com.mobile.bvault.ui.adapter.ReferAndEarnPagerAdapter;
import com.mobile.bvault.ui.fragment.UnusedPromocodeFragment;
import com.mobile.bvault.ui.fragment.UsedPromocodeFragment;
import com.mobile.bvault.utils.DateTimeUtils;
import com.mobile.bvault.utils.NetworkUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 01/11/17.
 */

public class EarnHistoryActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.tabLayout)
    public TabLayout tabLayout;
    @BindView(R.id.viewPager)
    public ViewPager viewPager;

    private ReferAndEarnPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn_history);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.title_earning_history));
        initDrawer();
        setTabLayout();
        getUserEarningHistory();
    }

    private void setTabLayout() {

        tabLayout.addTab(tabLayout.newTab().setText("Unused"));
        tabLayout.addTab(tabLayout.newTab().setText("Used"));
        //Creating our pager adapter
        adapter = new ReferAndEarnPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);

        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.gradient_start_color));
            drawable.setSize(3, 1);
            ((LinearLayout) root).setDividerPadding(12);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void getUserEarningHistory() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        showLoading();
        String userId = AppPreferencesHelper.getInstance(EarnHistoryActivity.this).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<List<PromoCodeModel>> apiCall = userApiInterface.getUserEarningHistory(userId);
        apiCall.enqueue(new Callback<List<PromoCodeModel>>() {
            @Override
            public void onResponse(Call<List<PromoCodeModel>> call, Response<List<PromoCodeModel>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    List<PromoCodeModel> promoCodeModelList = response.body();
                    updatePromoCodeList(promoCodeModelList);
                }
            }

            @Override
            public void onFailure(Call<List<PromoCodeModel>> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void updatePromoCodeList(List<PromoCodeModel> promoCodeModelList) {

        List<PromoCodeModel> unUsedPromoCodeList = new ArrayList<>();
        List<PromoCodeModel> usedPromoCodeList = new ArrayList<>();
        if (promoCodeModelList != null) {
            for (int i = 0; i < promoCodeModelList.size(); i++) {
                PromoCodeModel promoCodeModel = promoCodeModelList.get(i);
                if (promoCodeModel == null) continue;

                String expiryDate = promoCodeModel.getExpiryDate();
                if (!TextUtils.isEmpty(expiryDate)) {
                    expiryDate = expiryDate.substring(0, 10);
                }
                boolean isExpired = isPromoCodeExpired(expiryDate);
                if (promoCodeModel.getUsedStatus() == 0 && !isExpired) {
                    unUsedPromoCodeList.add(promoCodeModel);
                } else {
                    usedPromoCodeList.add(promoCodeModel);
                }
            }
        }
        UnusedPromocodeFragment fragment1 = (UnusedPromocodeFragment) adapter.getFragment(0);
        if (fragment1 != null) {
            fragment1.updatePromoCodeList(unUsedPromoCodeList);
        }
        UsedPromocodeFragment fragment2 = (UsedPromocodeFragment) adapter.getFragment(1);
        if (fragment2 != null) {
            fragment2.updatePromoCodeList(usedPromoCodeList);
        }
    }

    private boolean isPromoCodeExpired(String expiryDate) {
        boolean isExpired = false;
        if (TextUtils.isEmpty(expiryDate)) return isExpired;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentTime = null;
        try {
            String dateTimeStr = expiryDate;
            Date endTime = dateFormat.parse(dateTimeStr);
            currentTime = dateFormat.parse(dateFormat.format(new Date()));

            if (currentTime.after(endTime)) {
                isExpired = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isExpired;
    }
}
