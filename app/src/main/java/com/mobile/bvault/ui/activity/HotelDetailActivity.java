package com.mobile.bvault.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.presenter.HotelDetailPresenter;
import com.mobile.bvault.ui.view.HotelDetailView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.PermissionUtil;
import com.synnapps.carouselview.CarouselView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HotelDetailActivity extends BaseActivity implements HotelDetailView {

    private static final String TAG = "HotelDetailActivity";

    //    @BindView(R.id.detail_image_View) ImageView hotelImageView;
    @BindView(R.id.address_container)
    RelativeLayout addressImageView;
    @BindView(R.id.phone_number_container)
    RelativeLayout phoneNumberContainer;
    @BindView(R.id.hotel_name_text_view)
    TextView titleTextView;
    @BindView(R.id.hotel_desc_text_View)
    TextView addressTextView;
    @BindView(R.id.distance_text_view)
    TextView distanceTextView;
    @BindView(R.id.distance_container)
    LinearLayout distanceContainer;
    @BindView(R.id.text_phone_number)
    TextView phoneNumberTextView;
    @BindView(R.id.music_text_view)
    TextView musicTextView;
    @BindView(R.id.music_container)
    LinearLayout musicContainer;
    @BindView(R.id.cuisine_container)
    LinearLayout cuisineContainer;
    @BindView(R.id.cuisine_text_view)
    TextView cuisineTextView;

    @BindView(R.id.cost_container)
    LinearLayout costContainer;
    @BindView(R.id.cost_text_view)
    TextView costTextView;

    @BindView(R.id.more_info_container)
    LinearLayout moreInfoContainer;
    @BindView(R.id.more_info_text_view)
    TextView moreInfoTextView;

    @BindView(R.id.carouselView)
    CarouselView carouselView;

    @BindView(R.id.combo_button)
    Button comboButton;


    HotelDetailPresenter hotelDetailPresenter;

    //   use to check bottle is added or not.
    boolean isAdded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(R.string.restaurant);
        initDrawer();
        setHotelDetails();

        if (TextUtils.isEmpty(hotelDetailPresenter.hotel.getMenuDescription())) {
            comboButton.setVisibility(View.INVISIBLE);
        } else {
            comboButton.setVisibility(View.VISIBLE);
        }
        comboButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showComboMenuDialog();
            }
        });
    }

    @Override
    public void setHotelName(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void setHotelAddress(String desc) {
        addressTextView.setText(desc);
    }

    @Override
    public void setHotelImage(String[] urls) {
//        String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_HOTELS,url);
//        ImageUtil.setImage(this,hotelImageView,imgUrl,R.drawable.bottles);
        if (hotelDetailPresenter != null) {
            hotelDetailPresenter.setHotelImages(this.carouselView, urls);
        }
    }

    @Override
    public void setPhoneNumber(String number) {
        if (number != null)
            phoneNumberTextView.setText(number);
    }

    @Override
    public void setDistance(float dist) {
        if (dist == 0.0) {
            distanceContainer.setVisibility(View.GONE);
        } else {
            distanceTextView.setText("" + String.format("%.2f", dist) + " KM");
            distanceContainer.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void setHotelDetails() {
        hotelDetailPresenter = new HotelDetailPresenter(this, this);
        hotelDetailPresenter.setHotelDetail();
    }

    @Override
    public void setMarker(final double lat, final double lng) {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                new MapPresenter(HotelDetailActivity.this).setLocation(lat, lng);
//            }
//        },1000);
    }

    @Override
    public void setMusic(String music) {
        if (music != null && !music.isEmpty()) {
            musicTextView.setText(music);
            musicContainer.setVisibility(View.VISIBLE);
        } else {
            musicContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void setCuisine(String cuisine) {
        if (cuisine != null && !cuisine.isEmpty()) {
            cuisineTextView.setText(cuisine);
            cuisineContainer.setVisibility(View.VISIBLE);
        } else {
            cuisineContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void setCost(double cost) {
        if (cost > 0) {
            costTextView.setText("Approx cost for 2 is \u20B9 " + cost);
            costContainer.setVisibility(View.VISIBLE);
        } else {
            costContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void setMoreInfo(String moreInfo) {
        if (!TextUtils.isEmpty(moreInfo)) {
            moreInfoTextView.setText(moreInfo);
            moreInfoContainer.setVisibility(View.VISIBLE);
        } else {
            moreInfoContainer.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.phone_number_container)
    void doCall() {
        call();
    }

    @OnClick(R.id.address_container)
    void showOnGoogleMap() {
        hotelDetailPresenter.showMap();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CALL_CODE) {
            if (PermissionUtil.hasPermissionsGranted(this, PermissionUtil.CALL_PERMISSION)) {
                if (hotelDetailPresenter != null) {
                    CommonUtils.printErrorLog(TAG, "permission granted");
                    call();
                }
            }
        }
    }

    private void call() {
        hotelDetailPresenter.makePhoneCall();
    }

    private void showComboMenuDialog() {

        if (hotelDetailPresenter.hotel == null) return;
        View customView = this.getLayoutInflater().inflate(R.layout.combo_menu_dialog, null);
        final Dialog dialog = new Dialog(this, R.style.CustomDialog);
        dialog.setContentView(customView);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.8);
        dialog.getWindow().setLayout(width, height);

        TextView menuTV = (TextView) customView.findViewById(R.id.menuTV);
        Button closeButton = (Button) customView.findViewById(R.id.close_button);
        if (TextUtils.isEmpty(hotelDetailPresenter.hotel.getMenuDescription())) {
            menuTV.setText("");
        } else {
            menuTV.setText(hotelDetailPresenter.hotel.getMenuDescription());
        }
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
