package com.mobile.bvault.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.History;
import com.mobile.bvault.presenter.HistoryPresenter;
import com.mobile.bvault.ui.adapter.HistoryAdapter;
import com.mobile.bvault.ui.view.HistoryView;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends BaseActivity implements HistoryView {

    private static final String TAG = "HistoryActivity";

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    public static final int PAGE_SIZE = 15;
    private boolean isLastPage = false;
    private int currentPage = 1;
    private boolean isLoading = false;

    LinearLayoutManager layoutManager;
    HistoryAdapter historyAdapter;
    HistoryPresenter historyPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        setSupportToolbar();
        setTitle(R.string.history);
        initDrawer();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        initView();
        initPresenter();
    }

    void initView() {
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(onScrollListener);
        historyAdapter = new HistoryAdapter(this);
        recyclerView.setAdapter(historyAdapter);
    }

    void initPresenter(){
        historyPresenter = new HistoryPresenter(this,this);
        loadHistory();
    }

    void loadHistory() {
        showLoading();
        historyPresenter.getHistory(currentPage);
    }

    @Override
    public void onHistoryLoadedSuccessfully(List<History> historyList) {
        hideLoading();
        historyAdapter.addHistories(historyList);
        isLoading = false;
        if (historyList == null || (historyList != null && historyList.size() < PAGE_SIZE)) {
            isLastPage = true;
            CommonUtils.printErrorLog(TAG,"This is last page");
        }else {
            isLastPage = false;
            CommonUtils.printErrorLog(TAG,"This is not last page");
        }
    }

    @Override
    public void onFailure(String message) {
        hideLoading();
        showToastMessage(message);
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    CommonUtils.printErrorLog(TAG,"load more");
                    isLoading = true;
                    currentPage = currentPage + 1;
                    loadHistory();
                }
            }
        }
    };
}
