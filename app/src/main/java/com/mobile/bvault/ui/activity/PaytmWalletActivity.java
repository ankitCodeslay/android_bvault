package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.PaytmChecksum;
import com.mobile.bvault.utils.NetworkUtils;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.payu.india.Payu.PayuConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mobile.bvault.utils.Constants.TRANSACTION_ID;

/**
 * This is the sample app which will make use of the PG SDK. This activity will
 * show the usage of Paytm PG SDK API's.
 **/

public class PaytmWalletActivity extends BaseActivity {

    String txnid = "";
    String amount;
    String email;
    String phone;
    String userId;
    String checkSumHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm_wallet);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initViews();
    }

    protected void initViews() {
        userId = AppPreferencesHelper.getInstance(PaytmWalletActivity.this).getUserId();
        txnid = String.valueOf(System.currentTimeMillis());
        try {
            Double totalAmount = getIntent().getDoubleExtra("totalAmount", 0);
            amount = totalAmount + "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        email = AppPreferencesHelper.getInstance(PaytmWalletActivity.this).getUser().getEmail();
        if (TextUtils.isEmpty(email)) {
            email = "xyz@gmail.com";
        }
        phone = AppPreferencesHelper.getInstance(PaytmWalletActivity.this).getUser().getPhoneNumber();
        generateCheckum();
    }

    //This is to refresh the order id: Only for the Sample App's purpose.
    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public void generateCheckum() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        PaytmChecksum checksum = new PaytmChecksum();
        checksum.setCUST_ID(userId);
        checksum.setORDER_ID(txnid);
        checksum.setTXN_AMOUNT(amount);
        checksum.setEMAIL(email);
        checksum.setMOBILE_NO(phone);
        checksum.setMERC_UNQ_REF("bottlesData_12345");

        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<PaytmChecksum> apiCall = bottleApiInterface.generatePaytmChecksum(checksum);
        apiCall.enqueue(new Callback<PaytmChecksum>() {
            @Override
            public void onResponse(Call<PaytmChecksum> call, Response<PaytmChecksum> response) {
                if (response.isSuccessful()) {
                    PaytmChecksum paytmChecksum = response.body();
                    if (paytmChecksum != null) {
                        checkSumHash = paytmChecksum.getCHECKSUMHASH();
                        onStartTransaction();
                    }
                }
            }

            @Override
            public void onFailure(Call<PaytmChecksum> call, Throwable t) {

            }
        });
    }

    public void onStartTransaction() {
        PaytmPGService Service = PaytmPGService.getProductionService();

        Map<String, String> paramMap = new HashMap<String, String>();

        paramMap.put("MID", "DreamT32160771625239");
        paramMap.put("ORDER_ID", txnid);
        paramMap.put("CUST_ID", userId);
        paramMap.put("INDUSTRY_TYPE_ID", "Retail109");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", amount);
        paramMap.put("WEBSITE", "APPPROD");
        paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + txnid);
        paramMap.put("EMAIL", email);
        paramMap.put("MOBILE_NO", phone);
        paramMap.put("CHECKSUMHASH", checkSumHash);
        paramMap.put("MERC_UNQ_REF", "bottlesData_12345");

        Log.d("LOG", "PaymentTransaction : checkSumHash = " + checkSumHash);
        PaytmOrder Order = new PaytmOrder(paramMap);

        Service.initialize(Order, null);

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {

                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.d("LOG", "PaymentTransaction : " + inErrorMessage);
                    }

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        Log.d("LOG", "PaymentTransaction : " + inResponse);
                        //Toast.makeText(getBaseContext(), "PaymentTransaction " + inResponse, Toast.LENGTH_LONG).show();

                        JSONObject json = new JSONObject();
                        Set<String> keys = inResponse.keySet();
                        for (String key : keys) {
                            try {
                                // json.put(key, bundle.get(key)); see edit below
                                json.put(key, JSONObject.wrap(inResponse.get(key)));
                            } catch (JSONException e) {
                                //Handle exception here
                            }
                        }

                        Intent intent = new Intent();
                        intent.putExtra(PayuConstants.PAYU_RESPONSE, json.toString());
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                    @Override
                    public void networkNotAvailable() {
                        // If network is not
                        // available, then this
                        // method gets called.
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        // This method gets called if client authentication
                        // failed. // Failure may be due to following reasons //
                        // 1. Server error or downtime. // 2. Server unable to
                        // generate checksum or checksum response is not in
                        // proper format. // 3. Server failed to authenticate
                        // that client. That is value of payt_STATUS is 2. //
                        // Error Message describes the reason for failure.
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {

                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        finish();
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                    }

                });
    }
}
