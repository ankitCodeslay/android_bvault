package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;

import java.util.List;

/**
 * Created by diptif on 19/09/17.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private static final String TAG = "ViewPagerAdapter";

    private Context context;
    private List<Hotel> hotelList;
    private LayoutInflater layoutInflater;
    private OnCardClickListener onClickListener;
    private ViewPager viewPager;

    public ViewPagerAdapter(Context context, ViewPager viewPager,List<Hotel> hotelList, OnCardClickListener onClickListener) {
        this.context = context;
        this.viewPager = viewPager;
        this.hotelList = hotelList;
        this.onClickListener = onClickListener;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return CommonUtils.getCollectionSize(hotelList);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final Hotel hotel = hotelList.get(position);
        CommonUtils.printErrorLog(TAG,"item instantiate");
        View view = layoutInflater.inflate(R.layout.view_available_at,container,false);
        TextView hotelNameTextView = (TextView) view.findViewById(R.id.hotel_name);
        hotelNameTextView.setText(hotel.getName());
        TextView address = (TextView) view.findViewById(R.id.hotel_address);
        address.setText(hotel.getAddress());
        ImageView imageView = (ImageView) view.findViewById(R.id.hotel_list_image);
        String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_HOTELS,hotel.getImageURL().length >= 1 ? hotel.getImageURL()[0] : "");
        ImageUtil.setImage(context,imageView,imgUrl,R.drawable.bottles);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(hotel);
            }
        });

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public interface OnCardClickListener {
        void onClick(Hotel hotel);
    }
}
