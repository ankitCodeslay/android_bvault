package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.BottleListModel;
import com.mobile.bvault.ui.adapter.SearchBottleListAdapter;
import com.mobile.bvault.ui.view.BottleListView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchBottleActivity extends BaseActivity implements BottleListView, SearchBottleListAdapter.OnItemClickListener {

    private static final String TAG = "RackListActivity";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.editSearch)
    EditText editSearch;

    List<Bottle> bottleList;
    List<Bottle> searchedBottleList;
    SearchBottleListAdapter bottleListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_bottle);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle("Search Drinks");
        initDrawer();
        setRecyclerView();
        getBottleList();
        initSearchListner();
    }

    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.toolbar_left_layer)
    void showDrawer() {
        openDrawer();
    }

    private String getDrinkType() {
        return getIntent().getStringExtra(Constants.DRINK_TYPE);
    }

    private String getDrinkTypeId() {
        return getIntent().getStringExtra(Constants.DRINK_TYPE_ID);
    }

    private void getBottleList() {
        searchedBottleList = new ArrayList<>();
        bottleList = BottleListModel.getInstance().getBottleList();
        if (bottleList == null) {
            getAllBottles();
        } else if (bottleList != null && bottleList.isEmpty()) {
            getAllBottles();
        }
        bottleListAdapter = new SearchBottleListAdapter(this, searchedBottleList, this);
        recyclerView.setAdapter(bottleListAdapter);
    }

    @Override
    public void onSuccess(List<Bottle> bottleList) {
        hideLoading();
    }

    @Override
    public void onFailure(String message) {
        hideLoading();
    }

    @Override
    public void OnItemClick(Bottle bottle) {
        CommonUtils.printErrorLog(TAG, "Bottle selected from the bottle list");
        Intent intent = new Intent(this, BottleDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BOTTLE, bottle);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void initSearchListner() {

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (TextUtils.isEmpty(editSearch.getText().toString())) {
                    searchedBottleList.clear();
                    bottleListAdapter.notifyDataSetChanged();
                    return;
                }
                List<Bottle> bottles = getSearchedList(editSearch.getText().toString());
                searchedBottleList.clear();
                searchedBottleList.addAll(bottles);
                bottleListAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private List<Bottle> getSearchedList(String searchedString) {
        List<Bottle> bottles = new ArrayList<>();
        if (bottleList == null || bottleList.isEmpty()) {
            return bottles;
        }
        for (int i = 0; i < bottleList.size(); i++) {
            Bottle bottle = bottleList.get(i);
            if (bottle == null) continue;
            String title = bottle.getName();

            if (!TextUtils.isEmpty(title) && title.toLowerCase().contains(searchedString.toLowerCase())) {
                bottles.add(bottle);
            }
        }
        return bottles;
    }

    public void getAllBottles() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        showLoading();
        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<List<Bottle>> apiCall = bottleApiInterface.getAllBottles();
        apiCall.enqueue(new Callback<List<Bottle>>() {
            @Override
            public void onResponse(Call<List<Bottle>> call, Response<List<Bottle>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    bottleList = response.body();
                    BottleListModel.getInstance().setBottleList(bottleList);
                }
            }

            @Override
            public void onFailure(Call<List<Bottle>> call, Throwable t) {
                hideLoading();
            }
        });
    }

}
