package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.presenter.BottleListPresenter;
import com.mobile.bvault.ui.adapter.BottleListAdapter;
import com.mobile.bvault.ui.view.BottleListView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BottleListActivity extends BaseActivity implements BottleListView, BottleListAdapter.OnItemClickListener {

    private static final String TAG = "RackListActivity";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottle_list);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getDrinkType() + " Catalogue");
        initDrawer();
        setRecyclerView();
        getBottleList();
    }

    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.toolbar_left_layer)
    void showDrawer() {
        openDrawer();
    }

    private String getDrinkType() {
        return getIntent().getStringExtra(Constants.DRINK_TYPE);
    }

    private String getDrinkTypeId() {
        return getIntent().getStringExtra(Constants.DRINK_TYPE_ID);
    }

    private void getBottleList() {
        showLoading();
        new BottleListPresenter(this, this).getBottleList(getDrinkTypeId());
    }

    @Override
    public void onSuccess(List<Bottle> bottleList) {
        hideLoading();
        recyclerView.setAdapter(new BottleListAdapter(this, bottleList, this));
    }

    @Override
    public void onFailure(String message) {
        hideLoading();
        showSnackBar(message);
    }

    @Override
    public void OnItemClick(Bottle bottle) {
        CommonUtils.printErrorLog(TAG, "Bottle selected from the bottle list");
        Intent intent = new Intent(this, BottleDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BOTTLE, bottle);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
