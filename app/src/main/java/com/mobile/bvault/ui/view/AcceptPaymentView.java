package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;
import com.mobile.bvault.model.Payment;

/**
 * Created by diptif on 06/10/17.
 */

public interface AcceptPaymentView {

    void onPaymentStatusChangeSuccessful(String message);

    void onPaymentStatusChangeFailure(String message);

    void onPaymentByIdSuccess(Payment payment);

    void onPaymentByIdFailure(String message);
}
