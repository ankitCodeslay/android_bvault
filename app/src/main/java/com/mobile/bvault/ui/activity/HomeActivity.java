package com.mobile.bvault.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.maps.model.LatLng;
import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.Advertisement;
import com.mobile.bvault.model.AppVersion;
import com.mobile.bvault.model.ContactModel;
import com.mobile.bvault.model.HomeOption;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.model.UploadContactModel;
import com.mobile.bvault.model.User;
import com.mobile.bvault.presenter.AcceptPaymentPresenter;
import com.mobile.bvault.presenter.AdsPresenter;
import com.mobile.bvault.presenter.HomePresenter;
import com.mobile.bvault.ui.adapter.HomeAdapter;
import com.mobile.bvault.ui.view.AcceptPaymentView;
import com.mobile.bvault.ui.view.AdsView;
import com.mobile.bvault.ui.view.HomeView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;
import com.synnapps.carouselview.CarouselView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mobile.bvault.utils.Constants.CONTACT_REQUEST_CODE;
import static com.mobile.bvault.utils.Constants.LOCATION_REQUEST_CODE;

public class HomeActivity extends BaseActivity implements HomeView, HomeAdapter.ItemClickListener, AdsView, AcceptPaymentView {


    @BindView(R.id.carouselView)
    CarouselView carouselView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.ads_loading_container)
    LinearLayout adsLoadingContainer;

    ImageView toolbarSearch;

    HomePresenter homePresenter = null;
    AdsPresenter adsPresenter = null;

    AcceptPaymentPresenter acceptPaymentPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(R.string.app_name);
        setupRecyclerView();
        initDrawer();
        initPresenter();
        initSearchClick();
        getAppVersionCode();
        getRequestedPayment();
        getUserReferralCode();
        if (checkReadContactPermission()) {
            new FetchContactTask().execute();
        } else {
            requestReadContactPermission();
        }
    }

    private void getRequestedPayment() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String paymentId = bundle.getString(Constants.PAYMENT_RECEIPT_ID);
            if (paymentId != null && !paymentId.isEmpty()) {
                if (paymentId.equalsIgnoreCase("false")) {
                    return;
                }
                acceptPaymentPresenter = new AcceptPaymentPresenter(this, this);
                acceptPaymentPresenter.getPaymentRequestByID(paymentId);
            }
        }
    }

    void initPresenter() {
        homePresenter = new HomePresenter(this, this);
        homePresenter.setHomeOptions();

        adsLoadingContainer.setVisibility(View.VISIBLE);
        adsPresenter = new AdsPresenter(this, this);
        adsPresenter.getAds();

        GetLocationTask task = new GetLocationTask();
        task.execute(AppPreferencesHelper.getInstance(this).getLatLng());
    }

    @Override
    public void onItemClick(HomeOption homeOption) {
        if (homeOption != null) {
            boolean isIplFirstLaunch = AppPreferencesHelper.getInstance(this).getIplFirstLaunch();
            Class targetClass = homeOption.getActivityName();

            if (targetClass == HowToPlayActivity.class && !isIplFirstLaunch) {
                targetClass = IplMatchActivity.class;
            }

            if (homeOption.getActivityName() != null) {
                startActivity(new Intent(this, targetClass));
            }
        }
    }

    @Override
    public void onAdsLoadedSuccessfully(List<Advertisement> advertisementList) {
        adsLoadingContainer.setVisibility(View.GONE);
        if (adsPresenter != null) adsPresenter.setAds(this.carouselView, advertisementList);
    }

    @Override
    public void onAdsFailure(String message) {
        adsLoadingContainer.setVisibility(View.GONE);
        showSnackBar(message);
    }

    @Override
    public void setupRecyclerView() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void setHomeOptions(List<HomeOption> homeOptions) {
        recyclerView.setAdapter(new HomeAdapter(homeOptions, this, this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    public void onPaymentStatusChangeSuccessful(String message) {
        acceptPaymentPresenter.hideDialog();
        showToastMessage(message);
    }

    @Override
    public void onPaymentStatusChangeFailure(String message) {
        acceptPaymentPresenter.hideDialog();
        showToastMessage(message);
    }

    @Override
    public void onPaymentByIdSuccess(Payment payment) {
        acceptPaymentPresenter = new AcceptPaymentPresenter(this, this, payment);
        acceptPaymentPresenter.showDialog();
    }

    @Override
    public void onPaymentByIdFailure(String message) {
        //showToastMessage(message);
    }

    private boolean checkReadContactPermission() {
        if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void requestReadContactPermission() {
        ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, CONTACT_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CONTACT_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                new FetchContactTask().execute();
            }
        }
    }

    private class GetLocationTask extends AsyncTask<LatLng, Void, List<Address>> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<Address> doInBackground(LatLng... locationName) {
            LatLng requestedLatLong = locationName[0];
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(HomeActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(locationName[0].latitude, locationName[0].longitude, 1);
                // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            try {
                if (addresses == null) return;
                String addressLine1 = addresses.get(0).getAddressLine(0);
                //String locality = addresses.get(0).getSubLocality();
                String addressCity = addresses.get(0).getLocality();
                if (addressCity == null) return;
                AppPreferencesHelper.getInstance(HomeActivity.this).setCurrentCity(addressCity.trim());
                updateProfile(addressCity.trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateProfile(String addressCity) {

        if (!NetworkUtils.isNetworkConnected(HomeActivity.this)) {
            return;
        }
        User user = AppPreferencesHelper.getInstance(HomeActivity.this).getUser();
        if (user == null) return;

        String name = user.getDisplayName() != null ? user.getDisplayName() : "";
        String email = user.getEmail() != null ? user.getEmail() : "";
        String phoneNumber = user.getPhoneNumber() != null ? user.getPhoneNumber() : "";
        String dob = user.getDob() != null ? user.getDob() : "";
        String favourite = user.getFavoriteDrink() != null ? user.getFavoriteDrink() : "";

        String[] nameArray = CommonUtils.getFirstNameAndLastName(name);
        String username = email.split("@")[0];
        String userId = AppPreferencesHelper.getInstance(HomeActivity.this).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> updateCall = userApiInterface.updateUser(userId, nameArray[0], nameArray[1], email, username, phoneNumber, dob, favourite, addressCity, "",false);
        updateCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    //success
                } else {
                    //failed
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    @SuppressLint("WrongViewCast")
    private void initSearchClick() {
        toolbarSearch = findViewById(R.id.toolbar_search);
        toolbarSearch.setVisibility(View.VISIBLE);
        toolbarSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchBottleActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getAppVersionCode() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<AppVersion> apiCall = userApiInterface.getAppVersionCode();
        apiCall.enqueue(new Callback<AppVersion>() {
            @Override
            public void onResponse(Call<AppVersion> call, Response<AppVersion> response) {
                if (response.isSuccessful()) {
                    AppVersion appVersion = response.body();
                    if (appVersion == null) return;
                    PackageInfo pInfo = null;
                    try {
                        pInfo = HomeActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                        if (pInfo != null) {
                            int verCode = pInfo.versionCode;
                            if (verCode < appVersion.getVersionCode()) {
                                if (!isFinishing()) {
                                    alert(HomeActivity.this, "New Version Found!!", appVersion.getMessage(), getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), false, true);
                                }
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AppVersion> call, Throwable t) {

            }
        });
    }

    public void alert(Context context, String title, String message, String positiveButton, String negativeButton, boolean isNegativeButton, boolean isTitle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (isTitle) {
            builder.setTitle(title);
        }

        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                toOpenPlayStore();
                finish();
            }
        });
        if (isNegativeButton) {
            builder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    finish();
                }
            });
        }
        builder.show();
    }

    public void getUserReferralCode() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        String code = AppPreferencesHelper.getInstance(HomeActivity.this).getUserReferralCode();
        if (!TextUtils.isEmpty(code)) {
            return;
        }
        String userId = AppPreferencesHelper.getInstance(HomeActivity.this).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> apiCall = userApiInterface.getUserReferralCode(userId);
        apiCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    if (user == null) return;
                    AppPreferencesHelper.getInstance(HomeActivity.this).setUserReferralCode(user.getReferralCode());

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private class FetchContactTask extends AsyncTask<Void, Void, List<ContactModel>> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<ContactModel> doInBackground(Void... contact) {
            if (AppPreferencesHelper.getInstance(HomeActivity.this).isContactSynced()) {
                return null;
            }
            List<ContactModel> contactModels = getContactList();
            return contactModels;
        }

        @Override
        protected void onPostExecute(List<ContactModel> contactModels) {
            try {
                if (contactModels == null) return;
                if (contactModels.isEmpty()) return;
                uploadContactList(contactModels);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<ContactModel> getContactList() {

        Context context = HomeActivity.this;
        List<ContactModel> list = new ArrayList<>();
        if (context == null) return list;

        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor cursorInfo = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (cursorInfo == null) continue;
                    while (cursorInfo.moveToNext()) {
                        ContactModel contactModel = new ContactModel();
                        contactModel.setId(id);
                        contactModel.setName(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                        contactModel.setMobileNumber(cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                        list.add(contactModel);
                    }

                    cursorInfo.close();
                }
            }
            cursor.close();
        }
        return list;
    }

    public void uploadContactList(List<ContactModel> contactModels) {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        String userId = AppPreferencesHelper.getInstance(HomeActivity.this).getUserId();
        UploadContactModel uploadContactModel = new UploadContactModel();
        uploadContactModel.setUserId(userId);
        uploadContactModel.setContacts(contactModels);

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<Void> apiCall = userApiInterface.uploadContactList(uploadContactModel);
        apiCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    AppPreferencesHelper.getInstance(HomeActivity.this).setContactSynced(true);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

}
