package com.mobile.bvault.ui.activity;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.IPLApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.IPLMatchModel;
import com.mobile.bvault.model.UserEarningModel;
import com.mobile.bvault.ui.adapter.IPLPagerAdapter;
import com.mobile.bvault.ui.fragment.IPLTodayFragment;
import com.mobile.bvault.ui.fragment.IPLTomorrowFragment;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 01/11/17.
 */

public class IplMatchActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.tabLayout)
    public TabLayout tabLayout;
    @BindView(R.id.viewPager)
    public ViewPager viewPager;

    private IPLPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipl_match);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        //setTitle(getString(R.string.title_ipl));
        setTitle(getString(R.string.play_fifa));
        initDrawer();
        setTabLayout();
        getMatchList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getEarningPoints();
    }

    private void setTabLayout() {

        tabLayout.addTab(tabLayout.newTab().setText("Today"));
        tabLayout.addTab(tabLayout.newTab().setText("Tomorrow"));
        //Creating our pager adapter
        adapter = new IPLPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);

        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.gradient_start_color));
            drawable.setSize(3, 1);
            ((LinearLayout) root).setDividerPadding(12);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void getMatchList() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        showLoading();
        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        //String userId = AppPreferencesHelper.getInstance(getActivity()).getUserId();
        Call<List<IPLMatchModel>> apiCall = iplApiInterface.getAllMatches();

        apiCall.enqueue(new Callback<List<IPLMatchModel>>() {
            @Override
            public void onResponse(Call<List<IPLMatchModel>> call, Response<List<IPLMatchModel>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    showTodayMatchList(response.body());
                    showTomorrowMatchList(response.body());
                } else {
                    showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<IPLMatchModel>> call, Throwable t) {
                hideLoading();
                showSnackBar(t.getMessage());
            }
        });
    }

    private void showTodayMatchList(List<IPLMatchModel> matchModelList) {
        if (matchModelList == null) return;
        List<IPLMatchModel> todayMatchModelList = new ArrayList<>();
        for (int i = 0; i < matchModelList.size(); i++) {
            IPLMatchModel matchModel = matchModelList.get(i);
            if (matchModel == null) continue;
            if (matchModel.getMatchStartDate() == null) continue;
            if (matchModel.getMatchStartDate().substring(0, 10).equalsIgnoreCase(getTodayDate())) {
                todayMatchModelList.add(matchModel);
            }
        }
        IPLTodayFragment fragment = (IPLTodayFragment) adapter.getFragment(0);
        if (fragment != null) {
            fragment.updateAdapter(todayMatchModelList);
        }
    }

    private String getTodayDate() {
        String formattedDate = "";
        try {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            formattedDate = df.format(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    private void showTomorrowMatchList(List<IPLMatchModel> matchModelList) {
        if (matchModelList == null) return;
        List<IPLMatchModel> tomorrowMatchModelList = new ArrayList<>();
        for (int i = 0; i < matchModelList.size(); i++) {
            IPLMatchModel matchModel = matchModelList.get(i);
            if (matchModel == null) continue;
            if (matchModel.getMatchStartDate() == null) continue;
            if (matchModel.getMatchStartDate().substring(0, 10).equalsIgnoreCase(getTomorrowDate())) {
                tomorrowMatchModelList.add(matchModel);
            }
        }
        IPLTomorrowFragment fragment = (IPLTomorrowFragment) adapter.getFragment(1);
        if (fragment != null) {
            fragment.updateAdapter(tomorrowMatchModelList);
        }
    }

    private String getTomorrowDate() {
        String tomorrowAsString = "";
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = calendar.getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            tomorrowAsString = dateFormat.format(tomorrow);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tomorrowAsString;
    }

    public void getEarningPoints() {

        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        Call<UserEarningModel> apiCall = iplApiInterface.getUserEarningPoints(userId);

        apiCall.enqueue(new Callback<UserEarningModel>() {
            @Override
            public void onResponse(Call<UserEarningModel> call, Response<UserEarningModel> response) {
                IPLTodayFragment fragment1 = (IPLTodayFragment) adapter.getFragment(0);
                IPLTomorrowFragment fragment2 = (IPLTomorrowFragment) adapter.getFragment(1);
                if (response.isSuccessful()) {
                    UserEarningModel userEarningModel = response.body();
                    if (userEarningModel != null) {
                        if (fragment1 != null) {
                            fragment1.updateEarningPoint(userEarningModel);
                        }
                        if (fragment2 != null) {
                            fragment2.updateEarningPoint(userEarningModel);
                        }
                    }
                } else {
                    showSnackBar(response.message());
                    if (fragment1 != null) {
                        fragment1.desableBottles();
                    }
                    if (fragment2 != null) {
                        fragment2.desableBottles();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserEarningModel> call, Throwable t) {
                showSnackBar(t.getMessage());
                IPLTodayFragment fragment1 = (IPLTodayFragment) adapter.getFragment(0);
                IPLTomorrowFragment fragment2 = (IPLTomorrowFragment) adapter.getFragment(1);
                if (fragment1 != null) {
                    fragment1.desableBottles();
                }
                if (fragment2 != null) {
                    fragment2.desableBottles();
                }
            }
        });
    }

}
