package com.mobile.bvault.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobile.bvault.BuildConfig;
import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.Wishlist;
import com.mobile.bvault.presenter.PaymentPresenter;
import com.mobile.bvault.presenter.PromoCodePresenter;
import com.mobile.bvault.presenter.PurchasePresenter;
import com.mobile.bvault.ui.adapter.WishlistAdapter;
import com.mobile.bvault.presenter.WishlistPresenter;
import com.mobile.bvault.ui.view.PromoCodeView;
import com.mobile.bvault.ui.view.PurchaseView;
import com.mobile.bvault.ui.view.WishlistView;
import com.mobile.bvault.utils.CommonUtils;
import com.payu.india.CallBackHandler.OnetapCallback;
import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Interfaces.OneClickPaymentListener;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.payuui.Activity.PayUBaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobile.bvault.utils.Constants.PAYTM_REQUEST_CODE;

public class WishlistActivity extends BaseActivity implements WishlistView, WishlistAdapter.WishlistAdapterListener, PurchaseView, View.OnClickListener, PromoCodeView {

    private static final String TAG = "WishlistActivity";
    private static final int PROMOCODE_REQUEST_CODE = 101;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.total_amount_text_view)
    TextView totalAmountTextView;
    @BindView(R.id.amount_text_view)
    TextView amountTextView;
    @BindView(R.id.discounted_amount_text_view)
    TextView discountAmount;
    @BindView(R.id.bottom_layout)
    RelativeLayout bottomLayout;
    @BindView(R.id.agreement_text_view)
    TextView agreementTextView;
    @BindView(R.id.promo_code_ll)
    LinearLayout promo_code_ll;
    @BindView(R.id.promo_code_applied_ll)
    LinearLayout promo_code_applied_ll;
    @BindView(R.id.promo_code_applied_text_view)
    TextView promo_code_applied_text_view;

    //    Dialog View
    EditText etPromoCode;
    Button cancelButton;
    Button applyButton;

    Dialog promoCodeDialog;

    Dialog referralCodeDialog;
    EditText etReferralCode;
    Button cancelRefButton;
    Button applyRefButton;
    private String referralCode;

    private WishlistPresenter wishlistPresenter;
    private PurchasePresenter purchasePresenter;
    private PromoCodePresenter promoCodePresenter;
    private PaymentPresenter paymentPresenter;

    int discountPercent = 0;

    private PromoCode appliedPromoCode = null;
    double totalAmount = 0;
    double actualAmount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setTitle(getString(R.string.my_order));
        initDrawer();
        initDialog();
        initRefCodeDialog();
        setupRecyclerView();
        initPresenter();
    }

    private void initPresenter() {
        wishlistPresenter = new WishlistPresenter(this, this);
        wishlistPresenter.getWishlist();
        purchasePresenter = new PurchasePresenter(this, this);
        promoCodePresenter = new PromoCodePresenter(this, this);
        paymentPresenter = new PaymentPresenter(this);
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initDialog() {
        promoCodeDialog = new Dialog(this);
        promoCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        promoCodeDialog.setContentView(R.layout.view_promocode);
        promoCodeDialog.setCanceledOnTouchOutside(false);
        etPromoCode = (EditText) promoCodeDialog.findViewById(R.id.et_promo_code);
        applyButton = (Button) promoCodeDialog.findViewById(R.id.btn_apply);
        applyButton.setOnClickListener(this);
        cancelButton = (Button) promoCodeDialog.findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(this);
    }

    private void initRefCodeDialog() {
        referralCodeDialog = new Dialog(this);
        referralCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralCodeDialog.setContentView(R.layout.view_referral_promo_code);
        referralCodeDialog.setCanceledOnTouchOutside(false);
        etReferralCode = referralCodeDialog.findViewById(R.id.et_referral_code);
        applyRefButton = (Button) referralCodeDialog.findViewById(R.id.btn_ref_apply);
        applyRefButton.setOnClickListener(this);
        cancelRefButton = (Button) referralCodeDialog.findViewById(R.id.btn_ref_cancel);
        cancelRefButton.setOnClickListener(this);
    }

    @Override
    public void loadWishlist(List<Wishlist> wishlists) {
        if (CommonUtils.getCollectionSize(wishlists) > 0)
            bottomLayout.setVisibility(View.VISIBLE);
        else
            bottomLayout.setVisibility(View.GONE);

        recyclerView.setAdapter(new WishlistAdapter(this, wishlists, this, wishlistPresenter));

    }

    @OnClick(R.id.proceed_button)
    @Override
    public void proceedPurchase() {
        if (paymentPresenter != null) {
            //showLoading();
            //purchasePresenter.savePurchasedBottle(appliedPromoCode);
            //promoCodePresenter.useRefPromoCode(referralCode);
            //navigateToBaseActivity();
            Intent intent = new Intent(WishlistActivity.this, PaymentTypeActivity.class);
            intent.putExtra("totalAmount", totalAmount);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);

        }
    }

    @OnClick(R.id.agreement_text_view)
    void showTermsAndCondition() {
        CommonUtils.showTermsAndCondition(this);
    }

    @OnClick(R.id.promo_code_ll)
    void toOpenPromoCode() {
        Intent intent = new Intent(WishlistActivity.this, PromoCodeActivity.class);
        intent.putExtra("totalAmount", totalAmount);
        startActivityForResult(intent, PROMOCODE_REQUEST_CODE);
    }

    @OnClick(R.id.canclePromocodeIV)
    void canclePromocode() {
        promo_code_ll.setVisibility(View.VISIBLE);
        promo_code_applied_ll.setVisibility(View.GONE);
        appliedPromoCode = null;
        referralCode = "";
        setTotalAmount(actualAmount);
    }

    @Override
    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
        this.actualAmount = totalAmount;
        amountTextView.setText("\u20B9 " + String.format("%,.2f", this.totalAmount));

        if (appliedPromoCode != null) {
            float amount = appliedPromoCode.getDiscount();
            if (this.actualAmount >= appliedPromoCode.getMinAmountForDiscount()) {
                this.totalAmount = this.totalAmount - amount;
                totalAmountTextView.setText("\u20B9 " + String.format("%,.2f", this.totalAmount));
                discountAmount.setText("\u20B9 " + String.format("%,.2f", amount));

                promo_code_ll.setVisibility(View.GONE);
                promo_code_applied_ll.setVisibility(View.VISIBLE);
            } else {
                promo_code_ll.setVisibility(View.VISIBLE);
                promo_code_applied_ll.setVisibility(View.GONE);

                discountAmount.setText("\u20B9 " + String.format("%,.2f", 0.00));
                totalAmountTextView.setText("\u20B9 " + String.format("%,.2f", this.totalAmount));
                showToastMessage("To apply promo-code, Minimum purchase amount should be greater than " + appliedPromoCode.getMinAmountForDiscount());
            }
        } else {
            discountAmount.setText("\u20B9 " + String.format("%,.2f", 0.00));
            totalAmountTextView.setText("\u20B9 " + String.format("%,.2f", this.totalAmount));
        }
        if (this.totalAmount > 0) bottomLayout.setVisibility(View.VISIBLE);
        else bottomLayout.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(Wishlist wishlist) {
//        startActivity(new Intent(this, HotelListActivity.class));
    }

    @Override
    public void onPurchaseSuccess() {
        wishlistPresenter.clearWishlist();
        hideLoading();
        startActivity(new Intent(this, PurchasedMessageActivity.class));
        finish();
    }

    @Override
    public void onPurchaseFailure(String message) {
        hideLoading();
        showSnackBar(message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                hideDialog();
                break;
            case R.id.btn_apply:
                applyPromoCode();
                break;
            case R.id.btn_ref_cancel:
                hideRefCodeDialog();
                break;
            case R.id.btn_ref_apply:
                applyReferralCode();
                break;
        }
    }

    void hideDialog() {
        if (promoCodeDialog != null)
            promoCodeDialog.dismiss();
    }

    void showDialog() {
        if (promoCodeDialog != null) {
            promoCodeDialog.show();
            etPromoCode.setText("");
        }
    }

    void showRefCodeDialog() {
        if (referralCodeDialog != null) {
            referralCodeDialog.show();
            etReferralCode.setText("");
        }
    }

    void hideRefCodeDialog() {
        if (referralCodeDialog != null)
            referralCodeDialog.dismiss();
    }

    void applyPromoCode() {
        String code = etPromoCode.getText().toString().trim();
        if (code.isEmpty()) {
            showToastMessage("Please enter promo code");
        } else {
            if (promoCodePresenter != null) {
                referralCode = "";
                showLoading();
                promoCodePresenter.applyCode(code);
            }
        }
    }

    void applyReferralCode() {
        referralCode = etReferralCode.getText().toString().trim();
        if (referralCode.isEmpty()) {
            showToastMessage("Please enter referral promo code");
        } else {
            if (promoCodePresenter != null) {
                showLoading();
                promoCodePresenter.applyRefPromoCode(referralCode);
            }
        }
    }

    @Override
    public void onPromoCodeAppliedSuccessfully(PromoCode promoCode) {
        hideLoading();
        hideDialog();
        hideRefCodeDialog();
        if (promoCode != null) {
            if (this.actualAmount < promoCode.getDiscount()) {
                showToastMessage("To apply this promo-code, Minimum purchase amount should be greater than " + String.format("%,.2f", promoCode.getDiscount()));
                referralCode = "";
                promo_code_ll.setVisibility(View.VISIBLE);
                promo_code_applied_ll.setVisibility(View.GONE);
                return;
            }
            if (this.actualAmount >= promoCode.getMinAmountForDiscount()) {
                appliedPromoCode = promoCode;
                referralCode = promoCode.getPromoCode();
                if (wishlistPresenter != null)
                    wishlistPresenter.getTotalAmount();
                promo_code_ll.setVisibility(View.GONE);
                promo_code_applied_ll.setVisibility(View.VISIBLE);
                String descriptionText = "Your promo-code " + promoCode.getPromoCode() + " has been applied. You have got RS" + String.format("%,.2f", promoCode.getDiscount()) + " discount.";
                promo_code_applied_text_view.setText(descriptionText);
            } else {
                promo_code_ll.setVisibility(View.VISIBLE);
                promo_code_applied_ll.setVisibility(View.GONE);
                showToastMessage("To apply this promo-code, Minimum purchase amount should be greater than " + promoCode.getMinAmountForDiscount());
            }
        }
    }

    @Override
    public void onPromoCodeFailure(String message) {
        referralCode = "";
        appliedPromoCode = null;
        hideLoading();
        if (message != null)
            showToastMessage(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                try {
                    JSONObject jsonObject = new JSONObject(data.getStringExtra(PayuConstants.PAYU_RESPONSE));
                    if (purchasePresenter != null) {
                        purchasePresenter.savePayUPurchaseResponse(appliedPromoCode, jsonObject, totalAmount);
                    }
                    if (jsonObject.has("STATUS")) {
                        String status = jsonObject.getString("STATUS");
                        if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("TXN_SUCCESS")) {
                            if (purchasePresenter != null) {
                                showLoading();
                                purchasePresenter.savePurchasedBottle(appliedPromoCode);
                                promoCodePresenter.useRefPromoCode(referralCode);
                            }
                        }
                        return;
                    }
                    if (jsonObject.has(PayuConstants.STATUS)) {
                        if (jsonObject.getString(PayuConstants.STATUS).equalsIgnoreCase(PayuConstants.SUCCESS)) {
                            if (purchasePresenter != null) {
                                showLoading();
                                purchasePresenter.savePurchasedBottle(appliedPromoCode);
                                promoCodePresenter.useRefPromoCode(referralCode);
                            }
                        } else {
                            hideLoading();
                            showToastMessage("Some problem occur, please try again");
                        }
                    } else {
                        hideLoading();
                        showToastMessage("Some problem occur, please try again");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();
                    showToastMessage("Some problem occur, please try again");
                }
            } else {
                hideLoading();
            }
        } else if (requestCode == PROMOCODE_REQUEST_CODE && resultCode == RESULT_OK) {
            PromoCode promoCode = null;
            if (data != null) {
                promoCode = data.getParcelableExtra("promoCode");
            }
            onPromoCodeAppliedSuccessfully(promoCode);
        }
    }
}
