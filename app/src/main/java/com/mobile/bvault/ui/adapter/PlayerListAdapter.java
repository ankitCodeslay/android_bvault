package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.PlayerModel;
import com.mobile.bvault.model.PlayerModel;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

/**
 * Created by diptif on 21/08/17.
 */

public class PlayerListAdapter extends BaseAdapter {

    private static final String TAG = "IPLMatchModelListAdapter";

    private Context context;
    private List<PlayerModel> playerModelList;
    private LayoutInflater mInflater;
    private int team1Count;

    public PlayerListAdapter(Context context, List<PlayerModel> playerModelList, int team1Count) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.playerModelList = playerModelList;
        this.team1Count = team1Count;
    }

    @Override
    public boolean isEnabled(int position) {
        PlayerModel playerModel = playerModelList.get(position);
        if (playerModel.getPlayerName().equalsIgnoreCase("Select Player")) {
            return true;
        } else if (playerModel.getTeamName().equalsIgnoreCase(playerModelList.get(1).getPlayerName())) {
            return true;
        } else if (playerModel.getTeamName().equalsIgnoreCase(playerModelList.get(team1Count + 2).getPlayerName())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getCount() {
        int size = CommonUtils.getCollectionSize(this.playerModelList);
        return size > 0 ? size : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.view_team_list, parent, false);
            holder.teamNameTV = (TextView) convertView.findViewById(R.id.teamNameTV);
            holder.teamListContainer = convertView.findViewById(R.id.team_list_container);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        PlayerModel playerModel = playerModelList.get(position);
        if (playerModel != null) {
            if (playerModel.getPlayerName().equalsIgnoreCase("Select Player")) {
                holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIpl));
                holder.teamNameTV.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.teamNameTV.setTextSize(22);
            } else if (playerModel.getTeamName().equalsIgnoreCase(playerModelList.get(1).getPlayerName())) {
                holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIplTeam1));
                holder.teamNameTV.setTextColor(ContextCompat.getColor(context, R.color.white));
                holder.teamNameTV.setTextSize(16);
            } else if (playerModel.getTeamName().equalsIgnoreCase(playerModelList.get(team1Count + 2).getPlayerName())) {
                holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIplTeam2));
                holder.teamNameTV.setTextColor(ContextCompat.getColor(context, R.color.black));
                holder.teamNameTV.setTextSize(16);
            } else {
                holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIplTeamHeading));
                holder.teamNameTV.setTextColor(ContextCompat.getColor(context, R.color.white));
                holder.teamNameTV.setTextSize(16);
            }
            holder.teamNameTV.setText(playerModel.getPlayerName());
        }
        parent.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIpl));
        convertView.setTag(holder);
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public class ViewHolder {
        LinearLayout teamListContainer;
        TextView teamNameTV;
    }
}
