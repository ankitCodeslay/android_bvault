package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 21/08/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "NotificationAdapter";

    private Context context;
    private List<Payment> payments;
    private OnItemClickListener onItemClickListener;

    public NotificationAdapter(Context context, List<Payment> payments, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.payments = payments;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_notification, parent, false);
            return new ViewHolder(view);
        } else {
            return EmptyViewHolder.getEmptyViewHolder(parent, viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (CommonUtils.getCollectionSize(payments) > 0)
            return 0;
        else
            return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder viewHolder = (ViewHolder) holder;
                if (viewHolder != null) {
                    viewHolder.notificationTextView.setText(payments.get(position).getHotel().getName()+" requested for payment");
                    viewHolder.paymentStatus.setText(payments.get(position).getStatus());
                    viewHolder.onClick(payments.get(position),onItemClickListener);
                }
                break;
            case 1:
                EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
                if (emptyViewHolder != null) {
                    emptyViewHolder.setEmptyImageView(R.drawable.bottles);
                    emptyViewHolder.setMessage(R.string.payment_empty_message);
                    emptyViewHolder.onClick(context);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        int size = CommonUtils.getCollectionSize(this.payments);
        return  size > 0 ? size : 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.notification_text_view) TextView notificationTextView;
        @BindView(R.id.payment_status) TextView paymentStatus;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            setBackgroundColor();
        }

        public void setBackgroundColor() {
            itemView.setBackground(ContextCompat.getDrawable(context,R.drawable.list_selector));
        }

        public void onClick(final Payment payment, final OnItemClickListener onItemClick){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnItemClick(payment);
                }
            });
        }
    }


    public interface OnItemClickListener {

        void OnItemClick(Payment payment);
    }
}
