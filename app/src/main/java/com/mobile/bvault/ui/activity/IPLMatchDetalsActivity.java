package com.mobile.bvault.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.network.IPLApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.DateTimeModel;
import com.mobile.bvault.model.GetPlayerModel;
import com.mobile.bvault.model.IPLMatchModel;
import com.mobile.bvault.model.IPLTeamModel;
import com.mobile.bvault.model.PlayerModel;
import com.mobile.bvault.model.PurchaseRequest;
import com.mobile.bvault.model.PurchasedBottle;
import com.mobile.bvault.model.SubmitIPLAnsModel;
import com.mobile.bvault.model.TeamModel;
import com.mobile.bvault.model.UserAnsModel;
import com.mobile.bvault.model.UserEarningModel;
import com.mobile.bvault.ui.adapter.PlayerListAdapter;
import com.mobile.bvault.ui.adapter.TeamListAdapter;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.DateTimeUtils;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.NetworkUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mobile.bvault.utils.Constants.BOTTLE1_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE2_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE3_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE4_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_1;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_2;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_3;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_4;
import static com.mobile.bvault.utils.Constants.IPL_IMAGE_URL;

/**
 * Created by diptif on 01/11/16.
 */

public class IPLMatchDetalsActivity extends BaseActivity {

    private final String KEY_SELECT_TEAM = "Select Team";
    private final String KEY_SELECT_PLAYER = "Select Player";

    @BindView(R.id.earnedTV)
    public TextView earnedTV;

    @BindView(R.id.bottle1LL)
    public LinearLayout bottle1LL;
    @BindView(R.id.bottle2LL)
    public LinearLayout bottle2LL;

    @BindView(R.id.bottle3LL)
    public LinearLayout bottle3LL;
    @BindView(R.id.bottle4LL)
    public LinearLayout bottle4LL;

    @BindView(R.id.iplBannerIV)
    public ImageView iplBannerIV;
    @BindView(R.id.teamSpinner)
    public Spinner teamSpinner;
    @BindView(R.id.team1PlayerSpinner)
    public Spinner team1PlayerSpinner;
    @BindView(R.id.team2PlayerSpinner)
    public Spinner team2PlayerSpinner;
    @BindView(R.id.submit_ans_button)
    public Button submitAnsButton;

    @BindView(R.id.bottle1PointsTV)
    TextView bottle1PointsTV;
    @BindView(R.id.bottle2PointsTV)
    TextView bottle2PointsTV;

    @BindView(R.id.bottle3PointsTV)
    TextView bottle3PointsTV;
    @BindView(R.id.bottle4PointsTV)
    TextView bottle4PointsTV;

    private IPLMatchModel iplMatchModel;
    private ArrayList<TeamModel> teamArrayList;
    private ArrayList<PlayerModel> playerArrayList;
    private List<PlayerModel> team1PlayerList;
    private List<PlayerModel> team2PlayerList;
    private PlayerListAdapter playerAdapter;
    private TeamListAdapter teamAdapter;

    private String ques1Ans = "";
    private String ques2Ans = "";
    private String ques3Ans = "";

    private UserAnsModel userAnsModel;
    private boolean isUserAnswered;
    DateTimeModel dateTimeModel;

    private int totalEarningPoints;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipl_match_details);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        //setTitle(getString(R.string.title_ipl));
        setTitle(getString(R.string.play_fifa));
        initDrawer();
        if (getIntent().getExtras() != null) {
            iplMatchModel = getIntent().getExtras().getParcelable("iplMatchModel");
        }
        bottle1PointsTV.setText("Points: " + BOTTLE1_POINT);
        bottle2PointsTV.setText("Points: " + BOTTLE2_POINT);
        bottle3PointsTV.setText("Points: " + BOTTLE3_POINT);
        bottle4PointsTV.setText("Points: " + BOTTLE4_POINT);

        setBannerImage();
        initTeamSpinner();
        validateSubmitButton();
        initListners();
        desableBottles();
        getCurrentTime();
        validateAnswer();
        getPlayerList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getEarningPoints();
    }

    private void initTeamSpinner() {
        teamArrayList = new ArrayList<>();
        teamArrayList.add(new TeamModel("", KEY_SELECT_TEAM));
        teamArrayList.add(new TeamModel(iplMatchModel.getTeamOneId(), iplMatchModel.getTeamOne()));
        teamArrayList.add(new TeamModel(iplMatchModel.getTeamTwoId(), iplMatchModel.getTeamTwo()));
        teamArrayList.add(new TeamModel("5af353e83b6a04046889f333", "DRAW"));

        teamAdapter = new TeamListAdapter(this, teamArrayList);
        teamSpinner.setAdapter(teamAdapter);

        teamSpinner.setSelection(getAns1Possition());
    }

    private void initListners() {
        teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view != null) {
                    view.setBackground(ContextCompat.getDrawable(IPLMatchDetalsActivity.this, R.drawable.ipl_spinner_bg));
                    TextView textView = view.findViewById(R.id.teamNameTV);
                    if (textView != null) {
                        textView.setTextColor(Color.BLACK);
                        textView.setTextSize(16);
                    }
                }
                if (teamArrayList != null && teamArrayList.size() > position) {
                    TeamModel selectedItem = teamArrayList.get(position);
                    if (selectedItem == null) {
                        ques1Ans = "";
                        return;
                    }
                    if (selectedItem.getTeamId().isEmpty()) {
                        ques1Ans = "";
                        return;
                    }
                    ques1Ans = selectedItem.getTeamId();
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        team1PlayerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                view.setBackground(ContextCompat.getDrawable(IPLMatchDetalsActivity.this, R.drawable.ipl_spinner_bg));
                TextView textView = view.findViewById(R.id.teamNameTV);
                textView.setTextColor(Color.BLACK);
                textView.setTextSize(16);

                PlayerModel selectedItem = playerArrayList.get(position);
                if (selectedItem == null) {
                    ques2Ans = "";
                    return;
                }
                if (selectedItem.getId().isEmpty()) {
                    ques2Ans = "";
                    return;
                }
                ques2Ans = selectedItem.getId();

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        team2PlayerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                view.setBackground(ContextCompat.getDrawable(IPLMatchDetalsActivity.this, R.drawable.ipl_spinner_bg));
                TextView textView = view.findViewById(R.id.teamNameTV);
                textView.setTextColor(Color.BLACK);
                textView.setTextSize(16);

                PlayerModel selectedItem = playerArrayList.get(position);
                if (selectedItem == null) {
                    ques3Ans = "";
                    return;
                }
                if (selectedItem.getId().isEmpty()) {
                    ques3Ans = "";
                    return;
                }
                ques3Ans = selectedItem.getId();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submitAnsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidFields()) {
                    submitAnswer();
                }
            }
        });

        bottle1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_1);

            }
        });
        bottle2LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_2);
            }
        });

        bottle3LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_3);

            }
        });
        bottle4LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_4);
            }
        });
    }

    public void getPlayerList() {
        if (iplMatchModel == null) return;
        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        showLoading();
        GetPlayerModel getPlayerModel = new GetPlayerModel();
        getPlayerModel.setTeamOneId(iplMatchModel.getTeamOneId());
        getPlayerModel.setTeamTwoId(iplMatchModel.getTeamTwoId());

        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        Call<IPLTeamModel> apiCall = iplApiInterface.getPlayerList(getPlayerModel);

        apiCall.enqueue(new Callback<IPLTeamModel>() {
            @Override
            public void onResponse(Call<IPLTeamModel> call, Response<IPLTeamModel> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    showPlayerList(response.body());
                } else {
                    showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<IPLTeamModel> call, Throwable t) {
                hideLoading();
                showSnackBar(t.getMessage());
            }
        });
    }

    private void showPlayerList(IPLTeamModel iplTeamModel) {
        if (iplTeamModel == null) return;
        team1PlayerList = iplTeamModel.getTeamOnePlayers();
        team2PlayerList = iplTeamModel.getTeamtwoPlayers();

        playerArrayList = new ArrayList<>();
        playerArrayList.add(new PlayerModel("", KEY_SELECT_PLAYER, ""));
        playerArrayList.add(new PlayerModel("", iplMatchModel.getTeamOne(), ""));
        if (team1PlayerList != null) {
            for (int i = 0; i < team1PlayerList.size(); i++) {
                PlayerModel playerModel = team1PlayerList.get(i);
                if (playerModel == null) continue;
                playerArrayList.add(new PlayerModel(playerModel.getId(), playerModel.getPlayerName(), iplMatchModel.getTeamOne()));
            }
        }
        playerArrayList.add(new PlayerModel("", iplMatchModel.getTeamTwo(), ""));
        if (team2PlayerList != null) {
            for (int i = 0; i < team2PlayerList.size(); i++) {
                PlayerModel playerModel = team2PlayerList.get(i);
                if (playerModel == null) continue;
                playerArrayList.add(new PlayerModel(playerModel.getId(), playerModel.getPlayerName(), iplMatchModel.getTeamTwo()));
            }
        }

        playerArrayList.add(new PlayerModel("5af2c5d59422d7154c61e222", "None", ""));

        playerAdapter = new PlayerListAdapter(this, playerArrayList, team1PlayerList.size());
        team1PlayerSpinner.setAdapter(playerAdapter);
        team2PlayerSpinner.setAdapter(playerAdapter);

        team1PlayerSpinner.setSelection(getAns2Possition());
        team2PlayerSpinner.setSelection(getAns3Possition());
    }


    private boolean isValidFields() {

        if (ques1Ans.isEmpty()) {
            showSnackBar("Please select the team");
            return false;
        } else if (ques2Ans.isEmpty()) {
            showSnackBar("Please select the player");
            return false;
        } else if (ques3Ans.isEmpty()) {
            showSnackBar("Please select the player");
            return false;
        }

        return true;
    }

    public void submitAnswer() {
        if (iplMatchModel == null) return;
        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        showLoading();
        SubmitIPLAnsModel ansModel = new SubmitIPLAnsModel();
        ansModel.setMatchId(iplMatchModel.getId());
        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        ansModel.setUserId(userId);
        ansModel.setAnsOne(ques1Ans);
        ansModel.setAnsTwo(ques2Ans);
        ansModel.setAnsThree(ques3Ans);

        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        Call<SubmitIPLAnsModel> apiCall = iplApiInterface.submitAnswer(ansModel);

        apiCall.enqueue(new Callback<SubmitIPLAnsModel>() {
            @Override
            public void onResponse(Call<SubmitIPLAnsModel> call, Response<SubmitIPLAnsModel> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    showToastMessage("Your information has been submitted.");
                    finish();
                } else {
                    showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<SubmitIPLAnsModel> call, Throwable t) {
                hideLoading();
                showSnackBar(t.getMessage());
            }
        });
    }

    public void validateAnswer() {
        if (iplMatchModel == null) return;
        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        SubmitIPLAnsModel ansModel = new SubmitIPLAnsModel();
        ansModel.setMatchId(iplMatchModel.getId());
        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        ansModel.setUserId(userId);

        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        Call<SubmitIPLAnsModel> apiCall = iplApiInterface.validateAnswer(ansModel);

        apiCall.enqueue(new Callback<SubmitIPLAnsModel>() {
            @Override
            public void onResponse(Call<SubmitIPLAnsModel> call, Response<SubmitIPLAnsModel> response) {
                if (response.isSuccessful()) {
                    SubmitIPLAnsModel iplAnsModel = response.body();
                    if (iplAnsModel != null) {
                        if (iplAnsModel.getAnsOne() != null && iplAnsModel.getAnsOne().equalsIgnoreCase("true")) {
                            isUserAnswered = true;
                        }
                        userAnsModel = iplAnsModel.getUserAnsModel();
                        teamSpinner.setSelection(getAns1Possition());
                        team1PlayerSpinner.setSelection(getAns2Possition());
                        team2PlayerSpinner.setSelection(getAns3Possition());
                    }
                } else {
                    showSnackBar(response.message());
                }
                validateSubmitButton();
            }

            @Override
            public void onFailure(Call<SubmitIPLAnsModel> call, Throwable t) {
                showSnackBar(t.getMessage());
            }
        });
    }

    private void validateSubmitButton() {
        if (iplMatchModel == null) return;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date currentTime = null;
        try {
            String dateTimeStr = iplMatchModel.getMatchStartDate().substring(0, 10) + " " + iplMatchModel.getMatchStartTime();
            Date endTime = dateFormat.parse(dateTimeStr);

            if (dateTimeModel == null) {
                currentTime = dateFormat.parse(dateFormat.format(new Date()));
            } else {
                String currentDateTimeStr = DateTimeUtils.getDateByTimestamp(dateTimeModel.getCurrentTime()) + " " + DateTimeUtils.getTimeByTimestamp(dateTimeModel.getCurrentTime());
                currentTime = dateFormat.parse(currentDateTimeStr);
            }

            if (currentTime.after(endTime) || isUserAnswered) {
                submitAnsButton.setClickable(false);
                submitAnsButton.setEnabled(false);
                submitAnsButton.setAlpha(0.5f);
            } else {
                submitAnsButton.setClickable(true);
                submitAnsButton.setEnabled(true);
                submitAnsButton.setAlpha(1.0f);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setBannerImage() {
        String imageUrl = IPL_IMAGE_URL + "FIFA_Banner1.jpeg";
        ImageUtil.setImage(this, iplBannerIV, imageUrl, R.drawable.ipl_banner);
    }

    public void getEarningPoints() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            desableBottles();
            return;
        }
        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        Call<UserEarningModel> apiCall = iplApiInterface.getUserEarningPoints(userId);

        apiCall.enqueue(new Callback<UserEarningModel>() {
            @Override
            public void onResponse(Call<UserEarningModel> call, Response<UserEarningModel> response) {
                if (response.isSuccessful()) {
                    UserEarningModel userEarningModel = response.body();
                    updateEarningPoint(userEarningModel);
                } else {
                    desableBottles();
                    showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<UserEarningModel> call, Throwable t) {
                showSnackBar(t.getMessage());
                desableBottles();
            }
        });
    }

    public void getCurrentTime() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        Call<DateTimeModel> apiCall = iplApiInterface.getCurrentDateTime();

        apiCall.enqueue(new Callback<DateTimeModel>() {
            @Override
            public void onResponse(Call<DateTimeModel> call, Response<DateTimeModel> response) {
                if (response.isSuccessful()) {
                    dateTimeModel = response.body();
                    validateSubmitButton();
                } else {
                    desableBottles();
                    showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<DateTimeModel> call, Throwable t) {
                showSnackBar(t.getMessage());
                desableBottles();
            }
        });
    }

    public void updateEarningPoint(UserEarningModel earningModel) {
        if (earningModel == null) return;
        totalEarningPoints = earningModel.getEarnedValue();
        earnedTV.setText("Total points earned - " + earningModel.getEarnedValue());
        if (!earningModel.isBottleEnabled()) {
            bottle1LL.setClickable(false);
            bottle1LL.setEnabled(false);
            bottle1LL.setAlpha(0.5f);

            bottle2LL.setClickable(false);
            bottle2LL.setEnabled(false);
            bottle2LL.setAlpha(0.5f);

            bottle3LL.setClickable(false);
            bottle3LL.setEnabled(false);
            bottle3LL.setAlpha(0.5f);

            bottle4LL.setClickable(false);
            bottle4LL.setEnabled(false);
            bottle4LL.setAlpha(0.5f);

        } else {
            bottle1LL.setClickable(true);
            bottle1LL.setEnabled(true);
            bottle1LL.setAlpha(1.0f);

            bottle2LL.setClickable(true);
            bottle2LL.setEnabled(true);
            bottle2LL.setAlpha(1.0f);

            bottle3LL.setClickable(true);
            bottle3LL.setEnabled(true);
            bottle3LL.setAlpha(1.0f);

            bottle4LL.setClickable(true);
            bottle4LL.setEnabled(true);
            bottle4LL.setAlpha(1.0f);
        }

        if (earningModel.getEarnedValue() < BOTTLE1_POINT) {
            bottle1LL.setClickable(false);
            bottle1LL.setEnabled(false);
            bottle1LL.setAlpha(0.5f);
        }
        if (earningModel.getEarnedValue() < BOTTLE2_POINT) {
            bottle2LL.setClickable(false);
            bottle2LL.setEnabled(false);
            bottle2LL.setAlpha(0.5f);
        }

        if (earningModel.getEarnedValue() < BOTTLE3_POINT) {
            bottle3LL.setClickable(false);
            bottle3LL.setEnabled(false);
            bottle3LL.setAlpha(0.5f);
        }
        if (earningModel.getEarnedValue() < BOTTLE4_POINT) {
            bottle4LL.setClickable(false);
            bottle4LL.setEnabled(false);
            bottle4LL.setAlpha(0.5f);
        }
    }

    public void savePurchasedBottle(final int bottleType) {
        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        showLoading();
        PurchaseRequest purchaseRequest = getPurchaseRequestData(bottleType);
        if (purchaseRequest != null) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<Void> call = bottleApiInterface.purchaseBottle(purchaseRequest);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        updateUserEarningPoints(bottleType);
                    } else {
                        hideLoading();
                        showSnackBar(response.message());
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    showSnackBar(t.getMessage());
                    hideLoading();
                }
            });
        }
    }

    private PurchaseRequest getPurchaseRequestData(int bottleType) {

        PurchaseRequest purchaseRequest = null;
        List<PurchasedBottle> purchasedBottles = new ArrayList<PurchasedBottle>();

        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        if (bottleType == BOTTLE_TYPE_1) {
            purchasedBottles.add(new PurchasedBottle("5ad5da7abb7ab30012140809", 1, 330, "Silver", 300));
        } else if (bottleType == BOTTLE_TYPE_2) {
            purchasedBottles.add(new PurchasedBottle("5ad5dd0abb7ab3001214080b", 1, 30, "Silver", 300));
        }
        if (bottleType == BOTTLE_TYPE_3) {
            purchasedBottles.add(new PurchasedBottle("5ad5de0cbb7ab3001214080d", 1, 30, "Silver", 300));
        } else if (bottleType == BOTTLE_TYPE_4) {
            purchasedBottles.add(new PurchasedBottle("5ad5e2ddbb7ab3001214080f", 1, 200, "Silver", 500));
        }
        purchaseRequest = new PurchaseRequest(userId, purchasedBottles, "", true, totalEarningPoints);
        return purchaseRequest;
    }

    public void updateUserEarningPoints(final int bottleType) {
        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            hideLoading();
            return;
        }
        UserEarningModel userEarningModel = new UserEarningModel();
        if (bottleType == BOTTLE_TYPE_1) {
            userEarningModel.setRedeemPoints(BOTTLE1_POINT);
        } else if (bottleType == BOTTLE_TYPE_2) {
            userEarningModel.setRedeemPoints(BOTTLE2_POINT);
        } else if (bottleType == BOTTLE_TYPE_3) {
            userEarningModel.setRedeemPoints(BOTTLE3_POINT);
        } else if (bottleType == BOTTLE_TYPE_4) {
            userEarningModel.setRedeemPoints(BOTTLE4_POINT);
        }

        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        userEarningModel.setUserId(userId);

        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        Call<UserEarningModel> apiCall = iplApiInterface.updateUserEarningPoints(userEarningModel);

        apiCall.enqueue(new Callback<UserEarningModel>() {
            @Override
            public void onResponse(Call<UserEarningModel> call, Response<UserEarningModel> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    UserEarningModel earningModel = response.body();
                    if (earningModel == null) return;
                    updateEarningPoint(earningModel);
                    saveIPLRedeemedBottle(bottleType);
                    startActivity(new Intent(IPLMatchDetalsActivity.this, PurchasedMessageActivity.class));
                } else {
                    showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<UserEarningModel> call, Throwable t) {
                hideLoading();
                showSnackBar(t.getMessage());
            }
        });
    }

    private void desableBottles() {
        bottle1LL.setClickable(false);
        bottle1LL.setEnabled(false);
        bottle1LL.setAlpha(0.5f);

        bottle2LL.setClickable(false);
        bottle2LL.setEnabled(false);
        bottle2LL.setAlpha(0.5f);

        bottle3LL.setClickable(false);
        bottle3LL.setEnabled(false);
        bottle3LL.setAlpha(0.5f);

        bottle4LL.setClickable(false);
        bottle4LL.setEnabled(false);
        bottle4LL.setAlpha(0.5f);
    }

    public void alert(String positiveButton, String negativeButton, final int bottleType) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.msg_redeem));
        builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                savePurchasedBottle(bottleType);

            }
        });
        builder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void saveIPLRedeemedBottle(final int bottleType) {
        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        PurchaseRequest purchaseRequest = getPurchaseRequestData(bottleType);
        if (purchaseRequest != null) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<Void> call = bottleApiInterface.saveRedeemedBottle(purchaseRequest);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {

                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }
    }

    private int getAns1Possition() {
        int pos = 0;
        if (teamArrayList == null || userAnsModel == null) return pos;

        for (int i = 0; i < teamArrayList.size(); i++) {
            TeamModel teamModel = teamArrayList.get(i);
            if (teamModel == null) continue;
            if (!TextUtils.isEmpty(teamModel.getTeamId()) && teamModel.getTeamId().equalsIgnoreCase(userAnsModel.getAnsOne())) {
                pos = i;
                break;
            }
        }

        return pos;
    }

    private int getAns2Possition() {
        int pos = 0;
        if (team1PlayerList == null || team2PlayerList == null || userAnsModel == null) return pos;

        for (int i = 0; i < team1PlayerList.size(); i++) {
            PlayerModel playerModel = team1PlayerList.get(i);
            if (playerModel == null) continue;
            if (!TextUtils.isEmpty(playerModel.getId()) && playerModel.getId().equalsIgnoreCase(userAnsModel.getAnsTwo())) {
                pos = i;
                break;
            }
        }

        for (int i = 0; i < team2PlayerList.size(); i++) {
            PlayerModel playerModel = team2PlayerList.get(i);
            if (playerModel == null) continue;
            if (!TextUtils.isEmpty(playerModel.getId()) && playerModel.getId().equalsIgnoreCase(userAnsModel.getAnsTwo())) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    private int getAns3Possition() {
        int pos = 0;
        if (team1PlayerList == null || team2PlayerList == null || userAnsModel == null) return pos;

        for (int i = 0; i < team1PlayerList.size(); i++) {
            PlayerModel playerModel = team1PlayerList.get(i);
            if (playerModel == null) continue;
            if (!TextUtils.isEmpty(playerModel.getId()) && playerModel.getId().equalsIgnoreCase(userAnsModel.getAnsThree())) {
                pos = i;
                break;
            }
        }

        for (int i = 0; i < team2PlayerList.size(); i++) {
            PlayerModel playerModel = team2PlayerList.get(i);
            if (playerModel == null) continue;
            if (!TextUtils.isEmpty(playerModel.getId()) && playerModel.getId().equalsIgnoreCase(userAnsModel.getAnsThree())) {
                pos = i;
                break;
            }
        }
        return pos;
    }
}
