package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;
import com.mobile.bvault.model.DrinkType;

import java.util.List;

/**
 * Created by diptif on 18/08/17.
 */

public interface DrinkTypeView extends BaseView{

    /**
     * Indicate drink type successfully fetched from the server
     * @param drinkTypes list of drink type
     */
    void onSuccess(List<DrinkType> drinkTypes);

    /**
     * Indicate error occurred while fetching the type from server
     * @param message error message
     */
    void onFailure(String message);

}
