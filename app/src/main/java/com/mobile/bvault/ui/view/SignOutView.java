package com.mobile.bvault.ui.view;

/**
 * Created by diptif on 28/09/17.
 */

public interface SignOutView {

    void onSuccessfulSignOut();

    void onSignOutFailure(String message);
}
