package com.mobile.bvault.ui.anim;

import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.mobile.bvault.ui.custom_view.BottleMeterView;

/**
 * Created by diptif on 30/08/17.
 */

public class CircleAngleAnimation extends Animation {

    private BottleMeterView bottleMeterView;

    private float oldAngle;
    private float newAngle;

    public CircleAngleAnimation(BottleMeterView bottleMeterView, int newAngle) {
        this.oldAngle = bottleMeterView.getAngle();
        this.newAngle = newAngle;
        this.bottleMeterView = bottleMeterView;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = oldAngle + ((newAngle - oldAngle) * interpolatedTime);

        bottleMeterView.setAngle(angle);
        bottleMeterView.requestLayout();
    }
}
