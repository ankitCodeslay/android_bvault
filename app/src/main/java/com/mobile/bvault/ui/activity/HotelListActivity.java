package com.mobile.bvault.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.Advertisement;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.presenter.AdsPresenter;
import com.mobile.bvault.ui.adapter.HotelListAdapter;
import com.mobile.bvault.presenter.HotelListPresenter;
import com.mobile.bvault.ui.view.AdsView;
import com.mobile.bvault.ui.view.HotelListView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.LocationUtils;
import com.synnapps.carouselview.CarouselView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HotelListActivity extends BaseActivity implements HotelListView, HotelListAdapter.OnItemClickListener, AdsView, LocationUtils.OnLocationListener {

    private static final String TAG = "HotelListActivity";

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.carouselView) CarouselView carouselView;
    @BindView(R.id.ads_loading_container) LinearLayout adsContainer;
    private AdsPresenter adsPresenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_list);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(R.string.restaurant);
        initDrawer();
        setRecyclerView();
        getCurrentLocation();
        getAds();
    }

    private void getAds() {
        adsContainer.setVisibility(View.VISIBLE);
        adsPresenter = new AdsPresenter(this,this);
        adsPresenter.getAds();
    }

    private void getCurrentLocation() {
        new LocationUtils(this,this).getCurrentLocation();
    }

    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getHotelList(Location location) {
        showLoading();
        String bottleId = getIntent().getStringExtra(Constants.BOTTLE);
        if (bottleId != null)
            new HotelListPresenter(this,this).getHotelsByBottleId(location,bottleId);
        else
            new HotelListPresenter(this,this).getHotelList(location);

    }

    @Override
    public void onHotelListSuccessListener(List<Hotel> hotelList) {
        hideLoading();
        recyclerView.setAdapter(new HotelListAdapter(this,hotelList,this));
    }

    @Override
    public void onHotelListFailureListener(String message) {
        hideLoading();
        showSnackBar(message);
    }

    @Override
    public void OnItemClick(Hotel hotel) {
        CommonUtils.printErrorLog(TAG,"Hotel selected from the hotels list");
        Intent intent = new Intent(this, HotelDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.HOTEL,hotel);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.LOCATION_REQUEST_CODE) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                getHotelList(null);
            }else {
                getCurrentLocation();
            }
        }
    }

    @Override
    public void onAdsLoadedSuccessfully(List<Advertisement> advertisementList) {
        adsContainer.setVisibility(View.GONE);
        if (adsPresenter != null) adsPresenter.setAds(carouselView,advertisementList);
    }

    @Override
    public void onAdsFailure(String message) {
        adsContainer.setVisibility(View.GONE);
        showSnackBar(message);
    }

    @Override
    public void onLocationSuccess(Location location) {
        CommonUtils.printErrorLog(TAG,"Latitude: "+location.getLatitude());
        getHotelList(location);
    }

    @Override
    public void onLocationFailure() {
        CommonUtils.printErrorLog(TAG,"Latitude: null");
        getHotelList(null);
    }
}
