package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.User;

/**
 * Created by diptif on 05/10/17.
 */

public interface ProfileView {

    void setProfile(User user);

    void onProfileUpdatedSuccessfully();

    void onProfileUpdateFailure(String message);

    void onProfileImageUploadedSuccessfully(String filePath);

    void onProfileImageUploadedFailure(String errorMessage);


    void onValidationSuccess();

    void onValidationFailure(String message);

    /**
     * Indicate RefCode validation failure
     * @param message validation failure message
     */
    void onRefCodeValidationFail(String message);

    /**
     * Indicate RefCode validation successful
     */
    void onRefCodeValidationSuccess();
}
