package com.mobile.bvault.ui.activity;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;
import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.AppVersion;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.BottleListModel;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.NetworkUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mobile.bvault.utils.Constants.LOCATION_REQUEST_CODE;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = "SplashActivity";
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LatLng latLng;
    public static final int REQUEST_TAG_START_RESOLUTION_FOR_GPS = 0x01;
    public static final int UPDATE_INTERVAL = 10000; // 10 sec
    public static final int FATEST_INTERVAL = 1000; // 1 sec
    public static final int DISPLACEMENT = 10; // 10 meters

    private boolean isAppOldVersion = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setupFirebase();
        getLocation();
        getAppVersionCode();
        getAllBottles();
        getHashkey();
    }

    private void goToScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isAppOldVersion) {
                    return;
                }

                if (checkLocationPermission()) {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null) {
                        latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    }
                }
                AppPreferencesHelper.getInstance(SplashActivity.this).setLatLng(latLng);

                boolean isFirstTime = AppPreferencesHelper.getInstance(SplashActivity.this).isFirstTimeLaunch();
                if (isFirstTime) {
                    goToIntroScreen();
                } else {
                    boolean isLoggedIn = AppPreferencesHelper.getInstance(SplashActivity.this).isLoggedIn();
                    if (isLoggedIn) {
                        boolean hasMobAndDob = CommonUtils.hasMobAndDob(SplashActivity.this);
                        if (hasMobAndDob) {
                            goToHomeScreen();
                        } else {
                            goToProfileScreen();
                        }
                    } else {
                        goToLoginScreen();
                    }
                }
            }
        }, 2000);
    }

    private void goToHomeScreen() {
        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        finish();
    }

    private void goToLoginScreen() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }

    private void goToIntroScreen() {
        startActivity(new Intent(SplashActivity.this, IntroActivity.class));
        finish();
    }

    private void goToProfileScreen() {
        startActivity(new Intent(SplashActivity.this, ProfileActivity.class));
        finish();
    }

    private void setupFirebase() {
        FirebaseApp.initializeApp(getApplicationContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationManager notificationManager =
                        getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                        channelName, NotificationManager.IMPORTANCE_LOW));
            }
        }

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
    }

    /**
     * Used to fetch the current location.
     */
    private void getLocation() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkLocationPermission()) {
                buildGoogleApiClient();
            } else {
                requestLocationPermission();
            }
        } else {
            buildGoogleApiClient();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
            if (checkLocationPermission())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            else {
                requestLocationPermission();
            }
            turnGPSOn();
        } catch (Exception e) {

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                //.addApi(Places.GEO_DATA_API)
                //.addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    private void turnGPSOn() {
        if (mLocationRequest == null || mGoogleApiClient == null) {
            return;
        }
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS: {
                        try {
                            if (!checkLocationPermission()) {
                                requestLocationPermission();
                                return;
                            }
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            if (mLastLocation != null) {
                                latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                            }
                            goToScreen();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(SplashActivity.this, REQUEST_TAG_START_RESOLUTION_FOR_GPS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });
    }


    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_TAG_START_RESOLUTION_FOR_GPS) {

            if (resultCode == RESULT_OK) {

                try {
                    if (!checkLocationPermission()) {
                        requestLocationPermission();
                        return;
                    }
                    buildGoogleApiClient();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                goToScreen();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mGoogleApiClient == null) {
                    buildGoogleApiClient();
                }
            } else {
                goToScreen();
            }
        }
    }

    public void getHashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.i("Base64", Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Name not found", e.getMessage(), e);

        } catch (NoSuchAlgorithmException e) {
            Log.d("Error", e.getMessage(), e);
        }
    }

    public void getAllBottles() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }

        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<List<Bottle>> apiCall = bottleApiInterface.getAllBottles();
        apiCall.enqueue(new Callback<List<Bottle>>() {
            @Override
            public void onResponse(Call<List<Bottle>> call, Response<List<Bottle>> response) {
                if (response.isSuccessful()) {
                    List<Bottle> bottleList = response.body();
                    BottleListModel.getInstance().setBottleList(bottleList);
                } else {

                }
            }

            @Override
            public void onFailure(Call<List<Bottle>> call, Throwable t) {

            }
        });
    }

    public void getAppVersionCode() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }

        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<AppVersion> apiCall = userApiInterface.getAppVersionCode();
        apiCall.enqueue(new Callback<AppVersion>() {
            @Override
            public void onResponse(Call<AppVersion> call, Response<AppVersion> response) {
                if (response.isSuccessful()) {
                    AppVersion appVersion = response.body();
                    if (appVersion == null) return;
                    PackageInfo pInfo = null;
                    try {
                        pInfo = SplashActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                        if (pInfo != null) {
                            int verCode = pInfo.versionCode;
                            if (verCode < appVersion.getVersionCode()) {
                                isAppOldVersion = true;
                                if (!isFinishing()) {
                                    alert(SplashActivity.this, "New Version Found!!", appVersion.getMessage(), getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), false, true);
                                }
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AppVersion> call, Throwable t) {

            }
        });
    }

    public void alert(Context context, String title, String message, String positiveButton, String negativeButton, boolean isNegativeButton, boolean isTitle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (isTitle) {
            builder.setTitle(title);
        }

        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                toOpenPlayStore();
                finish();
            }
        });
        if (isNegativeButton) {
            builder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    finish();
                }
            });
        }
        builder.show();
    }
}
