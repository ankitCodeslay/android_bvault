package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.PromoCodeModel;
import com.mobile.bvault.model.RefPromoCodeModel;
import com.mobile.bvault.presenter.PromoCodePresenter;
import com.mobile.bvault.ui.adapter.ApplyPromoCodeAdapter;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 01/11/17.
 */

public class PromoCodeActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.invalidPromoCodeTV)
    TextView invalidPromoCodeTV;
    @BindView(R.id.et_promocode)
    EditText etPromocode;
    @BindView(R.id.lblOfferLL)
    LinearLayout lblOfferLL;
    private PromoCodePresenter promoCodePresenter;

    private LinearLayoutManager layoutManager;
    private ApplyPromoCodeAdapter adapter;
    private List<PromoCodeModel> promoCodeList;

    private double totalAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_code);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.apply_promocode));
        initDrawer();
        initViews();
    }

    private void initViews() {
        if (getIntent().getExtras() != null) {
            totalAmount = getIntent().getDoubleExtra("totalAmount", 0);
        }
        setupRecyclerView();
        getUserEarningHistory();
    }

    private void setupRecyclerView() {
        promoCodeList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ApplyPromoCodeAdapter(this, promoCodeList);
        recyclerView.setAdapter(adapter);
    }

    public void getUserEarningHistory() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        showLoading();
        String userId = AppPreferencesHelper.getInstance(PromoCodeActivity.this).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<List<PromoCodeModel>> apiCall = userApiInterface.getUserEarningHistory(userId);
        apiCall.enqueue(new Callback<List<PromoCodeModel>>() {
            @Override
            public void onResponse(Call<List<PromoCodeModel>> call, Response<List<PromoCodeModel>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    List<PromoCodeModel> promoCodeModelList = response.body();
                    updatePromoCodeList(promoCodeModelList);
                }
            }

            @Override
            public void onFailure(Call<List<PromoCodeModel>> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void updatePromoCodeList(List<PromoCodeModel> promoCodeModelList) {

        List<PromoCodeModel> unUsedPromoCodeList = new ArrayList<>();
        if (promoCodeModelList != null) {
            for (int i = 0; i < promoCodeModelList.size(); i++) {
                PromoCodeModel promoCodeModel = promoCodeModelList.get(i);
                if (promoCodeModel == null) continue;

                String expiryDate = promoCodeModel.getExpiryDate();
                if (!TextUtils.isEmpty(expiryDate)) {
                    expiryDate = expiryDate.substring(0, 10);
                }
                boolean isExpired = isPromoCodeExpired(expiryDate);
                if (promoCodeModel.getUsedStatus() == 0 && !isExpired) {
                    unUsedPromoCodeList.add(promoCodeModel);
                }
            }
        }
        promoCodeList.addAll(unUsedPromoCodeList);
        adapter.notifyDataSetChanged();
        if (promoCodeList.isEmpty()) {
            lblOfferLL.setVisibility(View.GONE);
        } else {
            lblOfferLL.setVisibility(View.VISIBLE);
        }
    }

    private boolean isPromoCodeExpired(String expiryDate) {
        boolean isExpired = false;
        if (TextUtils.isEmpty(expiryDate)) return isExpired;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentTime = null;
        try {
            String dateTimeStr = expiryDate;
            Date endTime = dateFormat.parse(dateTimeStr);
            currentTime = dateFormat.parse(dateFormat.format(new Date()));

            if (currentTime.after(endTime)) {
                isExpired = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isExpired;
    }

    @OnClick(R.id.btn_apply_promocode)
    public void applyPromocode() {
        invalidPromoCodeTV.setText("");
        String refCode = etPromocode.getText().toString();
        if (TextUtils.isEmpty(refCode)) {
            invalidPromoCodeTV.setText("Please enter promocode.");
            return;
        }
        showLoading();
        applyRefPromoCode2(refCode);
    }

    public void applyPromCode(String referralCode) {
        applyRefPromoCode(referralCode);
    }

    public void applyRefPromoCode(final String code) {

        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        showLoading();
        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        if (userId != null && !userId.isEmpty()) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            RefPromoCodeModel promoCodeModel = new RefPromoCodeModel();
            promoCodeModel.setUserId(userId);
            promoCodeModel.setPromoCode(code);
            Call<RefPromoCodeModel> promoCodeCall = bottleApiInterface.applyRefPromoCode(promoCodeModel);
            promoCodeCall.enqueue(new Callback<RefPromoCodeModel>() {
                @Override
                public void onResponse(Call<RefPromoCodeModel> call, Response<RefPromoCodeModel> response) {
                    hideLoading();
                    if (response.isSuccessful()) {
                        RefPromoCodeModel promoCodeModel = response.body();

                        if (promoCodeModel.isValid()) {
                            PromoCode promoCode = new PromoCode();
                            promoCode.setPromoCodeId("");
                            promoCode.setPromoCode(code);
                            promoCode.setDiscount(promoCodeModel.getDiscount());
                            if (totalAmount < promoCodeModel.getDiscount()) {
                                invalidPromoCodeTV.setText("To apply this promo-code, Minimum purchase amount should be greater than " + promoCode.getDiscount());
                                return;
                            }
                            Intent intent = new Intent();
                            intent.putExtra("promoCode", promoCode);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            invalidPromoCodeTV.setText("This Promocode has been expired.");
                        }
                    } else {
                        showSnackBar(response.message());
                    }
                }

                @Override
                public void onFailure(Call<RefPromoCodeModel> call, Throwable t) {
                    showSnackBar(t.getMessage());
                }
            });

        } else {
            hideLoading();
            showSnackBar("User is Logged Out");
        }
    }

    public void applyRefPromoCode2(final String code) {

        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(this).getUserId();

        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        RefPromoCodeModel promoCodeModel = new RefPromoCodeModel();
        promoCodeModel.setUserId(userId);
        promoCodeModel.setPromoCode(code);
        Call<RefPromoCodeModel> promoCodeCall = bottleApiInterface.applyRefPromoCode(promoCodeModel);
        promoCodeCall.enqueue(new Callback<RefPromoCodeModel>() {
            @Override
            public void onResponse(Call<RefPromoCodeModel> call, Response<RefPromoCodeModel> response) {
                if (response.isSuccessful()) {
                    RefPromoCodeModel promoCodeModel = response.body();

                    if (promoCodeModel.isValid()) {
                        PromoCode promoCode = new PromoCode();
                        promoCode.setPromoCodeId("");
                        promoCode.setPromoCode(code);
                        promoCode.setDiscount(promoCodeModel.getDiscount());
                        if (totalAmount < promoCodeModel.getDiscount()) {
                            invalidPromoCodeTV.setText("To apply this promo-code, Minimum purchase amount should be greater than " + promoCode.getDiscount());
                            return;
                        }
                        Intent intent = new Intent();
                        intent.putExtra("promoCode", promoCode);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        applyCode(code);
                    }
                } else {
                    applyCode(code);
                }
            }

            @Override
            public void onFailure(Call<RefPromoCodeModel> call, Throwable t) {
                applyCode(code);
            }
        });
    }

    public void applyCode(String code) {

        if (!NetworkUtils.isNetworkConnected(this)) {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }

        String userId = AppPreferencesHelper.getInstance(this).getUserId();
        BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
        Call<PromoCode> promoCodeCall = bottleApiInterface.applyPromoCode(code, userId);
        promoCodeCall.enqueue(new Callback<PromoCode>() {
            @Override
            public void onResponse(Call<PromoCode> call, Response<PromoCode> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    PromoCode promoCode = response.body();
                    if (promoCode == null) return;
                    if (totalAmount < promoCode.getMinAmountForDiscount()) {
                        invalidPromoCodeTV.setText("To apply this promo-code, Minimum purchase amount should be greater than " + promoCode.getMinAmountForDiscount());
                        return;
                    }
                    Intent intent = new Intent();
                    intent.putExtra("promoCode", promoCode);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    try {
                        String message = new JSONObject(response.errorBody().string()).getString(Constants.MESSAGE);
                        invalidPromoCodeTV.setText(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<PromoCode> call, Throwable t) {
                hideLoading();
            }
        });
    }

}
