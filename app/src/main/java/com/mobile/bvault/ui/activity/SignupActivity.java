package com.mobile.bvault.ui.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.presenter.SignupPresenter;
import com.mobile.bvault.ui.view.SignupView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.Helper;
import com.mobile.bvault.utils.NetworkUtils;
import com.mobile.bvault.utils.TimeUtils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobile.bvault.utils.Constants.RESET_LIMIT;
import static com.mobile.bvault.utils.Constants.longOneSecond;
import static com.mobile.bvault.utils.Constants.longTimeAfterWhichButtonEnable;
import static com.mobile.bvault.utils.Constants.longTotalVerificationTime;

public class SignupActivity extends BaseActivity implements SignupView, View.OnClickListener {

    private static final String TAG = "SignupActivity";

    @BindView(R.id.et_username)
    EditText userNameEditText;
    @BindView(R.id.et_password)
    EditText passEditText;
    @BindView(R.id.et_phone_number)
    EditText phoneNumberEditText;
    @BindView(R.id.et_email)
    EditText emailEditText;
    @BindView(R.id.tv_dob)
    TextView etDOB;
    @BindView(R.id.terms_condition_text_view)
    TextView termsConditionTextView;
    @BindView(R.id.referral_code_text_view)
    TextView referralCodeTextView;
    @BindView(R.id.appliedRefCodeIV)
    ImageView appliedRefCodeIV;

    SignupPresenter signupPresenter;

    Dialog referralCodeDialog;
    EditText etReferralCode;
    Button cancelButton;
    Button applyButton;

    Dialog otpDialog;
    EditText editOtp;
    Button cancelOtpBtn;
    Button submitOtpBtn;
    TextView resendOtpTv;
    TextView timerTV;

    public static Runnable r;
    public static Handler handler;
    public String textMobileNo;
    private int otp;
    static Handler handlerButtonVisibility;
    public static Runnable rButtonVisibility;
    private CountDownTimer countDownTimer;
    private int intNumberOfTimesResendPress = 0;

    private String referralCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        initPresenter();
        initRefCodeDialog();
        initOtpDialog();
    }

    private void initPresenter() {
        signupPresenter = new SignupPresenter(this, this);
    }

    private void initRefCodeDialog() {
        referralCodeDialog = new Dialog(this);
        referralCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralCodeDialog.setContentView(R.layout.view_referral_code);
        referralCodeDialog.setCanceledOnTouchOutside(false);
        etReferralCode = referralCodeDialog.findViewById(R.id.et_referral_code);
        applyButton = referralCodeDialog.findViewById(R.id.btn_apply);
        applyButton.setOnClickListener(this);
        cancelButton = referralCodeDialog.findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(this);
    }

    private void initOtpDialog() {
        otpDialog = new Dialog(this);
        otpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        otpDialog.setContentView(R.layout.view_otp_validation);
        otpDialog.setCanceledOnTouchOutside(false);
        otpDialog.setCancelable(false);
        editOtp = otpDialog.findViewById(R.id.edit_otp);
        submitOtpBtn = otpDialog.findViewById(R.id.btn_otp_submit);
        submitOtpBtn.setOnClickListener(this);
        cancelOtpBtn = otpDialog.findViewById(R.id.btn_otp_cancel);
        cancelOtpBtn.setOnClickListener(this);
        resendOtpTv = otpDialog.findViewById(R.id.resend_otp_tv);
        resendOtpTv.setOnClickListener(this);
        timerTV = otpDialog.findViewById(R.id.timerTV);
    }

    @OnClick(R.id.referral_code_text_view)
    void showReferralCodeDialog() {
        showRefCodeDialog();
    }

    @OnClick(R.id.btn_sign_up)
    @Override
    public void singUP() {
        if (signupPresenter != null) {
            String username = userNameEditText.getText().toString().trim();
            String password = passEditText.getText().toString();
            String email = emailEditText.getText().toString().trim();
            String phoneNumber = phoneNumberEditText.getText().toString().trim();
            String dob = etDOB.getText().toString().trim();
            signupPresenter.validateUser(username, email, password, phoneNumber, dob);
        }
    }

    @OnClick(R.id.terms_condition_text_view)
    void openTermsAndCondition() {
        CommonUtils.showTermsAndCondition(this);
    }

    @Override
    public void goToSignIn() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void onSignupSuccess(User user) {
        hideLoading();
        showToastMessage("Successfully signed up");
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onSignupFailure(String message) {
        hideLoading();
        showSnackBar(message);
    }

    @Override
    public void onValidationFail(String message) {
        showSnackBar(message);
    }

    @Override
    public void onValidationSuccess() {
        sendOtpTask();
        showOtpDialog();
    }

    @Override
    public void onRefCodeValidationFail(String message) {
        hideLoading();
        hideRefCodeDialog();
        showSnackBar(message);
        appliedRefCodeIV.setVisibility(View.GONE);
        referralCodeTextView.setText(getString(R.string.have_referral_code));

    }

    @Override
    public void onRefCodeValidationSuccess() {
        hideLoading();
        hideRefCodeDialog();
        appliedRefCodeIV.setVisibility(View.VISIBLE);
        showSnackBar(getString(R.string.messageRefCodeSuc));
        referralCodeTextView.setText("Referral code " + referralCode + " applied.");
    }

    @OnClick(R.id.tv_dob)
    void selectDob() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                String date = TimeUtils.getFormattedDob(calendar.getTime());
                etDOB.setText(date);
            }
        }, year, month, day);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        dialog.show();
    }

    void showRefCodeDialog() {
        if (referralCodeDialog != null) {
            referralCodeDialog.show();
            etReferralCode.setText("");
        }
    }

    void hideRefCodeDialog() {
        if (referralCodeDialog != null)
            referralCodeDialog.dismiss();
    }

    void showOtpDialog() {
        if (otpDialog != null) {
            otpDialog.show();
            editOtp.setText("");
        }
    }

    void hideOtpDialog() {
        if (otpDialog != null)
            otpDialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                hideRefCodeDialog();
                break;
            case R.id.btn_apply:
                applyReferralCode();
                break;
            case R.id.btn_otp_cancel:
                cancelHandler();
                hideOtpDialog();
                break;
            case R.id.btn_otp_submit:
                validateOtp();
                break;
            case R.id.resend_otp_tv: {
                intNumberOfTimesResendPress = intNumberOfTimesResendPress + 1;
                if (intNumberOfTimesResendPress <= RESET_LIMIT) {
                    sendOtpTask();
                }
                break;
            }
        }
    }

    void applyReferralCode() {
        referralCode = etReferralCode.getText().toString().trim();
        if (referralCode.isEmpty()) {
            showToastMessage("Please enter referral code");
        } else {
            if (signupPresenter != null) {
                showLoading();
                signupPresenter.validateReferralCode(referralCode);
            }
        }
    }


    public void sendOtpTask() {

        textMobileNo = phoneNumberEditText.getText().toString();
        handler = new Handler();
        if (otp == 0) {
            otp = Helper.randomNumberCreation(100000, 999999);
        }
        AppPreferencesHelper.getInstance(this).setOtp(otp);
        if (NetworkUtils.isNetworkConnected(this)) {
            resendOtpTv.setClickable(false);
            resendOtpTv.setAlpha(0.5f);
            functionEnablingButtons();
            functionInitializingCountDownTimer();
            signupPresenter.sendOtp(textMobileNo, otp + "");
        } else {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
        }
    }

    public void functionEnablingButtons() {
        rButtonVisibility = new Runnable() {
            @Override
            public void run() {
                resendOtpTv.setClickable(true);
                resendOtpTv.setAlpha(1);
            }
        };
        handlerButtonVisibility = new Handler();
        handlerButtonVisibility.postDelayed(rButtonVisibility, longTimeAfterWhichButtonEnable);
    }

    public void functionInitializingCountDownTimer() {
        timerTV.setVisibility(View.VISIBLE);
        countDownTimer = new CountDownTimer(longTotalVerificationTime, longOneSecond) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerTV.setVisibility(View.VISIBLE);
                int remainingIntTime = (int) millisUntilFinished / 1000;
                String remainingTime = "";
                if (remainingIntTime < 10) {
                    remainingTime = "0" + remainingIntTime;
                } else {
                    remainingTime = "" + remainingIntTime;
                }
                timerTV.setText("00:" + remainingTime);
            }

            @Override
            public void onFinish() {
                timerTV.setVisibility(View.INVISIBLE);
            }
        }.start();
    }

    void validateOtp() {
        String inputOtp = editOtp.getText().toString().trim();
        if (inputOtp.isEmpty()) {
            showToastMessage(getString(R.string.emptyOtp));
        } else {
            int oldOtp = AppPreferencesHelper.getInstance(this).getOtp();
            if (oldOtp == Integer.parseInt(inputOtp)) {
                showToastMessage(getString(R.string.messageNoValidated));
                if (signupPresenter != null) {
                    cancelHandler();
                    hideOtpDialog();
                    showLoading();
                    String name = userNameEditText.getText().toString().trim();
                    String[] nameArray = CommonUtils.getFirstNameAndLastName(name);
                    String password = passEditText.getText().toString();
                    String email = emailEditText.getText().toString().trim();
                    String username = email.split("@")[0];
                    String phoneNumber = phoneNumberEditText.getText().toString().trim();
                    String dob = etDOB.getText().toString().trim();
                    if (referralCode == null) referralCode = "";
                    signupPresenter.signup(nameArray[0], nameArray[1], email, username, password, phoneNumber, dob, false, referralCode);
                }
            } else {
                showToastMessage(getString(R.string.messageOtpInvalid));
                editOtp.setText("");
            }
        }
    }

    public void cancelHandler() {
        if (handlerButtonVisibility == null || countDownTimer == null || rButtonVisibility == null)
            return;
        handlerButtonVisibility.removeCallbacks(rButtonVisibility);
        countDownTimer.cancel();
    }
}
