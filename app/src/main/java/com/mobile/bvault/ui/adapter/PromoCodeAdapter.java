package com.mobile.bvault.ui.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.mobile.bvault.model.PromoCodeModel;
import com.mobile.bvault.model.User;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.DateTimeUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.ViewHolder> {

    private static final String TAG = "PromoCodeModelAdapter";
    private Context context;
    private List<PromoCodeModel> promoCodeList;
    private int type;

    public PromoCodeAdapter(Context context, List<PromoCodeModel> promoCodeList, int type) {
        this.context = context;
        this.promoCodeList = promoCodeList;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_promo_code, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PromoCodeModel promoCodeModel = promoCodeList.get(position);
        if (promoCodeModel == null) return;
        holder.promoCodeTextView.setText(promoCodeModel.getPromoCode());
        holder.amtTextView.setText(context.getString(R.string.rs_symb) + " " + promoCodeModel.getPromoCodeAmt());
        if (!TextUtils.isEmpty(promoCodeModel.getDateTime()) && promoCodeModel.getDateTime().length() > 10) {
            holder.dateTextView.setText("Expiry on-\n" + DateTimeUtils.convertDate(promoCodeModel.getExpiryDate().substring(0, 10)));
        }
        if (!TextUtils.isEmpty(promoCodeModel.getReferType())) {
            if (promoCodeModel.getReferType().equalsIgnoreCase("to")) {
                holder.firstTnxTV.setVisibility(View.VISIBLE);
                User user = promoCodeModel.getReferByUser();
                if (user != null) {
                    holder.descTextView.setText("Referred By-\n" + user.getDisplayName());
                }
            } else if (promoCodeModel.getReferType().equalsIgnoreCase("by")) {
                holder.firstTnxTV.setVisibility(View.GONE);
                User user = promoCodeModel.getReferToUser();
                if (user != null) {
                    holder.descTextView.setText("Referred To-\n" + user.getDisplayName());
                }
            }
        }
        final PromoCodeModel promoCodeModel1 = promoCodeModel;
        holder.promoCodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    copyPromoCode(promoCodeModel1.getPromoCode());
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(this.promoCodeList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.promocode_text_view)
        TextView promoCodeTextView;
        @BindView(R.id.description_text_view)
        TextView descTextView;

        @BindView(R.id.amount_text_view)
        TextView amtTextView;
        @BindView(R.id.date_text_view)
        TextView dateTextView;
        @BindView(R.id.firstTnxTV)
        TextView firstTnxTV;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PromoCodeModel PromoCodeModel);
    }

    public void copyPromoCode(String code) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", code);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, "Coppied to Clipboard!!", Toast.LENGTH_SHORT).show();
    }

}
