package com.mobile.bvault.ui.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.UserApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.presenter.ContactUsPresenter;
import com.mobile.bvault.ui.view.ContactUsView;
import com.mobile.bvault.utils.NetworkUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diptif on 01/11/17.
 */

public class ReferAndEarnActivity extends BaseActivity {

    @BindView(R.id.view_earn_history_container)
    LinearLayout viewEarnHistoryContainer;
    @BindView(R.id.btn_referral)
    Button btnReferral;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_and_earn);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.refer));
        initDrawer();
        initViews();
    }

    @OnClick(R.id.view_earn_history_container)
    public void toOpenEarningHistory() {
        startActivity(new Intent(ReferAndEarnActivity.this, EarnHistoryActivity.class));
    }

    @OnClick(R.id.tc_container)
    public void toOpenTC() {
        startActivity(new Intent(ReferAndEarnActivity.this, ReferAndEarnTCActivity.class));
    }

    private void initViews() {
        String code = AppPreferencesHelper.getInstance(ReferAndEarnActivity.this).getUserReferralCode();
        if (TextUtils.isEmpty(code)) {
            getUserReferralCode();
        } else {
            btnReferral.setText(code);
        }
    }

    public void getUserReferralCode() {

        if (!NetworkUtils.isNetworkConnected(this)) {
            return;
        }
        String userId = AppPreferencesHelper.getInstance(ReferAndEarnActivity.this).getUserId();
        UserApiInterface userApiInterface = ApiClient.getClient().create(UserApiInterface.class);
        Call<User> apiCall = userApiInterface.getUserReferralCode(userId);
        apiCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    if (user == null) return;
                    AppPreferencesHelper.getInstance(ReferAndEarnActivity.this).setUserReferralCode(user.getReferralCode());
                    btnReferral.setText(user.getReferralCode());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.btn_referral)
    public void copyReferralCode() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", btnReferral.getText().toString());
        clipboard.setPrimaryClip(clip);
        showToastMessage("Coppied to Clipboard!!");
    }

    @OnClick(R.id.btn_invite_frinds)
    public void shareReferralCode() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Use my referral code " + btnReferral.getText().toString() + " to download the BVault app and buy BVault Bonds to get sweeter deals on beverages in top restaurants! Download the app now! http://onelink.to/6zsbux");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
