package com.mobile.bvault.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mobile.bvault.R;
import com.mobile.bvault.ui.anim.CircleAngleAnimation;
import com.mobile.bvault.ui.custom_view.BottleMeterView;

public class RackDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rack_detail);

        BottleMeterView bottleMeterView = (BottleMeterView) findViewById(R.id.circle);

        int angle = (int) (3.6 * 30);
        CircleAngleAnimation animation = new CircleAngleAnimation(bottleMeterView, angle);
        animation.setDuration(1000);
        bottleMeterView.startAnimation(animation);
    }
}
