package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.User;

/**
 * Created by diptif on 04/09/17.
 */

public interface SignupView {

    /**
     *  Sign up the user
     */
    void singUP();

    /**
     * Show the sign page
     */
    void goToSignIn();

    /**
     *  Indicate user successfully signed up
     */
    void onSignupSuccess(User user);

    /**
     * Indicate sign up failure
     * @param message Login failure message
     */
    void onSignupFailure(String message);

    /**
     * Indicate validation failure
     * @param message validation failure message
     */
    void onValidationFail(String message);

    /**
     * Indicate validation successful
     */
    void onValidationSuccess();

    /**
     * Indicate RefCode validation failure
     * @param message validation failure message
     */
    void onRefCodeValidationFail(String message);

    /**
     * Indicate RefCode validation successful
     */
    void onRefCodeValidationSuccess();
}
