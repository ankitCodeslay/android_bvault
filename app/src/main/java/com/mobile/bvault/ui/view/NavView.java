package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.NavOption;

import java.util.List;

/**
 * Created by diptif on 18/09/17.
 */

public interface NavView {

    void setOptions(List<NavOption> navOptionList);
}
