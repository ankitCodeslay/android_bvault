package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.Price;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 21/08/17.
 */

public class PriceAdapter extends RecyclerView.Adapter<PriceAdapter.ViewHolder> {

    private static final String TAG = "PriceAdapter";

    private Context context;
    private List<Price> prices;
    private OnItemClickListener onItemClickListener;
    Price selectedPrice = null;
    private String unit = "";

    public PriceAdapter(Context context, List<Price> prices,String unit,Price selectedPrice,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.prices = prices;
        this.unit = unit;
        this.selectedPrice = selectedPrice;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public PriceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_price,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PriceAdapter.ViewHolder holder, int position) {
        Price price = prices.get(position);
        if (price.equals(selectedPrice)) {
            holder.priceCheckBox.setChecked(true);
        }else {
            holder.priceCheckBox.setChecked(false);
        }
        holder.priceCheckBox.setText(price.getLabel() + " Bond ("+ price.getVolume()+" "+ unit +")");
        holder.priceTextView.setText("\u20B9 "+CommonUtils.commaFormatter(price.getPrice()));
        holder.onClick(prices.get(position),onItemClickListener);
        holder.onChecked(prices.get(position),onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(this.prices);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.price_check_box) CheckBox priceCheckBox;
        @BindView(R.id.price_text_view) TextView priceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void onClick(final Price price, final OnItemClickListener onItemClick){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setChecked(price, onItemClick);
                }
            });
        }

        public void onChecked(final Price price, final OnItemClickListener onItemClickListener) {
//            priceCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                }
//            });

            priceCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setChecked(price, onItemClickListener);
                }
            });
        }

        private void setChecked(Price price, OnItemClickListener onItemClickListener) {
            priceCheckBox.setChecked(true);
            selectedPrice = price;
            onItemClickListener.OnItemClick(price);
            notifyDataSetChanged();
        }

    }



    public interface OnItemClickListener {

        void OnItemClick(Price price);
    }
}
