package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.ui.activity.DrinkTypeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 22/09/17.
 */

public class EmptyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.empty_src_image_view) ImageView emptyImageView;
    @BindView(R.id.empty_src_button) Button emptyScreenButton;
    @BindView(R.id.empty_src_text) TextView emptyMessage;

    public EmptyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

    public void onClick(final Context context){
        emptyScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) context).startActivity(new Intent(context, DrinkTypeActivity.class));
            }
        });
    }

    public static EmptyViewHolder getEmptyViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_empty,parent,false);
        return new EmptyViewHolder(view);
    }

    public void setMessage(String message) {
        emptyMessage.setText(message != null ? message : "");
    }

    public void setMessage(int resMessage) {
        emptyMessage.setText(resMessage);
    }

    public void setEmptyImageView(int resImage) {
        emptyImageView.setImageResource(resImage);
    }

    public void hideButton(boolean isHide) {
        if (isHide) emptyScreenButton.setVisibility(View.GONE);
        else emptyScreenButton.setVisibility(View.VISIBLE);
    }

}
