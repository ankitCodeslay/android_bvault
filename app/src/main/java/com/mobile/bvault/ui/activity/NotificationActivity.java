package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.Bottle;
import com.mobile.bvault.model.Payment;
import com.mobile.bvault.presenter.AcceptPaymentPresenter;
import com.mobile.bvault.presenter.BottleListPresenter;
import com.mobile.bvault.presenter.NotificationPresenter;
import com.mobile.bvault.ui.adapter.BottleListAdapter;
import com.mobile.bvault.ui.adapter.NavAdapter;
import com.mobile.bvault.ui.adapter.NotificationAdapter;
import com.mobile.bvault.ui.view.AcceptPaymentView;
import com.mobile.bvault.ui.view.BottleListView;
import com.mobile.bvault.ui.view.NotificationView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationActivity extends BaseActivity implements NotificationView,AcceptPaymentView, NotificationAdapter.OnItemClickListener{

    private static final String TAG = "NotificationActivity";

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    AcceptPaymentPresenter acceptPaymentPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(R.string.notification);
        initDrawer();
        setRecyclerView();
        getRequestedPayment();
    }

    private void getRequestedPayment() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String paymentId = bundle.getString(Constants.PAYMENT_RECEIPT_ID);
            if (paymentId != null) {
                acceptPaymentPresenter = new AcceptPaymentPresenter(this,this);
                acceptPaymentPresenter.getPaymentRequestByID(paymentId);
            }
        }

//        getAllPaymentRequest();
    }

//    private void getAllPaymentRequest() {
//        showLoading();
//        new NotificationPresenter(this,this).getRequestedPaymentList();
//    }

    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onPaymentRequestLoadedSuccessfully(List<Payment> payments) {
        hideLoading();
        recyclerView.setAdapter(new NotificationAdapter(this,payments,this));
    }

    @Override
    public void onPaymentRequestFailure(String message) {
        hideLoading();
        showToastMessage(message);
    }

    @Override
    public void OnItemClick(Payment payment) {
        acceptPaymentPresenter = new AcceptPaymentPresenter(this,this,payment);
        acceptPaymentPresenter.showDialog();
    }

    @Override
    public void onPaymentStatusChangeSuccessful(String message) {
        acceptPaymentPresenter.hideDialog();
        showToastMessage(message);
//        getAllPaymentRequest();
    }

    @Override
    public void onPaymentStatusChangeFailure(String message) {
        acceptPaymentPresenter.hideDialog();
        showToastMessage(message);
    }

    @Override
    public void onPaymentByIdSuccess(Payment payment) {
        acceptPaymentPresenter = new AcceptPaymentPresenter(this,this,payment);
        acceptPaymentPresenter.showDialog();
    }

    @Override
    public void onPaymentByIdFailure(String message) {
        //showToastMessage(message);
    }

}
