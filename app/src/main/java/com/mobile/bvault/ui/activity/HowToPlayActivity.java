package com.mobile.bvault.ui.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.pref.AppPreferencesHelper;

import java.util.List;

public class HowToPlayActivity extends BaseActivity implements
        YouTubePlayer.OnInitializedListener {

    //public static final String YOUTUBE_DEVELOPER_KEY = "AIzaSyBiiAzO1s4sCELe-8xXo_H_8O833RGWQBY";
    public static final String YOUTUBE_DEVELOPER_KEY = "AIzaSyDGxJ3fhxrzL1Gto4FG-FM8OElt6fioXuY";

    //private YouTubePlayerSupportFragment youTubePlayerFragment;
    private YouTubePlayer youTubePlayer;
    private boolean fullScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_how_to_play);
        AppPreferencesHelper.getInstance(HowToPlayActivity.this).setIplFirstLaunch(false);
        /*youTubePlayerFragment = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
        if (youTubePlayerFragment != null)
            youTubePlayerFragment.initialize(YOUTUBE_DEVELOPER_KEY, this);*/

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        youTubePlayer = player;
        if (youTubePlayer == null) return;

        youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {

            @Override
            public void onFullscreen(boolean _isFullScreen) {
                fullScreen = _isFullScreen;
            }
        });
        if (!wasRestored) {
            youTubePlayer.loadVideo("CtOvYj2RGYk");
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    public void onBackPressed() {
        if (fullScreen) {
            if (youTubePlayer != null)
                youTubePlayer.setFullscreen(false);
        } else {
            backActivity();
            super.onBackPressed();
        }
    }

    public void backActivity() {
        Intent intent = new Intent(HowToPlayActivity.this, IplMatchActivity.class);
        startActivity(intent);
        finish();
    }

    public void nextActivity(View view) {
        Intent intent = new Intent(HowToPlayActivity.this, IplMatchActivity.class);
        startActivity(intent);
        finish();
    }

    protected Boolean isActivityRunning(Class activityClass) {
        ActivityManager activityManager = (ActivityManager) getBaseContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }

        return false;
    }

}
