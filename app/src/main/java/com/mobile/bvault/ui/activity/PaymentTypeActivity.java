package com.mobile.bvault.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.paytm.pgsdk.PaytmConstants;
import com.payu.india.Payu.PayuConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mobile.bvault.utils.Constants.PAYTM_REQUEST_CODE;

/**
 * Created by diptif on 01/11/17.
 */

public class PaymentTypeActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.right_layer_container)
    public RelativeLayout right_layer_container;
    @BindView(R.id.totalAmountTV)
    public TextView totalAmountTV;
    @BindView(R.id.btnPaytm)
    public Button btnPaytm;
    @BindView(R.id.btnPayu)
    public Button btnPayu;

    private double totalAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_type);
        ButterKnife.bind(this);
        setSupportToolbar();
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setTitle(getString(R.string.payment_method));
        initDrawer();
        right_layer_container.setVisibility(View.INVISIBLE);
        btnPaytm.setOnClickListener(this);
        btnPayu.setOnClickListener(this);
        totalAmount = getIntent().getDoubleExtra("totalAmount", 0);
        totalAmountTV.setText(getString(R.string.rs_symb) + " " + String.format("%,.2f", this.totalAmount));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPaytm: {
                Intent intent = new Intent(PaymentTypeActivity.this, PaytmWalletActivity.class);
                intent.putExtra("totalAmount", totalAmount);
                startActivityForResult(intent, PAYTM_REQUEST_CODE);
                break;
            }
            case R.id.btnPayu: {
                Intent intent = new Intent(PaymentTypeActivity.this, PayUActivity.class);
                intent.putExtra("totalAmount", totalAmount);
                startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE || requestCode == PAYTM_REQUEST_CODE) {
            setResult(RESULT_OK, data);
            finish();
        }
    }
}
