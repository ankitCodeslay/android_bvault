package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.TeamModel;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;


/**
 * Created by diptif on 21/08/17.
 */

public class TeamListAdapter extends BaseAdapter {

    private static final String TAG = "IPLMatchModelListAdapter";

    private Context context;
    private List<TeamModel> teamModelList;
    private LayoutInflater mInflater;

    public TeamListAdapter(Context context, List<TeamModel> teamModelList) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.teamModelList = teamModelList;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        int size = CommonUtils.getCollectionSize(this.teamModelList);
        return size > 0 ? size : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.view_team_list, parent, false);
            holder.teamNameTV = (TextView) convertView.findViewById(R.id.teamNameTV);
            holder.teamListContainer = convertView.findViewById(R.id.team_list_container);
            holder.lineView = convertView.findViewById(R.id.lineView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIpl));
            holder.teamNameTV.setTextColor(ContextCompat.getColor(context,R.color.black));
            holder.teamNameTV.setTextSize(22);
        } else if (position == 1 || position == 3) {
            holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIplTeam1));
            holder.teamNameTV.setTextColor(ContextCompat.getColor(context,R.color.white));
            holder.teamNameTV.setTextSize(16);
        } else if (position == 2) {
            holder.teamListContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIplTeam2));
            holder.teamNameTV.setTextColor(ContextCompat.getColor(context,R.color.black));
            holder.teamNameTV.setTextSize(16);
        }
        parent.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIpl));

        holder.teamNameTV.setText(teamModelList.get(position).getTeamName());
        if (position == teamModelList.size() - 1) {
            holder.lineView.setVisibility(View.GONE);
        } else {
            holder.lineView.setVisibility(View.VISIBLE);
        }
        convertView.setTag(holder);
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public class ViewHolder {
        LinearLayout teamListContainer;
        TextView teamNameTV;
        View lineView;
    }
}
