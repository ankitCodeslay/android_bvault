package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.nfc.Tag;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.History;
import com.mobile.bvault.model.HistoryObject;
import com.mobile.bvault.model.Hotel;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 11/09/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "HistoryAdapter";

    private List<History> histories;
    private Context context;

    public HistoryAdapter(Context context) {
        this.histories = new ArrayList<History>();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_history,parent,false);
            return new ViewHolder(view);
        }else {
            return EmptyViewHolder.getEmptyViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder viewHolder = (ViewHolder) holder;
                if (viewHolder != null) {
                    setHistoryView(viewHolder,position);
                }
                break;
            case 1:
                EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
                if (emptyViewHolder != null) {
                    emptyViewHolder.setEmptyImageView(R.drawable.bottles);
                    emptyViewHolder.setMessage(R.string.history_empty_message);
                    emptyViewHolder.onClick(context);
                }
                break;
        }

    }

    @Override
    public int getItemCount() {
        int size = CommonUtils.getCollectionSize(this.histories);
        return  size > 0 ? size : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (histories != null && histories.size() > 0) {
            return 0;
        }else {
            return 1;
        }
    }

    public void addHistories(List<History> histories) {
        this.histories.addAll(histories);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.history_date_text_view) TextView dateTextView;
        @BindView(R.id.history_type_image_view) ImageView historyActionImageView;
        @BindView(R.id.history_type_text_view) TextView historyTypeTextView;
        @BindView(R.id.history_time_text_view) TextView timeTextView;
        @BindView(R.id.bottle_name) TextView bottleNameTextView;
        @BindView(R.id.purchase_type_image_view) ImageView purchaseTypeImageView;
        @BindView(R.id.purchase_type_text_view) TextView purchaseTypeTextView;
        @BindView(R.id.vol_text_view) TextView volTextView;
        @BindView(R.id.code_text_view) TextView codeTextView;
        @BindView(R.id.history_location_container) LinearLayout locationContainer;
        @BindView(R.id.hotel_name_text_view) TextView hotelNameTextView;
        @BindView(R.id.history_container) LinearLayout historyContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    private void setHistoryView(ViewHolder holder, int position) {
        try {
            History history = histories.get(position);
            HistoryObject historyObj = history.getHistoryObject();
            if (historyObj != null) {
                holder.historyContainer.setVisibility(View.VISIBLE);
                holder.historyTypeTextView.setText(history.getAction());
                holder.bottleNameTextView.setText(historyObj.getBottle() != null ? historyObj.getBottle().getName() : "");
                CommonUtils.printErrorLog(TAG,history.getDate());
                holder.dateTextView.setText(history.getCreatedDate());
                holder.purchaseTypeTextView.setText(historyObj.getPurchaseType());
                holder.codeTextView.setText(historyObj.getUniqueCode());
                CommonUtils.printErrorLog(TAG,"Position: "+position);
                if (history.getAction().equalsIgnoreCase("consumed")){
                    CommonUtils.printErrorLog("abc","this is consumed object");
                    Hotel hotel = historyObj.getHotel();
                    CommonUtils.printErrorLog(TAG, "consumed volume: "+historyObj.getConsumedVolume());
                    holder.volTextView.setText(""+historyObj.getConsumedVolume() + " "+historyObj.getBottle().getUnit());
                    holder.historyActionImageView.setImageResource(R.drawable.ic_history_type_24dp);
                    if (hotel != null) {
                        CommonUtils.printErrorLog("abc","hotel object is not null");
                        holder.locationContainer.setVisibility(View.VISIBLE);
                        holder.hotelNameTextView.setText(hotel.getName());
                    }else {
                        CommonUtils.printErrorLog("abc","hotel is null");
                        holder.locationContainer.setVisibility(View.GONE);
                    }
                }else {
                    holder.historyActionImageView.setImageResource(R.drawable.ic_history_type_24dp);
                    holder.volTextView.setText(""+historyObj.getTotalVolume() + " " +historyObj.getBottle().getUnit());
                    holder.locationContainer.setVisibility(View.GONE);
                }

                if (position >= 1) {
                    int res = TimeUtils.isEqualDate(history.getCreatedDate(),histories.get(position - 1).getCreatedDate());
                    if (res == 0) {
                        holder.dateTextView.setVisibility(View.GONE);
                    }else {
                        String formattedDate = TimeUtils.getHistoryDay(history.getCreatedDate());
                        holder.dateTextView.setVisibility(View.VISIBLE);
                        holder.dateTextView.setText(formattedDate);
                    }
                }else {
                    String formattedDate = TimeUtils.getHistoryDay(history.getCreatedDate());
                    holder.dateTextView.setVisibility(View.VISIBLE);
                    holder.dateTextView.setText(formattedDate);
                }

                holder.timeTextView.setText(""+TimeUtils.getFormattedTime(history.getCreatedDate()));
                holder.purchaseTypeImageView.setImageResource(ImageUtil.getPurchaseTypeIcon(historyObj.getPurchaseType()));
            }else {
                holder.historyContainer.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
