package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.History;

import java.util.List;

/**
 * Created by diptif on 11/09/17.
 */

public interface HistoryView {

    void onHistoryLoadedSuccessfully(List<History> historyList);

    void onFailure(String message);
}
