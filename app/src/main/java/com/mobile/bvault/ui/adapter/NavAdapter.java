package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.model.NavOption;
import com.mobile.bvault.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diptif on 11/09/17.
 */

public class NavAdapter extends RecyclerView.Adapter<NavAdapter.ViewHolder> {

    private List<NavOption> navOptions;
    private Context context;
    private ItemClickListener itemClickListener;
    int count = 0;

    public NavAdapter( Context context, ItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public NavAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_drawer,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NavAdapter.ViewHolder holder, int position) {
        NavOption navOption = this.navOptions.get(position);
        if (count > 0 && navOption.getTitle().equalsIgnoreCase(context.getString(R.string.notification))) {
            holder.textView.setText(navOption.getTitle());
            holder.countTextView.setVisibility(View.VISIBLE);
            holder.countTextView.setText(String.format("%02d", count));
        }else {
            holder.textView.setText(navOption.getTitle());
            holder.countTextView.setText("");
            holder.countTextView.setVisibility(View.GONE);
        }

        holder.onClick(navOption,itemClickListener);
    }

    public void setNavOptions(List<NavOption> navOptions) {
        this.navOptions = navOptions;
        notifyDataSetChanged();
    }

    public void setCount(int count) {
        this.count = count;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CommonUtils.getCollectionSize(navOptions);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.drawer_text_view) TextView textView;
        @BindView(R.id.drawer_text_view_count) TextView countTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            CommonUtils.setFont(textView, context);
        }

        public void onClick(final NavOption navOption, final ItemClickListener itemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(navOption);
                }
            });
        }
    }

    public interface ItemClickListener {

        void onItemClick(NavOption navOption);
    }
}
