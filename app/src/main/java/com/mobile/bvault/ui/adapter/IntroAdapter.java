package com.mobile.bvault.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by diptif on 16/10/17.
 */

public class IntroAdapter extends PagerAdapter {

    Context context;
    int[] ftuxLayouts;
    LayoutInflater layoutInflater;

    public IntroAdapter(Context context, int[] ftuxLayouts) {
        this.context = context;
        this.ftuxLayouts = ftuxLayouts;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return ftuxLayouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = layoutInflater.inflate(ftuxLayouts[position],container,false);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
