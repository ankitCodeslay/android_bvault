package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.PromoCode;

/**
 * Created by diptif on 10/10/17.
 */

public interface PromoCodeView {

    void onPromoCodeAppliedSuccessfully(PromoCode promoCode);

    void onPromoCodeFailure(String message);

}
