package com.mobile.bvault.ui.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.User;
import com.mobile.bvault.presenter.ChangePasswordPresenter;
import com.mobile.bvault.presenter.ProfilePresenter;
import com.mobile.bvault.presenter.SignOutPresenter;
import com.mobile.bvault.ui.view.ChangePasswordView;
import com.mobile.bvault.ui.view.ProfileView;
import com.mobile.bvault.ui.view.SignOutView;
import com.mobile.bvault.aws.AWSUtil;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.Helper;
import com.mobile.bvault.utils.ImageUtil;
import com.mobile.bvault.utils.NetworkUtils;
import com.mobile.bvault.utils.PermissionUtil;
import com.mobile.bvault.utils.TimeUtils;
import com.yalantis.ucrop.UCrop;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.mobile.bvault.utils.Constants.RESET_LIMIT;
import static com.mobile.bvault.utils.Constants.longOneSecond;
import static com.mobile.bvault.utils.Constants.longTimeAfterWhichButtonEnable;
import static com.mobile.bvault.utils.Constants.longTotalVerificationTime;

public class ProfileActivity extends BaseActivity implements SignOutView, ProfileView, ChangePasswordView, View.OnClickListener {

    private static final String TAG = "ProfileActivity";

    boolean isEditEnable = false;
    @BindView(R.id.et_full_name)
    EditText etName;
    @BindView(R.id.et_email)
    TextView tvEmail;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.tv_dob)
    TextView etDOB;
    @BindView(R.id.et_favourite)
    EditText etFavourite;
    @BindView(R.id.btn_update_profile)
    Button updateBtn;
    @BindView(R.id.profile_pic)
    CircleImageView profileImageView;

    @BindView(R.id.referral_code_text_view)
    TextView referralCodeTextView;
    @BindView(R.id.appliedRefCodeIV)
    ImageView appliedRefCodeIV;

    SignOutPresenter signOutPresenter;
    ProfilePresenter profilePresenter;
    ChangePasswordPresenter changePasswordPresenter;
    boolean hasMobAndDob = false;
    boolean isOtherLogin = false;
    Uri imageCaptureUri;

    String existingPhoneNumber;

    Dialog referralCodeDialog;
    EditText etReferralCode;
    Button cancelButton;
    Button applyButton;
    private String referralCode;

    Dialog otpDialog;
    EditText editOtp;
    Button cancelOtpBtn;
    Button submitOtpBtn;
    TextView resendOtpTv;
    TextView timerTV;

    public static Runnable r;
    public static Handler handler;
    public String textMobileNo;
    private int otp;
    static Handler handlerButtonVisibility;
    public static Runnable rButtonVisibility;
    private CountDownTimer countDownTimer;
    private int intNumberOfTimesResendPress = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setSupportToolbar();
        setupMenu();
        setTitle(getString(R.string.profile));
        initDrawer();
        initOtpDialog();
        initRefCodeDialog();
        boolean hasMobAndDob = CommonUtils.hasMobAndDob(ProfileActivity.this);
        referralCodeTextView.setVisibility(hasMobAndDob ? View.GONE : View.VISIBLE);
        isOtherLogin = hasMobAndDob ? false : true;
        if (hasMobAndDob) {
            etPhoneNumber.setClickable(false);
            etPhoneNumber.setEnabled(false);
            etPhoneNumber.setTextColor(ContextCompat.getColor(this, R.color.gray_color));
        } else {
            etPhoneNumber.setClickable(true);
            etPhoneNumber.setEnabled(true);
            etPhoneNumber.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
        signOutPresenter = new SignOutPresenter(this, this);
        changePasswordPresenter = new ChangePasswordPresenter(this, this);
        profilePresenter = new ProfilePresenter(this, this);
        profilePresenter.setUserInfo();
    }

    private void setupMenu() {
        hasMobAndDob = CommonUtils.hasMobAndDob(this);
        if (hasMobAndDob) {
            setToolbarLeftIconClickListener();
            setToolbarRightIconClickListener();
            enableDisableEditText(false);
        } else {
            enableDisableEditText(true);
            showToastMessage("Before proceeding, please fill the required field and update the profile");
        }
    }

    private void initRefCodeDialog() {
        referralCodeDialog = new Dialog(this);
        referralCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralCodeDialog.setContentView(R.layout.view_referral_code);
        referralCodeDialog.setCanceledOnTouchOutside(false);
        etReferralCode = referralCodeDialog.findViewById(R.id.et_referral_code);
        applyButton = referralCodeDialog.findViewById(R.id.btn_apply);
        applyButton.setOnClickListener(this);
        cancelButton = referralCodeDialog.findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(this);
    }

    @OnClick(R.id.referral_code_text_view)
    void showReferralCodeDialog() {
        showRefCodeDialog();
    }

    @OnClick(R.id.sign_out_text_view)
    void signOut() {
        if (signOutPresenter != null) {
            showLoading();
            signOutPresenter.signOut();
        }
    }

    @OnClick(R.id.edit_profile_text_view)
    void editProfile() {
        if (isEditEnable) {
            enableDisableEditText(false);
        } else {
            enableDisableEditText(true);
        }
    }

    @Override
    public void onRefCodeValidationFail(String message) {
        hideLoading();
        hideRefCodeDialog();
        showSnackBar(message);
        appliedRefCodeIV.setVisibility(View.GONE);
        referralCodeTextView.setText(getString(R.string.have_referral_code));

    }

    @Override
    public void onRefCodeValidationSuccess() {
        hideLoading();
        hideRefCodeDialog();
        appliedRefCodeIV.setVisibility(View.VISIBLE);
        showSnackBar(getString(R.string.messageRefCodeSuc));
        referralCodeTextView.setText("Referral code " + referralCode + " applied.");
    }

    void enableDisableEditText(boolean isEnable) {
        etName.setEnabled(isEnable);
        etName.requestFocus();
//        tvEmail.setEnabled(isEnable);
        etPhoneNumber.setEnabled(isEnable);
        etDOB.setEnabled(isEnable);
        etFavourite.setEnabled(isEnable);
        updateBtn.setVisibility(isEnable ? View.VISIBLE : View.INVISIBLE);
        isEditEnable = isEnable;
    }

    @OnClick(R.id.tv_dob)
    void selectDob() {
        if (!isEditEnable) {
            return;
        }

        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                String date = TimeUtils.getFormattedDob(calendar.getTime());
                etDOB.setText(date);
            }
        }, year, month, day);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        dialog.show();
    }

    @OnClick(R.id.btn_update_profile)
    void updateProfile() {
        String fullName = etName.getText().toString().trim();
        String email = tvEmail.getText().toString().trim();
        String phone = etPhoneNumber.getText().toString().trim();
        String dob = etDOB.getText().toString().trim();
        profilePresenter.validate(fullName, email, phone, dob);
    }

    @OnClick(R.id.change_password_text_view)
    void changePassword() {
        if (changePasswordPresenter != null)
            changePasswordPresenter.showDialog();
    }

    @Override
    public void setProfile(User user) {
        if (user != null) {
            etName.setText(user.getDisplayName() != null ? user.getDisplayName() : "");
            tvEmail.setText(user.getEmail() != null ? user.getEmail() : "");
            etPhoneNumber.setText(user.getPhoneNumber() != null ? user.getPhoneNumber() : "");
            existingPhoneNumber = user.getPhoneNumber() != null ? user.getPhoneNumber() : "";
//            String date = TimeUtils.getFormattedDob(user.getDob());
            etDOB.setText(user.getDob() != null ? user.getDob() : "");
            etFavourite.setText(user.getFavoriteDrink() != null ? user.getFavoriteDrink() : "");

            if (user.getImageUrl() != null) {
                CommonUtils.printErrorLog(TAG, "User image url is not null");
                String imgUrl = AWSUtil.generateAmazonImageURL(Constants.FOLDER_PROFILE_PICTURE, user.getImageUrl());
                ImageUtil.setImage(this, profileImageView, imgUrl, R.drawable.user_icon);
            } else {
                CommonUtils.printErrorLog(TAG, "User image is null");
                profileImageView.setImageResource(R.drawable.user_icon);
            }
        }
    }

    @Override
    public void onSuccessfulSignOut() {
        hideLoading();
        AppPreferencesHelper.getInstance(ProfileActivity.this).setUserReferralCode("");
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onSignOutFailure(String message) {
        hideLoading();
        showToastMessage(message);
    }


    @OnClick(R.id.change_pic_image_view)
    void capturePhoto() {
        if (PermissionUtil.hasPermissionsGranted(this, PermissionUtil.CAMERA_PERMISSIONS)) {
            imageCaptureUri = ImageUtil.getOutputMediaFileUri();
            if (imageCaptureUri != null) {
                ImageUtil.openMediaSelector(Constants.REQUEST_CHANGE_IMAGE_CODE, this, imageCaptureUri);
            }
        } else {
            PermissionUtil.requestPermissions(this, Constants.REQUEST_PERMISSION_CODE, PermissionUtil.CAMERA_PERMISSIONS);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CHANGE_IMAGE_CODE) {
//                Bundle bundle = data.getExtras();
                if (data != null && data.getData() != null) {
                    ImageUtil.startCropActivity(data.getData(), this);
                } else {
                    if (imageCaptureUri != null) {
                        ImageUtil.startCropActivity(imageCaptureUri, this);
                    } else {
                        Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            ImageUtil.handleCropError(data, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (PermissionUtil.hasPermissionsGranted(this, PermissionUtil.CAMERA_PERMISSIONS)) {
            imageCaptureUri = ImageUtil.getOutputMediaFileUri();
            if (imageCaptureUri != null) {
                ImageUtil.openMediaSelector(Constants.REQUEST_CHANGE_IMAGE_CODE, this, imageCaptureUri);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onProfileUpdatedSuccessfully() {
        hideLoading();
        enableDisableEditText(false);
        isEditEnable = false;
        AppPreferencesHelper.getInstance(this).setFacebookOrGoogleLogin(false);
        showToastMessage("Profile updated successfully");
        updateDrawerInfo();
        if (!hasMobAndDob) {
            goToHome();
        }
    }

    private void goToHome() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    @Override
    public void onProfileUpdateFailure(String message) {
        hideLoading();
        if (message != null && !message.isEmpty())
            showToastMessage(message);
    }

    @Override
    public void onProfileImageUploadedSuccessfully(String imageUrl) {
        hideLoading();
        showToastMessage("Profile Image Uploaded Successfully");
        profileImageView.setImageURI(Uri.parse(imageUrl));
        updateDrawerInfo();
//        ImageUtil.setImage(this,profileImageView,imageUrl,R.drawable.ic_person_black_24dp);

//        Picasso.with(this).load(imageUrl);
    }

    @Override
    public void onProfileImageUploadedFailure(String errorMessage) {
        hideLoading();

        if (errorMessage != null && !errorMessage.isEmpty())
            showToastMessage(errorMessage);

    }

    @Override
    public void onValidationSuccess() {
        String currentPhoneNumber = etPhoneNumber.getText().toString();
        if (!existingPhoneNumber.equalsIgnoreCase(currentPhoneNumber)) {
            sendOtpTask();
            showOtpDialog();
        } else {
            updateProfileToServer();
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        if (UCrop.getOutput(result) != null) {
            showLoading();
            profilePresenter.uploadImageOnAWS(UCrop.getOutput(result).getPath());
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onValidationFailure(String message) {
        showToastMessage(message);
    }

    @Override
    public void onPasswordChangedSuccessful() {
        showToastMessage("Password changed successfully");
    }

    @Override
    public void onPasswordChangeFailure(String message) {
        showToastMessage(message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                hideRefCodeDialog();
                break;
            case R.id.btn_apply:
                applyReferralCode();
                break;
            case R.id.btn_otp_cancel:
                cancelHandler();
                hideOtpDialog();
                break;
            case R.id.btn_otp_submit:
                validateOtp();
                break;
            case R.id.resend_otp_tv: {
                intNumberOfTimesResendPress = intNumberOfTimesResendPress + 1;
                if (intNumberOfTimesResendPress <= RESET_LIMIT) {
                    sendOtpTask();
                }
                break;
            }
        }
    }

    void applyReferralCode() {
        referralCode = etReferralCode.getText().toString().trim();
        if (referralCode.isEmpty()) {
            showToastMessage("Please enter referral code");
        } else {
            if (profilePresenter != null) {
                showLoading();
                profilePresenter.validateReferralCode(referralCode);
            }
        }
    }

    private void initOtpDialog() {
        otpDialog = new Dialog(this);
        otpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        otpDialog.setContentView(R.layout.view_otp_validation);
        otpDialog.setCanceledOnTouchOutside(false);
        otpDialog.setCancelable(false);
        editOtp = otpDialog.findViewById(R.id.edit_otp);
        submitOtpBtn = otpDialog.findViewById(R.id.btn_otp_submit);
        submitOtpBtn.setOnClickListener(this);
        cancelOtpBtn = otpDialog.findViewById(R.id.btn_otp_cancel);
        cancelOtpBtn.setOnClickListener(this);
        resendOtpTv = otpDialog.findViewById(R.id.resend_otp_tv);
        resendOtpTv.setOnClickListener(this);
        timerTV = otpDialog.findViewById(R.id.timerTV);
    }

    void showOtpDialog() {
        if (otpDialog != null) {
            otpDialog.show();
            editOtp.setText("");
        }
    }

    void hideOtpDialog() {
        if (otpDialog != null)
            otpDialog.dismiss();
    }

    public void sendOtpTask() {

        textMobileNo = etPhoneNumber.getText().toString();
        handler = new Handler();
        if (otp == 0) {
            otp = Helper.randomNumberCreation(100000, 999999);
        }
        AppPreferencesHelper.getInstance(this).setOtp(otp);
        if (NetworkUtils.isNetworkConnected(this)) {
            resendOtpTv.setClickable(false);
            resendOtpTv.setAlpha(0.5f);
            functionEnablingButtons();
            functionInitializingCountDownTimer();
            profilePresenter.sendOtp(textMobileNo, otp + "");
        } else {
            showSnackBar(Constants.NO_INTERNET_CONNECTION);
        }
    }

    public void functionEnablingButtons() {
        rButtonVisibility = new Runnable() {
            @Override
            public void run() {
                resendOtpTv.setClickable(true);
                resendOtpTv.setAlpha(1);
            }
        };
        handlerButtonVisibility = new Handler();
        handlerButtonVisibility.postDelayed(rButtonVisibility, longTimeAfterWhichButtonEnable);
    }

    public void functionInitializingCountDownTimer() {
        timerTV.setVisibility(View.VISIBLE);
        countDownTimer = new CountDownTimer(longTotalVerificationTime, longOneSecond) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerTV.setVisibility(View.VISIBLE);
                int remainingIntTime = (int) millisUntilFinished / 1000;
                String remainingTime = "";
                if (remainingIntTime < 10) {
                    remainingTime = "0" + remainingIntTime;
                } else {
                    remainingTime = "" + remainingIntTime;
                }
                timerTV.setText("00:" + remainingTime);
            }

            @Override
            public void onFinish() {
                timerTV.setVisibility(View.INVISIBLE);
            }
        }.start();
    }

    void validateOtp() {
        String inputOtp = editOtp.getText().toString().trim();
        if (inputOtp.isEmpty()) {
            showToastMessage(getString(R.string.emptyOtp));
        } else {
            int oldOtp = AppPreferencesHelper.getInstance(this).getOtp();
            if (oldOtp == Integer.parseInt(inputOtp)) {
                cancelHandler();
                hideOtpDialog();
                showToastMessage(getString(R.string.messageNoValidated));
                updateProfileToServer();
            } else {
                showToastMessage(getString(R.string.messageOtpInvalid));
                editOtp.setText("");
            }
        }
    }

    public void cancelHandler() {
        if (handlerButtonVisibility == null || countDownTimer == null || rButtonVisibility == null)
            return;
        handlerButtonVisibility.removeCallbacks(rButtonVisibility);
        countDownTimer.cancel();
    }

    private void updateProfileToServer() {
        showLoading();
        String fullName = etName.getText().toString().trim();
        String email = tvEmail.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String dob = etDOB.getText().toString().trim();
        String fav = etFavourite.getText().toString().trim();
        profilePresenter.updateProfile(fullName, email, phoneNumber, dob, fav, referralCode, isOtherLogin);
    }

    void showRefCodeDialog() {
        if (referralCodeDialog != null) {
            referralCodeDialog.show();
            etReferralCode.setText("");
        }
    }

    void hideRefCodeDialog() {
        if (referralCodeDialog != null)
            referralCodeDialog.dismiss();
    }
}
