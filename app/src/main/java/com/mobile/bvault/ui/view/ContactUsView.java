package com.mobile.bvault.ui.view;

/**
 * Created by diptif on 01/11/17.
 */

public interface ContactUsView {

    void onContactUsSuccess(String message);

    void onContactUsFailure(String message);
}
