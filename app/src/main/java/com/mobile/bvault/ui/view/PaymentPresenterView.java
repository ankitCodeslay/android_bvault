package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;

/**
 * Created by diptif on 13/11/17.
 */

public interface PaymentPresenterView extends BaseView{

    void onPayUMoneyPaymentSuccess();
    void onPayUMoneyPaymentFailure(String message);
}
