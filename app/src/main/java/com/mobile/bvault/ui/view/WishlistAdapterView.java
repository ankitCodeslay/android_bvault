package com.mobile.bvault.ui.view;

import android.widget.TextView;

/**
 * Created by diptif on 30/08/17.
 */

public interface WishlistAdapterView {

    /**
     * Indicates quantity updated or not
     * @param result number of row affected by update call
     * @param textView text field to set quantity
     * @param qty qty to update
     */
    void updateQuantityCallback(int result, TextView textView,final TextView priceTextView,int qty, int price, int position);

    /**
     * Indicates item removed from wishlist or not
     * @param result index of removed item.
     * @param position adapter position
     *
     */
    void removeItemCallback(int result, int position);

    /**
     * Calculate total amount
     */
    void setTotalAmount();
}
