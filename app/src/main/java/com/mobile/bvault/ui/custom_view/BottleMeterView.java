package com.mobile.bvault.ui.custom_view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.mobile.bvault.R;

/**
 * Created by diptif on 30/08/17.
 */

public class BottleMeterView extends View {

    private static final int START_ANGLE_POINT = 270;

    private final Paint remainPaint;
    private final Paint consumePaint;
    private final Paint whiteBackgroundPaint;
    private final Paint outLinePaint;
    private RectF rect;
    private RectF rectF;
    private RectF whiteBackRectF;
    private RectF outlineRectF;
    private float cx;
    private float cy;
    int strokeWidth;
    int dis = 1;
    int dis2 = 30;
    int dis3 = 50;
    private float angle;

    public BottleMeterView(Context context, AttributeSet attrs) {
        super(context, attrs);

        strokeWidth = getResources().getInteger(R.integer.meter_stroke_size);
        dis2 = getResources().getInteger(R.integer.meter_outline_gap_size);
        dis3 = dis2 + getResources().getInteger(R.integer.stroke_distance_size);

        remainPaint = new Paint();
        remainPaint.setAntiAlias(true);
        remainPaint.setStyle(Paint.Style.STROKE);
        remainPaint.setStrokeWidth(strokeWidth);
        remainPaint.setColor(ContextCompat.getColor(this.getContext(),R.color.remain_color));

        consumePaint = new Paint();
        consumePaint.setAntiAlias(true);
        consumePaint.setStyle(Paint.Style.STROKE);
        consumePaint.setStrokeWidth(strokeWidth);
        consumePaint.setColor(ContextCompat.getColor(this.getContext(),R.color.consume_color));

        whiteBackgroundPaint = new Paint();
        whiteBackgroundPaint.setStyle(Paint.Style.FILL);
        whiteBackgroundPaint.setColor(Color.WHITE);

        outLinePaint = new Paint();
        outLinePaint.setStyle(Paint.Style.STROKE);
        outLinePaint.setAntiAlias(true);
        outLinePaint.setStrokeWidth(2);
        outLinePaint.setColor(Color.WHITE);


        //Initial Angle (optional, it can be zero)
        angle = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int min = Math.min(getMeasuredWidth(),getMeasuredHeight());

        outlineRectF = new RectF(dis, dis, min - dis, min - dis);
        whiteBackRectF = new RectF(dis2, dis2, min - dis2, min - dis2);
        rect = new RectF(dis3, dis3, min - dis3, min - dis3);

        canvas.drawArc(outlineRectF,0,360,false, outLinePaint);
        canvas.drawArc(whiteBackRectF, 0, 360, false, whiteBackgroundPaint);
        canvas.drawArc(rect,START_ANGLE_POINT,360,false, consumePaint);
        canvas.drawArc(rect, START_ANGLE_POINT, angle, false, remainPaint);
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
