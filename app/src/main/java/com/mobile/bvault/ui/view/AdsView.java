package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.Advertisement;

import java.util.List;

/**
 * Created by diptif on 18/09/17.
 */

public interface AdsView {

    void onAdsLoadedSuccessfully(List<Advertisement> advertisementList);

    void onAdsFailure(String message);
}
