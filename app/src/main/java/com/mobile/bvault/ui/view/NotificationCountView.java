package com.mobile.bvault.ui.view;

import com.mobile.bvault.model.NotificationCount;
import com.mobile.bvault.model.Payment;

import java.util.List;

/**
 * Created by diptif on 25/10/17.
 */

public interface NotificationCountView {

    void onNotificationCountLoadedSuccessfully(NotificationCount notificationCount);

    void onNotificationCountFailure(String message);

}
