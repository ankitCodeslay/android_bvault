package com.mobile.bvault.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mobile.bvault.ui.fragment.UnusedPromocodeFragment;
import com.mobile.bvault.ui.fragment.UsedPromocodeFragment;

import java.util.HashMap;
import java.util.Map;


public class ReferAndEarnPagerAdapter extends FragmentStatePagerAdapter {
    int tabCount;

    private Map<Integer, Fragment> fragmentMap;

    //Constructor to the class
    public ReferAndEarnPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
        fragmentMap = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                UnusedPromocodeFragment tab1 = new UnusedPromocodeFragment();
                fragmentMap.put(0, tab1);
                return tab1;
            case 1:
                UsedPromocodeFragment tab2 = new UsedPromocodeFragment();
                fragmentMap.put(1, tab2);
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    public Fragment getFragment(int pos) {
        if (fragmentMap == null) return null;
        return fragmentMap.get(pos);
    }
}
