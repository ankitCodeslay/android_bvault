package com.mobile.bvault.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mobile.bvault.R;
import com.mobile.bvault.model.PromoCode;
import com.mobile.bvault.model.PromoCodeModel;
import com.mobile.bvault.ui.adapter.DrinkTypeAdapter;
import com.mobile.bvault.ui.adapter.PromoCodeAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnusedPromocodeFragment extends Fragment {

    View view;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.noHistoryLL)
    LinearLayout noHistoryLL;

    private LinearLayoutManager layoutManager;
    private PromoCodeAdapter adapter;
    private List<PromoCodeModel> promoCodeList;

    public UnusedPromocodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_unused_promocode, container, false);
        ButterKnife.bind(this, view);
        setupRecyclerView();
        return view;
    }

    private void setupRecyclerView() {
        promoCodeList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new PromoCodeAdapter(getActivity(), promoCodeList,1);
        recyclerView.setAdapter(adapter);
    }

    public void updatePromoCodeList(List<PromoCodeModel> pCodeList) {
        if (pCodeList != null && !pCodeList.isEmpty()) {
            noHistoryLL.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            promoCodeList.clear();
            promoCodeList.addAll(pCodeList);
            adapter.notifyDataSetChanged();
        } else {
            noHistoryLL.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

}
