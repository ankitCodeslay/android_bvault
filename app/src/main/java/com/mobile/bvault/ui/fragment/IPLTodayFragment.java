package com.mobile.bvault.ui.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.data.network.ApiClient;
import com.mobile.bvault.data.network.BottleApiInterface;
import com.mobile.bvault.data.network.IPLApiInterface;
import com.mobile.bvault.data.pref.AppPreferencesHelper;
import com.mobile.bvault.model.IPLMatchModel;
import com.mobile.bvault.model.PurchaseRequest;
import com.mobile.bvault.model.PurchasedBottle;
import com.mobile.bvault.model.UserEarningModel;
import com.mobile.bvault.ui.activity.HowToPlayActivity;
import com.mobile.bvault.ui.activity.PurchasedMessageActivity;
import com.mobile.bvault.ui.adapter.IplMatchAdapter;
import com.mobile.bvault.utils.Constants;
import com.mobile.bvault.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mobile.bvault.utils.Constants.BOTTLE1_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE2_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE3_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE4_POINT;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_1;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_2;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_3;
import static com.mobile.bvault.utils.Constants.BOTTLE_TYPE_4;

/**
 * A simple {@link Fragment} subclass.
 */
public class IPLTodayFragment extends Fragment {

    View view;
    @BindView(R.id.earnedTV)
    public TextView earnedTV;

    @BindView(R.id.bottle1LL)
    public LinearLayout bottle1LL;
    @BindView(R.id.bottle2LL)
    public LinearLayout bottle2LL;

    @BindView(R.id.bottle3LL)
    public LinearLayout bottle3LL;
    @BindView(R.id.bottle4LL)
    public LinearLayout bottle4LL;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btn_play)
    Button btnPlay;
    @BindView(R.id.noMatchTV)
    TextView noMatchTV;

    @BindView(R.id.bottle1PointsTV)
    TextView bottle1PointsTV;
    @BindView(R.id.bottle2PointsTV)
    TextView bottle2PointsTV;

    @BindView(R.id.bottle3PointsTV)
    TextView bottle3PointsTV;
    @BindView(R.id.bottle4PointsTV)
    TextView bottle4PointsTV;

    private LinearLayoutManager layoutManager;
    private IplMatchAdapter adapter;
    private List<IPLMatchModel> iplMatchList;
    private int totalEarningPoints;

    public IPLTodayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ipl_today, container, false);
        ButterKnife.bind(this, view);

        bottle1PointsTV.setText("Points: " + BOTTLE1_POINT);
        bottle2PointsTV.setText("Points: " + BOTTLE2_POINT);
        bottle3PointsTV.setText("Points: " + BOTTLE3_POINT);
        bottle4PointsTV.setText("Points: " + BOTTLE4_POINT);

        setupRecyclerView();
        initClickListner();
        desableBottles();
        return view;
    }

    private void setupRecyclerView() {
        iplMatchList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new IplMatchAdapter(getActivity(), iplMatchList);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(true);
    }

    public void updateAdapter(List<IPLMatchModel> matchModelList) {
        iplMatchList.clear();
        iplMatchList.addAll(matchModelList);
        adapter.notifyDataSetChanged();
        if (iplMatchList.isEmpty()) {
            noMatchTV.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noMatchTV.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void updateEarningPoint(UserEarningModel earningModel) {
        if (earningModel == null) return;
        totalEarningPoints = earningModel.getEarnedValue();
        earnedTV.setText("Total points earned - " + earningModel.getEarnedValue());
        if (!earningModel.isBottleEnabled()) {
            bottle1LL.setClickable(false);
            bottle1LL.setEnabled(false);
            bottle1LL.setAlpha(0.5f);

            bottle2LL.setClickable(false);
            bottle2LL.setEnabled(false);
            bottle2LL.setAlpha(0.5f);

            bottle3LL.setClickable(false);
            bottle3LL.setEnabled(false);
            bottle3LL.setAlpha(0.5f);

            bottle4LL.setClickable(false);
            bottle4LL.setEnabled(false);
            bottle4LL.setAlpha(0.5f);

        } else {
            bottle1LL.setClickable(true);
            bottle1LL.setEnabled(true);
            bottle1LL.setAlpha(1.0f);

            bottle2LL.setClickable(true);
            bottle2LL.setEnabled(true);
            bottle2LL.setAlpha(1.0f);

            bottle3LL.setClickable(true);
            bottle3LL.setEnabled(true);
            bottle3LL.setAlpha(1.0f);

            bottle4LL.setClickable(true);
            bottle4LL.setEnabled(true);
            bottle4LL.setAlpha(1.0f);
        }

        if (earningModel.getEarnedValue() < BOTTLE1_POINT) {
            bottle1LL.setClickable(false);
            bottle1LL.setEnabled(false);
            bottle1LL.setAlpha(0.5f);
        }
        if (earningModel.getEarnedValue() < BOTTLE2_POINT) {
            bottle2LL.setClickable(false);
            bottle2LL.setEnabled(false);
            bottle2LL.setAlpha(0.5f);
        }

        if (earningModel.getEarnedValue() < BOTTLE3_POINT) {
            bottle3LL.setClickable(false);
            bottle3LL.setEnabled(false);
            bottle3LL.setAlpha(0.5f);
        }
        if (earningModel.getEarnedValue() < BOTTLE4_POINT) {
            bottle4LL.setClickable(false);
            bottle4LL.setEnabled(false);
            bottle4LL.setAlpha(0.5f);
        }
    }

    public void desableBottles() {

        bottle1LL.setClickable(false);
        bottle1LL.setEnabled(false);
        bottle1LL.setAlpha(0.5f);

        bottle2LL.setClickable(false);
        bottle2LL.setEnabled(false);
        bottle2LL.setAlpha(0.5f);

        bottle3LL.setClickable(false);
        bottle3LL.setEnabled(false);
        bottle3LL.setAlpha(0.5f);

        bottle4LL.setClickable(false);
        bottle4LL.setEnabled(false);
        bottle4LL.setAlpha(0.5f);
    }

    private void initClickListner() {
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HowToPlayActivity.class);
                startActivity(intent);
            }
        });

        bottle1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_1);
            }
        });
        bottle2LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_2);
            }
        });

        bottle3LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_3);

            }
        });
        bottle4LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(getString(R.string.alert_ok_button), getString(R.string.alert_cancel_button), BOTTLE_TYPE_4);
            }
        });
    }

    public void savePurchasedBottle(final int bottleType) {
        if (getActivity() == null) return;
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            ((BaseActivity) getActivity()).showSnackBar(Constants.NO_INTERNET_CONNECTION);
            return;
        }
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showLoading();
        }
        PurchaseRequest purchaseRequest = getPurchaseRequestData(bottleType);
        if (purchaseRequest != null) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<Void> call = bottleApiInterface.purchaseBottle(purchaseRequest);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (getActivity() == null) return;
                    if (response.isSuccessful()) {
                        updateUserEarningPoints(bottleType);
                    } else {
                        ((BaseActivity) getActivity()).hideLoading();
                        ((BaseActivity) getActivity()).showSnackBar(response.message());
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    if (getActivity() == null) return;
                    ((BaseActivity) getActivity()).showSnackBar(t.getMessage());
                    ((BaseActivity) getActivity()).hideLoading();
                }
            });
        }
    }

    private PurchaseRequest getPurchaseRequestData(int bottleType) {
        PurchaseRequest purchaseRequest = null;
        List<PurchasedBottle> purchasedBottles = new ArrayList<PurchasedBottle>();

        String userId = AppPreferencesHelper.getInstance(getActivity()).getUserId();
        if (bottleType == BOTTLE_TYPE_1) {
            purchasedBottles.add(new PurchasedBottle("5ad5da7abb7ab30012140809", 1, 330, "Silver", 300));
        } else if (bottleType == BOTTLE_TYPE_2) {
            purchasedBottles.add(new PurchasedBottle("5ad5dd0abb7ab3001214080b", 1, 30, "Silver", 300));
        }
        if (bottleType == BOTTLE_TYPE_3) {
            purchasedBottles.add(new PurchasedBottle("5ad5de0cbb7ab3001214080d", 1, 30, "Silver", 300));
        } else if (bottleType == BOTTLE_TYPE_4) {
            purchasedBottles.add(new PurchasedBottle("5ad5e2ddbb7ab3001214080f", 1, 200, "Silver", 500));
        }
        purchaseRequest = new PurchaseRequest(userId, purchasedBottles, "", true, totalEarningPoints);
        return purchaseRequest;
    }

    public void updateUserEarningPoints(final int bottleType) {
        if (getActivity() == null) return;
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            ((BaseActivity) getActivity()).showSnackBar(Constants.NO_INTERNET_CONNECTION);
            ((BaseActivity) getActivity()).hideLoading();
            return;
        }
        UserEarningModel userEarningModel = new UserEarningModel();
        if (bottleType == BOTTLE_TYPE_1) {
            userEarningModel.setRedeemPoints(BOTTLE1_POINT);
        } else if (bottleType == BOTTLE_TYPE_2) {
            userEarningModel.setRedeemPoints(BOTTLE2_POINT);
        } else if (bottleType == BOTTLE_TYPE_3) {
            userEarningModel.setRedeemPoints(BOTTLE3_POINT);
        } else if (bottleType == BOTTLE_TYPE_4) {
            userEarningModel.setRedeemPoints(BOTTLE4_POINT);
        }
        String userId = AppPreferencesHelper.getInstance(getActivity()).getUserId();
        userEarningModel.setUserId(userId);

        IPLApiInterface iplApiInterface = ApiClient.getClient().create(IPLApiInterface.class);
        Call<UserEarningModel> apiCall = iplApiInterface.updateUserEarningPoints(userEarningModel);

        apiCall.enqueue(new Callback<UserEarningModel>() {
            @Override
            public void onResponse(Call<UserEarningModel> call, Response<UserEarningModel> response) {
                if (getActivity() == null) return;
                ((BaseActivity) getActivity()).hideLoading();
                if (response.isSuccessful()) {
                    UserEarningModel earningModel = response.body();
                    if (earningModel == null) return;
                    updateEarningPoint(earningModel);
                    saveIPLRedeemedBottle(bottleType);
                    startActivity(new Intent(getActivity(), PurchasedMessageActivity.class));
                } else {
                    ((BaseActivity) getActivity()).showSnackBar(response.message());
                }
            }

            @Override
            public void onFailure(Call<UserEarningModel> call, Throwable t) {
                if (getActivity() == null) return;
                ((BaseActivity) getActivity()).hideLoading();
                ((BaseActivity) getActivity()).showSnackBar(t.getMessage());
            }
        });
    }

    public void alert(String positiveButton, String negativeButton, final int bottleType) {
        if (getActivity() == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.msg_redeem));
        builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                savePurchasedBottle(bottleType);

            }
        });
        builder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void saveIPLRedeemedBottle(final int bottleType) {
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            return;
        }
        PurchaseRequest purchaseRequest = getPurchaseRequestData(bottleType);
        if (purchaseRequest != null) {
            BottleApiInterface bottleApiInterface = ApiClient.getClient().create(BottleApiInterface.class);
            Call<Void> call = bottleApiInterface.saveRedeemedBottle(purchaseRequest);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {

                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }
    }
}
