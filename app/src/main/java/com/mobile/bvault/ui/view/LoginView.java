package com.mobile.bvault.ui.view;

import com.mobile.bvault.base.BaseView;
import com.mobile.bvault.model.User;

/**
 * Created by diptif on 17/08/17.
 */

public interface LoginView extends BaseView{

    /**
     *  Indicate user successfully logged in
     */
    void onLoginSuccess(User user);

    /**
     * Indicate user fail to login
     * @param message Login failure message
     */
    void onLoginFailure(String message);

    /**
     * Indicate validation failure
     * @param message validation failure message
     */
    void onValidationFail(String message);

    /**
     * Indicate validation successful
     */
    void onValidationSuccess();

    /**
     * Set the user name from the preferences
     */
    void setUserName();

    /**
     * Go to sign screen
     */
    void goToSignUP();

    void onDeviceTokenUpdatedSuccessfully(String deviceToken);

    void onDeviceTokenUpdatedFailure(String message);

}
