package com.mobile.bvault.ui.view;

/**
 * Created by diptif on 04/09/17.
 */

public interface DrawerInterface {

    /**
     * Initialize the drawer layout
     */
    void initDrawer();

    /**
     * Shows the drawer
     */
    void openDrawer();

    /**
     * Hides the drawer
     */
    void closeDrawer();
}
