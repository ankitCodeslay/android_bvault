package com.mobile.bvault.ui.adapter;

import android.support.v7.widget.CardView;

/**
 * Created by diptif on 19/09/16.
 */
public interface CardAdapter {

    int MAX_ELEVATION_FACTOR = 5;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
