package com.mobile.bvault.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mobile.bvault.R;
import com.mobile.bvault.base.BaseActivity;
import com.mobile.bvault.model.DrinkType;
import com.mobile.bvault.presenter.DrinkTypePresenter;
import com.mobile.bvault.ui.adapter.DrinkTypeAdapter;
import com.mobile.bvault.ui.view.DrinkTypeView;
import com.mobile.bvault.utils.CommonUtils;
import com.mobile.bvault.utils.Constants;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DrinkTypeActivity extends BaseActivity implements DrinkTypeView, DrinkTypeAdapter.OnItemClickListener {

    private static final String TAG = "DrinkTypeActivity";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.move_down_image_btn)
    ImageButton imageButton;

    ImageView toolbarSearch;

    GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_type);
        ButterKnife.bind(this);
        setSupportToolbar();
        setTitle(R.string.home_toolbar_title);
        setToolbarLeftIconClickListener();
        setToolbarRightIconClickListener();
        setupRecyclerView();
        initDrawer();
        getDrinkType();
        initSearchClick();
    }

    private void setupRecyclerView() {
        layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(this.onScrollListener);
    }

    private void getDrinkType() {
        showLoading();
        new DrinkTypePresenter(this, this).getDrinkTypes();
    }

    @OnClick(R.id.toolbar_left_layer)
    void showDrawer() {
        openDrawer();
    }

    @OnClick(R.id.move_down_image_btn)
    void scrollDown() {
        recyclerView.smoothScrollToPosition(layoutManager.getChildCount());
    }

    @Override
    public void onSuccess(List<DrinkType> drinkTypes) {
        hideLoading();
        CommonUtils.printErrorLog(TAG, "drink type fetched successfully");

        sortDrinkTypeList(drinkTypes);

        recyclerView.setAdapter(new DrinkTypeAdapter(this, drinkTypes, this));
    }

    @Override
    public void onFailure(String message) {
        hideLoading();
        showSnackBar(message);

    }

    @Override
    public void onItemClick(DrinkType drinkType) {
        showToastMessage(drinkType.getType());
        Intent intent = new Intent(this, BottleListActivity.class);
        intent.putExtra(Constants.DRINK_TYPE, drinkType.getType());
        intent.putExtra(Constants.DRINK_TYPE_ID, drinkType.getTypeId());
        startActivity(intent);
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            CommonUtils.printErrorLog(TAG, "Visible item count :" + visibleItemCount);
            CommonUtils.printErrorLog(TAG, "total item count :" + totalItemCount);
            CommonUtils.printErrorLog(TAG, "first Visible item count :" + firstVisibleItemPosition);

            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                CommonUtils.printErrorLog(TAG, "scrolled down");
                imageButton.setVisibility(View.GONE);
            } else {
                imageButton.setVisibility(View.VISIBLE);
            }
        }
    };

    @SuppressLint("WrongViewCast")
    private void initSearchClick() {
        toolbarSearch = findViewById(R.id.toolbar_search);
        toolbarSearch.setVisibility(View.VISIBLE);
        toolbarSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DrinkTypeActivity.this, SearchBottleActivity.class);
                startActivity(intent);
            }
        });
    }

    private void sortDrinkTypeList(List<DrinkType> drinkTypes) {
        if (drinkTypes == null) return;
        Collections.sort(drinkTypes, new Comparator<DrinkType>() {
            @Override
            public int compare(DrinkType drinkType1, DrinkType drinkType2) {
                String s1 = drinkType1.getType();
                String s2 = drinkType2.getType();
                if (TextUtils.isEmpty(s1) || TextUtils.isEmpty(s2)) {
                    return 0;
                }
                return s1.compareToIgnoreCase(s2);
            }
        });
    }
}
