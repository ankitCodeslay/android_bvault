package com.mobile.bvault.example;

/**
 * Created by diptif on 22/08/17.
 */

public class Computer {

    // Required Parameter
    private String HDD;
    private String RAM;

//    Optional Parameter
    private boolean isGraphicCardEnable;
    private boolean isBluetoothEnable;

    public Computer(Builder builder) {
        this.HDD = builder.HDD;
        this.RAM = builder.RAM;
        this.isGraphicCardEnable = builder.isGraphicCardEnable;
        this.isBluetoothEnable = builder.isBluetoothEnable;
    }

    public String getHDD() {
        return HDD;
    }

    public String getRAM() {
        return RAM;
    }

    public boolean isBluetoothEnable() {
        return isBluetoothEnable;
    }

    public boolean isGraphicCardEnable() {
        return isGraphicCardEnable;
    }



    public static class Builder {

        // Required Parameter
        private String HDD;
        private String RAM;

//      Optional Parameter
        private boolean isGraphicCardEnable;
        private boolean isBluetoothEnable;

        public Builder(String HDD, String RAM) {
            this.HDD = HDD;
            this.RAM = RAM;
        }

        public Builder setBluetoothEnable(boolean bluetoothEnable) {
            isBluetoothEnable = bluetoothEnable;
            return this;
        }

        public Builder setGraphicCardEnable(boolean graphicCardEnable) {
            isGraphicCardEnable = graphicCardEnable;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }
}
